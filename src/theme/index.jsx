import { createTheme } from '@mui/material';

export const theme = createTheme({
  palette: {
    mode: 'light',
    primary: {
      light: '#1E2E33',
      main: '#19262b',
      dark: '#162226',
      contrastText: '#FFFFFF',
      contrastTextLight: '#8AB1BF',
      contrastTextActive: '#33C16C',
      transparent: 'transparent',
    },
    secondary: {
      light: '#26383F',
      main: '#253539',
      dark: '#1B292E',
      contrastTextActive: '#2F4950',
    },
    success: {
      main: '#04BD00',
    },
    danger: {
      main: '#BD0000',
    },
    warning: {
      main: '#F8D648',
    },
    white: {
      main: '#FFFFFF',
    },
    lightTransparent: {
      light_01: 'rgba(255, 255, 255, 0.1)',
      light_02: 'rgba(255, 255, 255, 0.2)',
      light_03: 'rgba(255, 255, 255, 0.3)',
      light_04: 'rgba(255, 255, 255, 0.4)',
      light_05: 'rgba(255, 255, 255, 0.5)',
      light_06: 'rgba(255, 255, 255, 0.6)',
      light_07: 'rgba(255, 255, 255, 0.7)',
      light_08: 'rgba(255, 255, 255, 0.8)',
      light_09: 'rgba(255, 255, 255, 0.9)',
    },
    darkTransparent: {
      dark_01: 'rgba(0, 0, 0, 0.1)',
      dark_02: 'rgba(0, 0, 0, 0.2)',
      dark_03: 'rgba(0, 0, 0, 0.3)',
      dark_04: 'rgba(0, 0, 0, 0.4)',
      dark_05: 'rgba(0, 0, 0, 0.5)',
      dark_06: 'rgba(0, 0, 0, 0.6)',
      dark_07: 'rgba(0, 0, 0, 0.7)',
      dark_08: 'rgba(0, 0, 0, 0.8)',
      dark_09: 'rgba(0, 0, 0, 0.9)',
    },
    border: {
      light: '#2F4950',
      main: '#8AB1BF',
      dark: '#253539',
      active: '#33c16c',
      green: '#55FA97',
      grey: '#4d575b',
      transparent: 'transparent',
    },
    button: {
      light: '#26383F',
      main: '#33C16C',
      white: '#FFFFFF',
      transparent: 'transparent',
    },
    gradient: {
      appBarImg: 'linear-gradient(270deg, #1E2E33 0%, rgba(30, 46, 51, 0) 100%)',
      tabBg: 'linear-gradient(0deg, #1E2E33 0%, rgba(30, 46, 51, 0) 100%)',
      tabBgActive: 'linear-gradient(0deg, rgba(51, 193, 108, 0.3) 0%, rgba(51, 193, 108, 0) 100%)',
      tabBgActiveFlip: 'linear-gradient(180deg, rgba(51, 193, 108, 0.3) 0%, rgba(51, 193, 108, 0) 100%)',
      tabIconBg: 'linear-gradient(135deg, rgba(255, 255, 255, 0.193) 0%, rgba(255, 255, 255, 0.06) 46.64%, rgba(255, 255, 255, 0.068) 72.72%, rgba(255, 255, 255, 0) 95.59%)',
    },
    scrollbar: {
      track: '#26383F',
      thumb: '#33C16C',
      transparent: 'transparent',
    },
  },
  typography: {
    fontFamily: 'Rubik',
    fontWeightThin: 100,
    fontWeightExtraLight: 200,
    fontWeightLight: 300,
    fontWeightRegular: 400,
    fontWeightMedium: 500,
    fontWeightSemiBold: 600,
    fontWeightBold: 700,
    fontWeightExtraBold: 800,
    fontWeightBoldBlack: 900,
  },
  shadows: [
    'none',
    '-4px 4px 12px rgba(0, 0, 0, 0.25)',
    '0px 6px 6px rgba(0, 0, 0, 0.25)',
    '0px 0px 20px rgba(51, 193, 108, 0.4)',
    'inset 0px 2px 12px rgba(255, 255, 255, 0.02)',
    '0px 0px 24.1117px rgba(40, 169, 9, 0.5), inset -4px -4px 10px #0D5B2C, inset 4px 4px 10px #45FF90',
  ],
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 900,
      xm: 1070,
      lg: 1200,
      ml: 1396,
      xl: 1536,
    },
  },
});
