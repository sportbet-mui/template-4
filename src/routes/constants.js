import { Navigate } from 'react-router-dom';
import Home from '../components/Home';
import MainLayout from '../components/Shared/MainLayout';
import Casino from '../components/Casino';
import Crash from '../components/Crash';
import Bonuses from '../components/Bonuses';
import Results from '../components/Results';
import MyBets from '../components/MyBets';
import MyAccount from '../components/UserDetails/MyAccount';
import Wallet from '../components/UserDetails/Wallet';
import ResponsibleGambling from '../components/UserDetails/ResponsibleGambling';

const Redirect = ({ to }) => {
    return <Navigate to={to} />;
  };
  
  export const desktopRoutes = [
    {
      path: '/',
      element: <MainLayout />,
      children: [
        {
          index: true,
          element: <Home/>,
        },
        {
          path: 'casino',
          element: <Casino />,
        },
        {
          path: 'crash',
          element: <Crash />,
        },
        {
          path: 'bonuses',
          element: <Bonuses />,
        },
        {
          path: 'results',
          element: <Results />,
        },
        {
          path: 'my-bets',
          element: <MyBets />,
        },
        {
          path: 'my-account',
          element: <MyAccount />,
        },
        {
          path: 'wallet',
          element: <Wallet />,
        },
        {
          path: 'responsible-gambling',
          element: <ResponsibleGambling />,
        },
      ],
    },
    {
      path: '*',
      element: <Redirect to='/' />,
    },
  ];