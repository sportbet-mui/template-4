import React, { useEffect, useState } from 'react';
import { useRoutes } from 'react-router-dom';
import { desktopRoutes } from './constants';

export default function Routes() {
  const [routes, setRoutes] = useState([]);

  useEffect(() => {
    setRoutes(desktopRoutes);
  }, []);

  return useRoutes(routes);
}
