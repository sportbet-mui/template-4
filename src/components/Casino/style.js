import { Box, styled } from '@mui/material';

export const CasinoWrapper = styled(Box)(({ theme }) => {
    return {
        '& .banner-wrap': {
            marginBottom: theme.spacing(2),
            '& .banner-img': {
                width: theme.spacing('100%'),
                maxWidth: theme.spacing('100%'),
                height: theme.spacing('auto'),
            },
        },
        '& .game-providers-wrap': {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            flexWrap: 'wrap',
            margin: theme.spacing(-1, -1, 5),
            '& .logo-wrap': {
                width: theme.spacing('calc(100% / 7)'),
                padding: theme.spacing(1),
                [theme.breakpoints.down('xm')]: {
                    width: theme.spacing('calc(100% / 5)'),
                },
                [theme.breakpoints.down('md')]: {
                    width: theme.spacing('calc(100% / 4)'),
                },
                [theme.breakpoints.down('sm')]: {
                    width: theme.spacing('calc(100% / 3)'),
                },
            },
            '& .logo-box': {
                width: theme.spacing('100%'),
                height: theme.spacing('100%'),
                backgroundColor: theme.palette.primary.light,
                borderRadius: theme.spacing(1.25),
                boxShadow: theme.shadows[2],
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                overflow: 'hidden',
                position: 'relative',
                padding: theme.spacing(2),
                '&::before': {
                    content: '""',
                    display: 'block',
                    paddingTop: theme.spacing('100%'),
                },
                '& .logo-img': {
                    width: theme.spacing('100%'),
                    maxWidth: theme.spacing('100%'),
                    height: theme.spacing('auto'),
                },
            },
        },
    };
});

export const GamesCategoryWrapper = styled(Box)(({ theme }) => {
    return {
        '& .games-category-wrap': {
            '.game-wrap': {
                display: 'grid',
                gridGap: theme.spacing(2),
                gridAutoFlow: 'dense',
                gridTemplateColumns: '1.265fr 1.265fr 1fr 1fr 1fr 1fr 1fr',
                marginBottom: theme.spacing(5),
                [theme.breakpoints.down('md')]: {
                    gridTemplateColumns: '1fr 1fr 1fr 1fr 1fr',
                },
                [theme.breakpoints.down('sm')]: {
                    gridTemplateColumns: '1fr 1fr',
                },
                '& .MuiCard-root': {
                    backgroundColor: theme.palette.primary.light,
                    borderRadius: theme.spacing(1.25),
                    boxShadow: theme.shadows[2],
                    '& .MuiCardContent-root': {
                        padding: theme.spacing(1.5),
                        '& .MuiTypography-h6': {
                            color: theme.palette.primary.contrastTextLight,
                            fontSize: theme.spacing('1rem'),
                            fontWeight: theme.typography.fontWeightRegular,
                            whiteSpace: 'nowrap',
                            textOverflow: 'ellipsis',
                            overflow: 'hidden',
                            [theme.breakpoints.down('xl')]: {
                                fontSize: theme.spacing('0.875rem'),
                            },
                        },
                    },
                },
                '& .main-game-wrap': {
                    gridColumn: 'auto/span 2',
                    gridRow: 'auto/span 2',
                    [theme.breakpoints.down('md')]: {
                        gridColumn: 'auto/span 2',
                        gridRow: 'auto/span 2',
                    },
                },
                '& .side-game-wrap': {
                    display: 'grid',
                    gridGap: theme.spacing(2),
                    gridAutoFlow: 'dense',
                    gridColumn: 'auto/span 5',
                    gridRow: 'auto/span 2',
                    gridTemplateColumns: 'repeat(auto-fill, minmax(9rem, 1fr))',
                    gridAutoRows: 'minmax(9rem, auto)',
                    [theme.breakpoints.up('xl')]: {
                        gridTemplateColumns: '1fr 1fr 1fr 1fr 1fr',
                    },
                    [theme.breakpoints.down('ml')]: {
                        gridTemplateColumns: 'repeat(auto-fill, minmax(8.25rem, 1fr))',
                        gridAutoRows: 'minmax(8.25rem, auto)',
                    },
                    [theme.breakpoints.down('lg')]: {
                        gridTemplateColumns: 'repeat(auto-fill, minmax(8rem, 1fr))',
                        gridAutoRows: 'minmax(8rem, auto)',
                    },
                    [theme.breakpoints.down('md')]: {
                        gridColumn: 'auto/span 3',
                        gridRow: 'auto/span 2',
                    },
                    [theme.breakpoints.down('sm')]: {
                        gridColumn: 'auto/span 2',
                        gridRow: 'auto/span 2',
                    },
                },
            },
            '& .btn-box': {
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                margin: theme.spacing(0, 0, 5),
            },
        },
    };
});