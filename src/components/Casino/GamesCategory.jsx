import React from 'react'
import { GamesCategoryWrapper } from './style';
import { useTheme } from '@emotion/react'
import { Box, Card, CardContent, CardMedia, Typography } from '@mui/material';
import { HeadingWrapper, StyledHeadingTypography } from '../Common/StyledTypography/style';
import { CustomPrimaryButton } from '../Common/StyledButton/CustomButton';

export default function GamesCategory() {
  const theme = useTheme();

  return (
    <GamesCategoryWrapper theme={theme}>
      <Box className='games-category-wrap'>
        <HeadingWrapper>
          <StyledHeadingTypography>
            <img src={'assets/images/svg/sb-originals.svg'} className='heading-img' alt='Img'/>
            SB Originals
          </StyledHeadingTypography>
        </HeadingWrapper>
        <Box className='game-wrap'>
          <Box className='main-game-wrap'>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/sb-originals-img.png' alt='Img'/>
            </Card>
          </Box>
          <Box className='side-game-wrap'>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/sb-originals-img-01.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Dino Odyssey</Typography>
              </CardContent>
            </Card>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/sb-originals-img-02.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Dino Odyssey</Typography>
              </CardContent>
            </Card>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/sb-originals-img-03.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Dino Odyssey</Typography>
              </CardContent>
            </Card>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/sb-originals-img-04.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Dino Odyssey</Typography>
              </CardContent>
            </Card>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/sb-originals-img-05.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Dino Odyssey</Typography>
              </CardContent>
            </Card>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/sb-originals-img-06.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Dino Odyssey</Typography>
              </CardContent>
            </Card>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/sb-originals-img-07.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Dino Odyssey</Typography>
              </CardContent>
            </Card>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/sb-originals-img-08.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Dino Odyssey</Typography>
              </CardContent>
            </Card>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/sb-originals-img-09.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Dino Odyssey</Typography>
              </CardContent>
            </Card>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/sb-originals-img-10.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Dino Odyssey</Typography>
              </CardContent>
            </Card>
          </Box>
        </Box>
        <Box className='btn-box'>
          <CustomPrimaryButton>View All Games</CustomPrimaryButton>
        </Box>
      </Box>

      <Box className='games-category-wrap'>
        <HeadingWrapper>
          <StyledHeadingTypography>
            <img src={'assets/images/svg/slots.svg'} className='heading-img' alt='Img'/>
            Slots
          </StyledHeadingTypography>
        </HeadingWrapper>
        <Box className='game-wrap'>
          <Box className='main-game-wrap'>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/slots-img.png' alt='Img'/>
            </Card>
          </Box>
          <Box className='side-game-wrap'>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/slots-img-01.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Mermaids Galore</Typography>
              </CardContent>
            </Card>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/slots-img-02.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Mavi Millions</Typography>
              </CardContent>
            </Card>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/slots-img-03.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Monkey God</Typography>
              </CardContent>
            </Card>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/slots-img-04.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Pearls of Aphrodite</Typography>
              </CardContent>
            </Card>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/slots-img-05.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Pharaoh’s Reign</Typography>
              </CardContent>
            </Card>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/slots-img-06.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Sadie Swift</Typography>
              </CardContent>
            </Card>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/slots-img-07.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Super Size Atlas</Typography>
              </CardContent>
            </Card>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/slots-img-08.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Super Size Atlas</Typography>
              </CardContent>
            </Card>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/slots-img-09.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Super Size Atlas</Typography>
              </CardContent>
            </Card>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/slots-img-10.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Super Size Atlas</Typography>
              </CardContent>
            </Card>
          </Box>
        </Box>
        <Box className='btn-box'>
          <CustomPrimaryButton>View All Games</CustomPrimaryButton>
        </Box>
      </Box>

      <Box className='games-category-wrap'>
        <HeadingWrapper>
          <StyledHeadingTypography>
            <img src={'assets/images/svg/poker.svg'} className='heading-img' alt='Img'/>
            Poker
          </StyledHeadingTypography>
        </HeadingWrapper>
        <Box className='game-wrap'>
          <Box className='main-game-wrap'>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/poker-img.png' alt='Img'/>
            </Card>
          </Box>
          <Box className='side-game-wrap'>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/poker-img-01.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>3 Hand Casino Hold’em</Typography>
              </CardContent>
            </Card>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/poker-img-02.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>5x Magic</Typography>
              </CardContent>
            </Card>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/poker-img-03.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Classic Roulette</Typography>
              </CardContent>
            </Card>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/poker-img-04.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>High Hand Hold’em</Typography>
              </CardContent>
            </Card>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/poker-img-05.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Ace of Spades</Typography>
              </CardContent>
            </Card>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/poker-img-06.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Blackjack</Typography>
              </CardContent>
            </Card>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/poker-img-07.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Hold’em Poker</Typography>
              </CardContent>
            </Card>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/poker-img-08.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Hold’em Poker</Typography>
              </CardContent>
            </Card>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/poker-img-09.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Hold’em Poker</Typography>
              </CardContent>
            </Card>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/poker-img-10.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Hold’em Poker</Typography>
              </CardContent>
            </Card>
          </Box>
        </Box>
        <Box className='btn-box'>
          <CustomPrimaryButton>View All Games</CustomPrimaryButton>
        </Box>
      </Box>
    </GamesCategoryWrapper>
  )
}
