import React from 'react'
import { CasinoWrapper } from './style';
import { useTheme } from '@emotion/react'
import GamesCategory from './GamesCategory';
import { Box, Grid } from '@mui/material';
import GameNavbar from '../GameNavbar';
import { HeadingWrapper, StyledHeadingTypography } from '../Common/StyledTypography/style';

export default function Casino() {
  const theme = useTheme();

  return (
    <CasinoWrapper theme={theme}>
      <Box className='banner-wrap'>
        <img src={'assets/images/stock-images/casino-banner.png'} className='banner-img' alt='Banner Img' />
      </Box>
      <GameNavbar/>
      <GamesCategory/>
      <HeadingWrapper>
        <StyledHeadingTypography>
          <img src={'assets/images/svg/game-providers.svg'} className='heading-img' alt='Img'/>
          Game Providers
        </StyledHeadingTypography>
      </HeadingWrapper>
      <Box className='game-providers-wrap'>
        <Box className='logo-wrap'>
          <Box className='logo-box'>
            <img src={'assets/images/stock-images/game-providers-01.png'} className='logo-img' alt='Img' />
          </Box>
        </Box>
        <Box className='logo-wrap'>
          <Box className='logo-box'>
            <img src={'assets/images/stock-images/game-providers-02.png'} className='logo-img' alt='Img' />
          </Box>
        </Box>
        <Box className='logo-wrap'>
          <Box className='logo-box'>
            <img src={'assets/images/stock-images/game-providers-03.png'} className='logo-img' alt='Img' />
          </Box>
        </Box>
        <Box className='logo-wrap'>
          <Box className='logo-box'>
            <img src={'assets/images/stock-images/game-providers-04.png'} className='logo-img' alt='Img' />
          </Box>
        </Box>
        <Box className='logo-wrap'>
          <Box className='logo-box'>
            <img src={'assets/images/stock-images/game-providers-05.png'} className='logo-img' alt='Img' />
          </Box>
        </Box>
        <Box className='logo-wrap'>
          <Box className='logo-box'>
            <img src={'assets/images/stock-images/game-providers-06.png'} className='logo-img' alt='Img' />
          </Box>
        </Box>
        <Box className='logo-wrap'>
          <Box className='logo-box'>
            <img src={'assets/images/stock-images/game-providers-07.png'} className='logo-img' alt='Img' />
          </Box>
        </Box>
      </Box>
    </CasinoWrapper>
  )
}
