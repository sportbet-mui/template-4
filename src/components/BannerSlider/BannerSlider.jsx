import React from 'react'
import { BannerSliderWrapper } from './style';
import { Box, Grid, IconButton, useTheme } from '@mui/material';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css'; 
import 'slick-carousel/slick/slick-theme.css';

export default function BannerSlider() {
  const theme = useTheme();
  
  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: <IconButton><img src={'assets/images/svg/icon-caret-left.svg'} alt='Arrow Img' /></IconButton>,
    nextArrow: <IconButton><img src={'assets/images/svg/icon-caret-right.svg'} alt='Arrow Img' /></IconButton>,
  };
  
  return (
    <BannerSliderWrapper theme={theme}>
      <Grid container columnSpacing={2}>
        <Grid item lg={8} md={8} sm={8} xs={12}>
          <Slider {...settings}>
            <Box className='slider-img-box'>
              <img src={'assets/images/stock-images/slider-img-1.png'} alt='Slider Img' />
            </Box>
            <Box className='slider-img-box'>
              <img src={'assets/images/stock-images/slider-img-1.png'} alt='Slider Img' />
            </Box>
            <Box className='slider-img-box'>
              <img src={'assets/images/stock-images/slider-img-1.png'} alt='Slider Img' />
            </Box>
          </Slider>
        </Grid>
        <Grid item lg={4} md={4} sm={4} xs={12}>
          <Box className='img-box'>
            <img src={'assets/images/stock-images/discount-img.png'} alt='Img' />
          </Box>
        </Grid>
      </Grid>
    </BannerSliderWrapper>
  )
}
