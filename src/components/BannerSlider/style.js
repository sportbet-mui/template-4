import { Box, styled } from '@mui/material';

export const BannerSliderWrapper = styled(Box)(({ theme }) => {
    return {
        marginBottom: theme.spacing(1.25),
        '& .slick-slider': {
            '& .slick-arrow': {
                width: theme.spacing(6.75),
                height: theme.spacing(10.25),
                backgroundColor: theme.palette.lightTransparent.light_03,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                zIndex: '1',
                position: 'absolute',
                '&.slick-prev': {
                    borderRadius: theme.spacing(0, 1.25, 1.25, 0),
                    left: theme.spacing(0),
                },
                '&.slick-next': {
                    borderRadius: theme.spacing(1.25, 0, 0, 1.25),
                    right: theme.spacing(0),
                },
                '&::before': {
                    display: 'none',
                },
                [theme.breakpoints.down('md')]: {
                    width: theme.spacing(2.5),
                    height: theme.spacing(5),
                    padding: theme.spacing(0.5),
                    '& img': {
                        transform: 'scale(0.75)',
                    }
                },
            },
        },
        '& .slider-img-box': {
            borderRadius: theme.spacing(1.25),
            overflow: 'hidden',
            '& img': {
                width: theme.spacing('100%'),
                maxWidth: theme.spacing('100%'),
                height: theme.spacing('100%'),
                borderRadius: theme.spacing(1.25),
            },
        },
        '& .img-box': {
            borderRadius: theme.spacing(1.25),
            overflow: 'hidden',
            '& img': {
                width: theme.spacing('100%'),
                maxWidth: theme.spacing('100%'),
                height: theme.spacing('100%'),
                borderRadius: theme.spacing(1.25),
            },
        },
    };
});