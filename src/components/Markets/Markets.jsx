import React from 'react'
import { MarketsWrapper } from './style';
import MainMarkets from './MainMarkets';
import { Accordion, AccordionDetails, AccordionSummary, Box, Typography, useTheme } from '@mui/material';
import ArrowDropDownRoundedIcon from '@mui/icons-material/ArrowDropDownRounded';

export default function Markets() {
  const theme = useTheme();

  const [expanded, setExpanded] = React.useState('panel1');

  const handleChange = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };

  return (
    <MarketsWrapper theme={theme}>
      <Box className='accordion-wrap'>
        <Accordion expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
          <AccordionSummary
            expandIcon={<ArrowDropDownRoundedIcon />}
            aria-controls='panel1bh-content'
            id='panel1bh-header'
          >
            <Typography variant='h5'>England / Premier League</Typography>
          </AccordionSummary>
          <AccordionDetails>
            {Array(4).fill('').map((item)=>{
              return <MainMarkets/>
            })}
          </AccordionDetails>
        </Accordion>

        <Accordion expanded={expanded === 'panel2'} onChange={handleChange('panel2')}>
          <AccordionSummary
            expandIcon={<ArrowDropDownRoundedIcon />}
            aria-controls='panel2bh-content'
            id='panel2bh-header'
          >
            <Typography variant='h5'>Spain / LaLiga</Typography>
          </AccordionSummary>
          <AccordionDetails>
            {Array(4).fill('').map((item)=>{
              return <MainMarkets/>
            })}
          </AccordionDetails>
        </Accordion>
      </Box>
    </MarketsWrapper>
  )
}
