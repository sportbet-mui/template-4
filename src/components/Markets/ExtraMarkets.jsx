import React from 'react'
import { ExtraMarketsWrapper } from './style';
import { useTheme } from '@emotion/react'
import { Accordion, AccordionDetails, AccordionSummary, Box, Grid, Typography } from '@mui/material';
import ArrowDropDownRoundedIcon from '@mui/icons-material/ArrowDropDownRounded';
import LockRoundedIcon from '@mui/icons-material/LockRounded';

export default function ExtraMarkets({expand}) {
  const theme = useTheme();

  return (
    <ExtraMarketsWrapper className={`${expand ? 'expend-wrap' :  'shrink-wrap'}`} theme={theme}>
      <Box className='extra-markets-wrap'>
        <Box className='accordion-wrap'>
          <Accordion>
            <AccordionSummary
              expandIcon={<ArrowDropDownRoundedIcon />}
              aria-controls="panel1bh-content"
              id="panel1bh-header"
            >
              <Typography variant='h5'>1x2</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Grid container spacing={2}>
                <Grid item xs>
                  <Box className='market-box'>
                    <Typography variant='body1'>Chelsea</Typography>
                    <Typography variant='body2'>1.67</Typography>
                  </Box>
                </Grid>
                <Grid item xs>
                  <Box className='market-box'>
                    <Typography variant='body1'>Draw</Typography>
                    <Typography variant='body2'>1.67</Typography>
                  </Box>
                </Grid>
                <Grid item xs>
                  <Box className='market-box'>
                    <Typography variant='body1'>Draw</Typography>
                    <Typography variant='body2'>1.67</Typography>
                  </Box>
                </Grid>
              </Grid>
            </AccordionDetails>
          </Accordion>

          <Accordion>
            <AccordionSummary
              expandIcon={<ArrowDropDownRoundedIcon />}
              aria-controls="panel2bh-content"
              id="panel2bh-header"
            >
              <Typography variant='h5'>Double Chance</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Grid container spacing={2}>
                <Grid item xs>
                  <Box className='market-box'>
                    <Typography variant='body1'>Chelsea or Draw</Typography>
                    <Typography variant='body2'>1.67</Typography>
                  </Box>
                </Grid>
                <Grid item xs>
                  <Box className='market-box'>
                    <Typography variant='body1'>Chelsea or Fulham</Typography>
                    <Typography variant='body2'>1.67</Typography>
                  </Box>
                </Grid>
                <Grid item xs>
                  <Box className='market-box'>
                    <Typography variant='body1'>Draw or Fulham</Typography>
                    <Typography variant='body2'>1.67</Typography>
                  </Box>
                </Grid>
              </Grid>
            </AccordionDetails>
          </Accordion>

          <Accordion>
            <AccordionSummary
              expandIcon={<ArrowDropDownRoundedIcon />}
              aria-controls="panel3bh-content"
              id="panel3bh-header"
            >
              <Typography variant='h5'>Total</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Grid container spacing={2}>
                <Grid item xs>
                  <Box className='market-box'>
                    <Typography variant='body1'>Over 1.5</Typography>
                    <Typography variant='body2'>1.67</Typography>
                  </Box>
                </Grid>
                <Grid item xs>
                  <Box className='market-box'>
                    <Typography variant='body1'>Under 1.5</Typography>
                    <Typography variant='body2'>1.67</Typography>
                  </Box>
                </Grid>
              </Grid>
            </AccordionDetails>
          </Accordion>

          <Accordion>
            <AccordionSummary
              expandIcon={<ArrowDropDownRoundedIcon />}
              aria-controls="panel4bh-content"
              id="panel4bh-header"
            >
              <Typography variant='h5'>Draw No Bet</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Grid container spacing={2}>
                <Grid item xs>
                  <Box className='market-box'>
                    <LockRoundedIcon/>
                  </Box>
                </Grid>
              </Grid>
            </AccordionDetails>
          </Accordion>

          <Accordion>
            <AccordionSummary
              expandIcon={<ArrowDropDownRoundedIcon />}
              aria-controls="panel5bh-content"
              id="panel5bh-header"
            >
              <Typography variant='h5'>Handicap</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Grid container spacing={2}>
                <Grid item xs>
                  <Box className='market-box'>
                    <LockRoundedIcon/>
                  </Box>
                </Grid>
              </Grid>
            </AccordionDetails>
          </Accordion>

          <Accordion>
            <AccordionSummary
              expandIcon={<ArrowDropDownRoundedIcon />}
              aria-controls="panel6bh-content"
              id="panel6bh-header"
            >
              <Typography variant='h5'>Halftime / Fulltime</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Grid container spacing={2}>
                <Grid item xs>
                  <Box className='market-box'>
                    <LockRoundedIcon/>
                  </Box>
                </Grid>
              </Grid>
            </AccordionDetails>
          </Accordion>

          <Accordion>
            <AccordionSummary
              expandIcon={<ArrowDropDownRoundedIcon />}
              aria-controls="panel7bh-content"
              id="panel7bh-header"
            >
              <Typography variant='h5'>Odd / Even</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Grid container spacing={2}>
                <Grid item xs>
                  <Box className='market-box'>
                    <LockRoundedIcon/>
                  </Box>
                </Grid>
              </Grid>
            </AccordionDetails>
          </Accordion>
        </Box>
      </Box>
    </ExtraMarketsWrapper>
  )
}
