import React, { useState } from 'react'
import { MainMarketsWrapper } from './style';
import ExtraMarkets from './ExtraMarkets';
import { useTheme } from '@emotion/react'
import { Box, Grid, Typography } from '@mui/material';
import { CustomTextButton } from '../Common/StyledButton/CustomButton';
import StarOutlineRoundedIcon from '@mui/icons-material/StarOutlineRounded';

export default function MainMarkets() {
  const theme = useTheme();
  
  const [showExtraMarket, setShowExtraMarket] = useState(false)

  return (
    <MainMarketsWrapper theme={theme}>
      <Box className='accordion-details-wrap'>
        <Box className='markets-content-wrap'>
          <Box className='match-details-wrap'>
            <Typography variant='subtitle1'>Feb 4, 1:30 AM – in 8 hours</Typography>
            <Box className='score-box'>
              <Typography variant='body1'>
                <span>Chelsea</span>
                <span>0</span>
              </Typography>
              <Typography variant='body1'>
                <span>Fulham</span>
                <span>0</span>
              </Typography>
            </Box>
          </Box>
          <Box className='match-markets-wrap'>
            <Typography variant='subtitle1'>1x2</Typography>
            <Grid container columnSpacing={{ xs: 1, sm: 2 }}>
              <Grid item sm={4} xs={4}>
                <Box className='market-box active'>
                  <Typography variant='body1'>Chelsea</Typography>
                  <Typography variant='body2'>1.67</Typography>
                </Box>
              </Grid>
              <Grid item sm={4} xs={4}>
                <Box className='market-box'>
                  <Typography variant='body1'>Draw</Typography>
                  <Typography variant='body2'>1.67</Typography>
                </Box>
              </Grid>
              <Grid item sm={4} xs={4}>
                <Box className='market-box'>
                  <Typography variant='body1'>Draw</Typography>
                  <Typography variant='body2'>1.67</Typography>
                </Box>
              </Grid>
            </Grid>
          </Box>
          <Box className='more-markets-wrap'>
            <CustomTextButton className='favorite-btn'><StarOutlineRoundedIcon/></CustomTextButton>
            <CustomTextButton className={`${showExtraMarket ? 'active' : ''}`} onClick={()=>{setShowExtraMarket(prev=> prev? false: true)}}>+50 Markets</CustomTextButton>
          </Box>
        </Box>
        <ExtraMarkets expand={showExtraMarket}/>
      </Box>
    </MainMarketsWrapper>
  )
}
