import { Box, styled } from '@mui/material';

export const MarketsWrapper = styled(Box)(({ theme }) => {
    return {
        marginTop: theme.spacing(2),
        '& .accordion-wrap': {
            '& .MuiAccordion-root': {
                backgroundColor: theme.palette.primary.transparent,
                borderRadius: theme.spacing(0),
                boxShadow: theme.shadows[0],
                marginBottom: theme.spacing(2),
                '&::before': {
                    display: 'none',
                },
                '&.Mui-expanded': {
                    marginBottom: theme.spacing(2),
                },
                '& .MuiAccordionSummary-root': {
                    backgroundColor: theme.palette.primary.light,
                    borderRadius: theme.spacing(1.25, 1.25, 0, 0),
                    padding: theme.spacing(0.1675, 2),
                    '&.Mui-expanded': {
                        minHeight: theme.spacing(6),
                    },
                    '& .MuiAccordionSummary-content': {
                        '& .MuiTypography-h5': {
                            color: theme.palette.primary.contrastText,
                            fontSize: theme.spacing('1rem'),
                            fontWeight: theme.typography.fontWeightMedium,
                        },
                        '&.Mui-expanded': {
                            margin: theme.spacing(1.5, 0),
                        },
                    },
                },
                '& .MuiAccordionSummary-expandIconWrapper': {
                    '& .MuiSvgIcon-root': {
                        fill: theme.palette.primary.contrastText,
                        transform: 'scale(1.5)',
                    },
                },
                '& .MuiCollapse-root': {
                    '& .MuiAccordionDetails-root': {
                        padding: theme.spacing(0),
                    },
                },
            },
        },
    };
});

export const MainMarketsWrapper = styled(Box)(({ theme }) => {
    return {
        backgroundColor: theme.palette.secondary.dark,
        padding: theme.spacing(0),
        '& .markets-content-wrap': {
            borderWidth: theme.spacing(0, 0, 0.125, 0),
            borderStyle: 'solid',
            borderColor: theme.palette.border.dark,
            display: 'flex',
            justifyContent: 'space-between',
            padding: theme.spacing(2),
            '& .match-details-wrap': {
                minWidth: theme.spacing(27.5),
                '& .MuiTypography-subtitle1': {
                    color: theme.palette.primary.contrastTextLight,
                    fontSize: theme.spacing('0.75rem'),
                    fontWeight: theme.typography.fontWeightRegular,
                },
                '& .score-box': {
                    height: theme.spacing('calc(100% - 32px)'),
                    borderWidth: theme.spacing(0, 0.125, 0, 0),
                    borderStyle: 'solid',
                    borderColor: theme.palette.border.main,
                    paddingRight: theme.spacing(2),
                    marginTop: theme.spacing(1.5),
                    '& .MuiTypography-body1': {
                        color: theme.palette.primary.contrastText,
                        fontSize: theme.spacing('0.875rem'),
                        fontWeight: theme.typography.fontWeightMedium,
                        display: 'flex',
                        justifyContent: 'space-between',
                        marginBottom: theme.spacing(1),
                        '&:last-child': {
                            marginBottom: theme.spacing(0),
                        },
                    },
                },
            },
            '& .match-markets-wrap': {
                width: theme.spacing('100%'),
                padding: theme.spacing(0, 2),
                '& .MuiTypography-subtitle1': {
                    color: theme.palette.primary.contrastTextLight,
                    fontSize: theme.spacing('0.875rem'),
                    fontWeight: theme.typography.fontWeightRegular,
                    textAlign: 'center',
                },
                '& .market-box': {
                    backgroundColor: theme.palette.primary.dark,
                    borderRadius: theme.spacing(0.625),
                    display: 'flex',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    cursor: 'pointer',
                    padding: theme.spacing(1.5),
                    marginTop: theme.spacing(1.5),
                    '& .MuiTypography-body1, & .MuiTypography-body2': {
                        color: theme.palette.primary.contrastText,
                        fontSize: theme.spacing('0.875rem'),
                        fontWeight: theme.typography.fontWeightRegular,
                    },
                    '& .MuiTypography-body2': {
                        color: theme.palette.warning.main,
                        marginLeft: theme.spacing(2),
                    },
                    '& .MuiSvgIcon-root': {
                        fill: theme.palette.primary.contrastText,
                        margin: theme.spacing('auto'),
                    },
                    '&.active': {
                        backgroundColor: theme.palette.button.main,
                        '& .MuiTypography-body2': {
                            color: theme.palette.primary.contrastText,
                        },
                    }
                },
            },
            '& .more-markets-wrap': {
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'flex-end',
                flexDirection: 'column',
                '& .MuiButton-text': {
                    color: theme.palette.primary.contrastTextLight,
                    fontSize: theme.spacing('0.875rem'),
                    fontWeight: theme.typography.fontWeightRegular,
                    whiteSpace: 'nowrap',
                    padding: theme.spacing(0),
                    '&:hover, &.active': {
                        color: theme.palette.button.main,
                    },
                },
                '& .favorite-btn': {
                    width: theme.spacing('auto'),
                    minWidth: theme.spacing('auto'),
                    height: theme.spacing('auto'),
                    '& svg': {
                        width: theme.spacing(2.5),
                        height: theme.spacing(2.5),
                        fill: theme.palette.primary.contrastTextLight,
                    },
                },
            },
            [theme.breakpoints.down('xm')]: {
                '& .match-details-wrap': {
                    minWidth: theme.spacing(21.25),
                },
                '& .match-markets-wrap': {
                    '& .market-box': {
                        flexDirection: 'column',
                        padding: theme.spacing(1),
                        marginTop: theme.spacing(1),
                        '& .MuiTypography-body1, & .MuiTypography-body2': {
                            textAlign: 'center',
                        },
                        '& .MuiTypography-body2': {
                            marginLeft: theme.spacing(0),
                        }
                    },
                },
            },
            [theme.breakpoints.down('md')]: {
                flexWrap: 'wrap',
                '& .match-details-wrap': {
                    order: '1',
                },
                '& .match-markets-wrap': {
                    order: '3',
                    padding: theme.spacing(0),
                    marginTop: theme.spacing(2),
                },
                '& .more-markets-wrap': {
                    order: '2',
                },
            },
            [theme.breakpoints.down('sm')]: {
                '& .match-details-wrap': {
                    minWidth: theme.spacing(25),
                },
                '& .match-markets-wrap': {
                    '& .market-box': {
                        flexDirection: 'column',
                        padding: theme.spacing(1),
                        marginTop: theme.spacing(1),
                        '& .MuiTypography-body1, & .MuiTypography-body2': {
                            textAlign: 'center',
                        },
                        '& .MuiTypography-body2': {
                            marginLeft: theme.spacing(0),
                        }
                    },
                },
            },
        },
    };
});

export const ExtraMarketsWrapper = styled(Box)(({ theme }) => {
    return {
        '&.shrink-wrap': {
            maxHeight: theme.spacing(0),
            minHeight: theme.spacing(0),
            overflow: 'hidden',
            transition: '0.5s all',
        },
        '&.expend-wrap': {
            minHeight: theme.spacing(0),
            maxHeight: theme.spacing('1000vh'),
            overflow: 'auto',
            transition: '1.5s all',
        },
        '& .extra-markets-wrap': {
            '& .accordion-wrap': {
                '& .MuiAccordion-root': {
                    backgroundColor: theme.palette.primary.light,
                    borderWidth: theme.spacing(0, 0, 0.125, 0),
                    borderStyle: 'solid',
                    borderColor: theme.palette.border.dark,
                    borderRadius: theme.spacing(0),
                    boxShadow: theme.shadows[0],
                    margin: theme.spacing(0),
                    '&::before': {
                        display: 'none',
                    },
                    '& .MuiAccordionSummary-root': {
                        borderRadius: theme.spacing(0),
                        padding: theme.spacing(0.1675, 2),
                        '&.Mui-expanded': {
                            minHeight: theme.spacing(6),
                        },
                        '& .MuiAccordionSummary-content': {
                            justifyContent: 'center',
                            '& .MuiTypography-h5': {
                                color: theme.palette.primary.contrastTextLight,
                                fontSize: theme.spacing('0.875rem'),
                                fontWeight: theme.typography.fontWeightRegular,
                                textAlign: 'center',
                            },
                            '&.Mui-expanded': {
                                margin: theme.spacing(1.5, 0),
                            },
                        },
                    },
                    '& .MuiAccordionSummary-expandIconWrapper': {
                        '& .MuiSvgIcon-root': {
                            fill: theme.palette.primary.contrastTextLight,
                            transform: 'scale(1)',
                        },
                    },
                    '& .MuiCollapse-root': {
                        '& .MuiAccordionDetails-root': {
                            padding: theme.spacing(0, 2, 2),
                            '& .market-box': {
                                backgroundColor: theme.palette.primary.dark,
                                borderRadius: theme.spacing(0.625),
                                display: 'flex',
                                justifyContent: 'space-between',
                                alignItems: 'center',
                                cursor: 'pointer',
                                padding: theme.spacing(1.5),
                                '& .MuiTypography-body1, & .MuiTypography-body2': {
                                    color: theme.palette.primary.contrastText,
                                    fontSize: theme.spacing('0.875rem'),
                                    fontWeight: theme.typography.fontWeightRegular,
                                },
                                '& .MuiTypography-body2': {
                                    color: theme.palette.warning.main,
                                    marginLeft: theme.spacing(2),
                                },
                                '& .MuiSvgIcon-root': {
                                    fill: theme.palette.primary.contrastText,
                                    margin: theme.spacing('auto'),
                                },
                                '&.active': {
                                    backgroundColor: theme.palette.button.main,
                                    '& .MuiTypography-body2': {
                                        color: theme.palette.primary.contrastText,
                                    },
                                }
                            },
                        },
                    },
                },
            },
        },
    };
});