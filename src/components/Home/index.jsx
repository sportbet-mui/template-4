import React from 'react'
import { HomeWrapper } from './style';
import { Grid, useTheme } from '@mui/material';
import BannerSlider from '../BannerSlider/BannerSlider';
import GamesMenu from '../GamesMenu/GamesMenu';
import Betslip from '../Betslip/Betslip';
import GameNavbar from '../GameNavbar';
import Markets from '../Markets/Markets';

export default function Home() {
  const theme = useTheme();
  return (
    <HomeWrapper theme={theme}>
      <BannerSlider/>
      <Grid container spacing={2}>
        <Grid item className='GamesMenu-column'>
          <GamesMenu/>
        </Grid>
        <Grid item className='Markets-column'>
          <GameNavbar/>
          <Markets/>
        </Grid>
        <Grid item className='Betslip-column'>
          <Betslip/>
        </Grid>
      </Grid>
    </HomeWrapper>
  )
}
