import { Box, styled } from '@mui/material';

export const HomeWrapper = styled(Box)(({ theme }) => {
    return {
        '& .GamesMenu-column': {
            width: theme.spacing('100%'),
            [theme.breakpoints.up('md')]: {
                maxWidth: theme.spacing(37.25),
                minWidth: theme.spacing(37.25),
            },
        },
        '& .Markets-column': {
            width: theme.spacing('100%'),
            [theme.breakpoints.up('md')]: {
                width: theme.spacing('calc(100% - 298px)'),
            },
            [theme.breakpoints.up('xl')]: {
                width: theme.spacing('calc(100% - 591px)'),
            },
        },
        '& .Betslip-column': {
            width: theme.spacing('100%'),
            [theme.breakpoints.up('xl')]: {
                maxWidth: theme.spacing(36.625),
                minWidth: theme.spacing(36.625),
            },
        },
    };
});