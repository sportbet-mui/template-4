import React, { useState } from 'react'
import { BetLimitWrapper } from './style';
import { useTheme } from '@emotion/react'
import { Box, Grid, MenuItem } from '@mui/material';
import { CustomInputLabel, CustomSelectField } from '../../Common/StyledForm/style';
import { CustomMainButton, CustomPrimaryButton } from '../../Common/StyledButton/CustomButton';

export default function BetLimit() {
  const theme = useTheme();

  const [selectTypeSingleLimit,setTypeSingleLimit]=useState(1);

  const [selectTypeDailyLimit,setTypeDailyLimit]=useState(1);

  const [selectTypeWeeklyLimit,setTypeWeeklyLimit]=useState(1);

  const [selectTypeMonthlyLimit,setTypeMonthlyLimit]=useState(1);

  return (
    <BetLimitWrapper theme={theme}>
      <Grid container spacing={3}>
        <Grid item sm={6} xs={12}>
          <CustomInputLabel>Single Limit</CustomInputLabel>
          <CustomSelectField value={selectTypeSingleLimit} onChange={setTypeSingleLimit}>
            <MenuItem value={selectTypeSingleLimit}>Single Limit 1</MenuItem>
            <MenuItem value={selectTypeSingleLimit}>Single Limit 2</MenuItem>
            <MenuItem value={selectTypeSingleLimit}>Single Limit 3</MenuItem>
          </CustomSelectField>
        </Grid>
        <Grid item sm={6} xs={12}>
          <CustomInputLabel>Daily Limit</CustomInputLabel>
          <CustomSelectField value={selectTypeDailyLimit} onChange={setTypeDailyLimit}>
            <MenuItem value={selectTypeDailyLimit}>Daily Limit 1</MenuItem>
            <MenuItem value={selectTypeDailyLimit}>Daily Limit 2</MenuItem>
            <MenuItem value={selectTypeDailyLimit}>Daily Limit 3</MenuItem>
          </CustomSelectField>
        </Grid>
        <Grid item sm={6} xs={12}>
          <CustomInputLabel>Weekly Limit</CustomInputLabel>
          <CustomSelectField value={selectTypeWeeklyLimit} onChange={setTypeWeeklyLimit}>
            <MenuItem value={selectTypeWeeklyLimit}>Weekly Limit 1</MenuItem>
            <MenuItem value={selectTypeWeeklyLimit}>Weekly Limit 2</MenuItem>
            <MenuItem value={selectTypeWeeklyLimit}>Weekly Limit 3</MenuItem>
          </CustomSelectField>
        </Grid>
        <Grid item sm={6} xs={12}>
          <CustomInputLabel>Monthly Limit</CustomInputLabel>
          <CustomSelectField value={selectTypeMonthlyLimit} onChange={setTypeMonthlyLimit}>
            <MenuItem value={selectTypeMonthlyLimit}>Monthly Limit 1</MenuItem>
            <MenuItem value={selectTypeMonthlyLimit}>Monthly Limit 2</MenuItem>
            <MenuItem value={selectTypeMonthlyLimit}>Monthly Limit 3</MenuItem>
          </CustomSelectField>
        </Grid>
      </Grid>
      <Box className='form-btn-wrap'>
        <Grid container spacing={2}>
          <Grid item lg={3} md={4} sm={6} xs={6}>
            <CustomPrimaryButton fullWidth>Cancel</CustomPrimaryButton>
          </Grid>
          <Grid item lg={3} md={4} sm={6} xs={6}>
            <CustomMainButton fullWidth>Save</CustomMainButton>
          </Grid>
        </Grid>
      </Box>
    </BetLimitWrapper>
  )
}
