import React, { useState } from 'react'
import { SelfExclusionWrapper } from './style';
import { useTheme } from '@emotion/react'
import { Box, Grid, MenuItem, Typography } from '@mui/material';
import { CustomInputLabel, CustomSelectField } from '../../Common/StyledForm/style';
import { CustomMainButton, CustomPrimaryButton } from '../../Common/StyledButton/CustomButton';

export default function SelfExclusion() {
  const theme = useTheme();

  const [selectTypePrevent,setTypePrevent]=useState(1);

  return (
    <SelfExclusionWrapper theme={theme}>
    <Typography variant='subtitle1'>You’ll be restricted from logging-in for the time you select here!</Typography>
      <Grid container spacing={3}>
        <Grid item sm={12} xs={12}>
          <CustomInputLabel>Prevent me to login for</CustomInputLabel>
          <CustomSelectField value={selectTypePrevent} onChange={setTypePrevent}>
            <MenuItem value={selectTypePrevent}>1 Minute</MenuItem>
            <MenuItem value={selectTypePrevent}>2 Minute</MenuItem>
            <MenuItem value={selectTypePrevent}>3 Minute</MenuItem>
          </CustomSelectField>
        </Grid>
      </Grid>
      <Box className='form-btn-wrap'>
        <Grid container spacing={2}>
          <Grid item lg={3} md={4} sm={6} xs={6}>
            <CustomPrimaryButton fullWidth>Cancel</CustomPrimaryButton>
          </Grid>
          <Grid item lg={3} md={4} sm={6} xs={6}>
            <CustomMainButton fullWidth>Save</CustomMainButton>
          </Grid>
        </Grid>
      </Box>
    </SelfExclusionWrapper>
  )
}
