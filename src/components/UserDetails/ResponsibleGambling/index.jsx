import React from 'react'
import { ResponsibleGamblingWrapper } from './style';
import { useTheme } from '@emotion/react'
import UserTabs from '../UserTabs';
import { Box, Container, Tab, Tabs } from '@mui/material';
import { ContentWrapper, UserSubTabsWrapper } from '../style';
import DepositLimit from './DepositLimit';
import BetLimit from './BetLimit';
import SelfExclusion from './SelfExclusion';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role='tabpanel'
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box className='tab-panel'>
          {children}
        </Box>
      )}
    </div>
  );
}

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

export default function ResponsibleGambling() {
  const theme = useTheme();

  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <ResponsibleGamblingWrapper theme={theme}>
      <UserTabs/>

      <Container maxWidth='lg'>
        <UserSubTabsWrapper theme={theme}>
          <Tabs value={value} onChange={handleChange} variant='scrollable' scrollButtons allowScrollButtonsMobile aria-label='basic tabs example'>
            <Tab label='Deposit Limit' {...a11yProps(0)} />
            <Tab label='Bet Limit' {...a11yProps(1)} />
            <Tab label='Self Exclusion' {...a11yProps(2)} />
          </Tabs>
        </UserSubTabsWrapper>

        <TabPanel value={value} index={0}>
          <ContentWrapper theme={theme}>
            <DepositLimit/>
          </ContentWrapper>
        </TabPanel>

        <TabPanel value={value} index={1}>
          <ContentWrapper theme={theme}>
          <BetLimit/>
          </ContentWrapper>
        </TabPanel>

        <TabPanel value={value} index={2}>
          <ContentWrapper theme={theme}>
            <SelfExclusion/>
          </ContentWrapper>
        </TabPanel>
      </Container>
    </ResponsibleGamblingWrapper>
  )
}
