import React from 'react'
import { WalletWrapper } from './style';
import { useTheme } from '@emotion/react'
import UserTabs from '../UserTabs';
import { ContentWrapper, UserSubTabsWrapper } from '../style';
import { Box, Container, Tab, Tabs } from '@mui/material';
import Deposit from './Deposit';
import Withdraw from './Withdraw';
import AllTransactions from './AllTransactions';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role='tabpanel'
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box className='tab-panel'>
          {children}
        </Box>
      )}
    </div>
  );
}

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

export default function Wallet() {
  const theme = useTheme();

  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <WalletWrapper theme={theme}>
      <UserTabs/>

      <Container maxWidth='lg'>
        <UserSubTabsWrapper theme={theme}>
          <Tabs value={value} onChange={handleChange} variant='scrollable' scrollButtons allowScrollButtonsMobile aria-label='basic tabs example'>
            <Tab label='Deposit' {...a11yProps(0)} />
            <Tab label='Withdraw' {...a11yProps(1)} />
            <Tab label='All Transactions' {...a11yProps(2)} />
          </Tabs>
        </UserSubTabsWrapper>

        <TabPanel value={value} index={0}>
          <ContentWrapper theme={theme}>
            <Deposit/>
          </ContentWrapper>
        </TabPanel>

        <TabPanel value={value} index={1}>
          <ContentWrapper theme={theme}>
            <Withdraw/>
          </ContentWrapper>
        </TabPanel>

        <TabPanel value={value} index={2}>
          <AllTransactions/>
        </TabPanel>
      </Container>
    </WalletWrapper>
  )
}
