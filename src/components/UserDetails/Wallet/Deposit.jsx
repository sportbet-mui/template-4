import React, { useState } from 'react'
import { DepositWrapper } from './style';
import { useTheme } from '@emotion/react'
import { Box, Grid, IconButton, InputAdornment, List, ListItem, ListItemText, MenuItem } from '@mui/material';
import { CustomInputLabel, CustomSelectField, CustomTextIconField, IconInput } from '../../Common/StyledForm/style';
import ContentCopyRoundedIcon from '@mui/icons-material/ContentCopyRounded';

export default function Deposit() {
  const theme = useTheme();

  const [selectType,setType]=useState(1);

  return (
    <DepositWrapper theme={theme}>
      <Grid container spacing={3}>
        <Grid item sm={12} xs={12}>
          <CustomSelectField value={selectType} onChange={setType}>
            <MenuItem value={selectType}>
              <Box className='select-content'>
                <img src='assets/images/stock-images/currency-ltc.png' className='coin-img' alt='Coin' />
                Bit Coin
              </Box>
            </MenuItem>
            <MenuItem value={selectType}>
              <Box className='select-content'>
                <img src='assets/images/stock-images/currency-ltc.png' className='coin-img' alt='Coin' />
                Lite Coin
              </Box>
            </MenuItem>
          </CustomSelectField>
        </Grid>
        <Grid item sm={12} xs={12}>
          <CustomInputLabel>Your LTC deposit address</CustomInputLabel>
          <CustomTextIconField>
            <IconInput
              id='outlined-adornment-weight'
              endAdornment={
                <InputAdornment position='end'>
                  <IconButton
                    aria-label='Icon Button'
                    edge='end'
                  >
                    <ContentCopyRoundedIcon/>
                  </IconButton>
                </InputAdornment>
              }
              inputProps={{
                'aria-label': 'icon',
              }}
            />
          </CustomTextIconField>
        </Grid>
        <Grid item lg={2} md={3} sm={3} xs={6}>
          <Box className='qr-code-box'>
            <img src='assets/images/stock-images/qr-code.png' className='qr-code' alt='QR Code' />
          </Box>
        </Grid>
        <Grid item lg={10} md={9} sm={9} xs={12}>
          <List>
            <ListItem>
              <ListItemText primary='Only send LTC to this address'/>
            </ListItem>
            <ListItem>
              <ListItemText primary='Please do NOT deposit via cross-chain transfer'/>
            </ListItem>
            <ListItem>
              <ListItemText primary='The minimum deposit amount 0.00005 LTC, lower amount wont be credited or refunded.'/>
            </ListItem>
          </List>
        </Grid>
      </Grid>
    </DepositWrapper>
  )
}
