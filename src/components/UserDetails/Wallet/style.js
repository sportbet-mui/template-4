import { Box, styled } from '@mui/material';

export const WalletWrapper = styled(Box)(({ theme }) => {
    return {
    };
});

export const DepositWrapper = styled(Box)(({ theme }) => {
    return {
        '& .MuiInputBase-root': {
            '& .MuiSelect-select': {
                '& .select-content': {
                    display: 'flex',
                    alignItems: 'center',
                    '& .coin-img': {
                        width: theme.spacing(3.5),
                        height: theme.spacing(3.5),
                        marginRight: theme.spacing(2),
                    },
                },
            },
            '& .MuiSvgIcon-root': {
                fill: theme.palette.primary.contrastTextLight,
            },
        },
        '& .MuiFormControl-root': {
            '& .MuiInputBase-root': {
                '& .MuiInputAdornment-root': {
                    '& .MuiButtonBase-root': {
                        '& .MuiSvgIcon-root': {
                            fill: theme.palette.primary.contrastTextLight,
                        },
                    },
                },
            },
        },
        '& .qr-code-box': {
            backgroundColor: theme.palette.white.main,
            borderRadius: theme.spacing(1.25),
            padding: theme.spacing(2),
            '& .qr-code': {
                width: theme.spacing('100%'),
                maxWidth: theme.spacing('100%'),
                height: theme.spacing('auto'),
            },
        },
    };
});

export const WithdrawWrapper = styled(Box)(({ theme }) => {
    return {
    };
});

export const AllTransactionsWrapper = styled(Box)(({ theme }) => {
    return {
    };
});