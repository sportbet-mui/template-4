import React from 'react'
import { WithdrawWrapper } from './style';
import { useTheme } from '@emotion/react'
import { Box, Grid, List, ListItem, ListItemText, Typography } from '@mui/material';
import { CustomInputLabel, CustomTextField } from '../../Common/StyledForm/style';
import { CustomMainButton, CustomTextButton } from '../../Common/StyledButton/CustomButton';

export default function Withdraw() {
  const theme = useTheme();

  return (
    <WithdrawWrapper theme={theme}>
      <Grid container spacing={3}>
        <Grid item sm={6} xs={12}>
          <CustomInputLabel>Amount (Min. 0.03 LTC)</CustomInputLabel>
          <CustomTextField placeholder='Enter Withdraw Amount' />
        </Grid>
        <Grid item sm={6} xs={12}>
          <CustomInputLabel>Your ERC20 address</CustomInputLabel>
          <CustomTextField placeholder='Enter Your Address' />
        </Grid>
        <Grid item sm={12} xs={12}>
          <List>
            <ListItem>
              <ListItemText primary='Please ensure that you do NOT input BEP2, BEP20 (BSC) address as a LTC withdraw address wallets'/>
            </ListItem>
            <ListItem>
              <ListItemText primary='Please do NOT withdraw as cross-chain transfer'/>
            </ListItem>
          </List>
        </Grid>
      </Grid>
      <Box className='form-btn-wrap'>
        <Grid container spacing={2}>
          <Grid item lg={3} md={4} sm={6} xs={6}>
            <CustomMainButton fullWidth>Withdraw Request</CustomMainButton>
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          <Grid item lg={12} md={12} sm={12} xs={12}>
            <Box className='withdrawal-amount-box'>
              <CustomInputLabel>Withdrawal Fee:</CustomInputLabel>
              <CustomTextButton>0.000006 LTC</CustomTextButton>
            </Box>
          </Grid>
        </Grid>
      </Box>
    </WithdrawWrapper>
  )
}
