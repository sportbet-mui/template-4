import React, { useState } from 'react'
import { AllTransactionsWrapper } from './style';
import { useTheme } from '@emotion/react'
import { FilterWrapper, TableWrapper } from '../../Common/style';
import { Chip, Grid, MenuItem, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@mui/material';
import { CustomDatePicker, CustomInputLabel, CustomSelectField } from '../../Common/StyledForm/style';
import { DatePicker, LocalizationProvider } from '@mui/x-date-pickers';
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { CustomMainButton, CustomPrimaryButton, CustomTextButton } from '../../Common/StyledButton/CustomButton';

export default function AllTransactions() {
  const theme = useTheme();

  const [selectTypeType,setTypeType]=useState(1);

  return (
    <AllTransactionsWrapper theme={theme}>
      <FilterWrapper theme={theme}>
        <Grid container spacing={2}>
          <Grid item className='filter-content-box'>
            <CustomInputLabel>Type</CustomInputLabel>
            <CustomSelectField value={selectTypeType} onChange={setTypeType}>
              <MenuItem value={selectTypeType}>Type 1</MenuItem>
              <MenuItem value={selectTypeType}>Type 2</MenuItem>
              <MenuItem value={selectTypeType}>Type 3</MenuItem>
            </CustomSelectField>
          </Grid>
          <Grid item className='filter-content-box'>
            <CustomInputLabel>Date</CustomInputLabel>
            <CustomDatePicker>
              <LocalizationProvider dateAdapter={AdapterDayjs}>
                <DemoContainer components={['DatePicker']}>
                  <DatePicker/>
                </DemoContainer>
              </LocalizationProvider>
            </CustomDatePicker>
          </Grid>
          <Grid item className='filter-content-box btn-box'>
            <Grid container spacing={2}>
              <Grid item xs={6}>
                <CustomPrimaryButton fullWidth>Reset</CustomPrimaryButton>
              </Grid>
              <Grid item xs={6}>
                <CustomMainButton fullWidth>Show</CustomMainButton>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </FilterWrapper>
      <TableWrapper theme={theme} className='table-wrap'>
        <TableContainer component={Paper}>
          <Table aria-label='simple table'>
            <TableHead>
              <TableRow>
                <TableCell>Date</TableCell>
                <TableCell>Amount</TableCell>
                <TableCell>TXID</TableCell>
                <TableCell>Type</TableCell>
                <TableCell>Status</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {Array(5).fill('').map((item)=>{
                return (
                  <>
                    <TableRow>
                      <TableCell>16/02/2023 <br/> 16:28</TableCell>
                      <TableCell>$100</TableCell>
                      <TableCell>8ac64d82d7054sdg884g</TableCell>
                      <TableCell>Withdraw</TableCell>
                      <TableCell>
                        <Chip label='Win' className='status-win' />
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>16/02/2023 <br/> 16:28</TableCell>
                      <TableCell>$100</TableCell>
                      <TableCell>8ac64d82d7054sdg884g</TableCell>
                      <TableCell>Deposit</TableCell>
                      <TableCell>
                        <Chip label='Lose' className='status-lose' />
                      </TableCell>
                    </TableRow>
                  </>
                )
              })}
            </TableBody>
          </Table>
        </TableContainer>
      </TableWrapper>
    </AllTransactionsWrapper>
  )
}
