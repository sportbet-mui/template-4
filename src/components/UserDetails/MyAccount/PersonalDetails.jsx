import React, { useState } from 'react'
import { PersonalDetailsWrapper } from './style';
import { useTheme } from '@emotion/react'
import { Avatar, Box, Grid, MenuItem, Typography } from '@mui/material';
import { CustomMainButton, CustomPrimaryButton, CustomTextButton } from '../../Common/StyledButton/CustomButton';
import { CustomDatePicker, CustomInputLabel, CustomSelectField, CustomTextField } from '../../Common/StyledForm/style';
import { DatePicker, LocalizationProvider } from '@mui/x-date-pickers';
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';

export default function PersonalDetails() {
  const theme = useTheme();

  const [selectTypeState,setTypeState]=useState(1);

  const [selectTypeCountry,setTypeCountry]=useState(1);

  return (
    <PersonalDetailsWrapper theme={theme}>
      <Box className='profile-wrap'>
        <Box className='profile-box'>
          <Avatar src={'assets/images/stock-images/avatar.png'} alt='Avatar' />
          <Box className='text-box'>
            <Typography variant='h6'>Josh Ludin</Typography>
            <CustomTextButton component='label'>
              Change Picture
              <input hidden accept='image/*' multiple type='file' />
            </CustomTextButton>
          </Box>
        </Box>
      </Box>
      <Grid container spacing={3}>
        <Grid item sm={6} xs={12}>
          <CustomInputLabel>First Name</CustomInputLabel>
          <CustomTextField placeholder='First Name' />
        </Grid>
        <Grid item sm={6} xs={12}>
          <CustomInputLabel>Last Name</CustomInputLabel>
          <CustomTextField placeholder='Last Name' />
        </Grid>
        <Grid item sm={6} xs={12}>
          <CustomInputLabel>Username</CustomInputLabel>
          <CustomTextField disabled placeholder='Username' />
        </Grid>
        <Grid item sm={6} xs={12}>
          <CustomInputLabel>Date of Birth</CustomInputLabel>
          <CustomDatePicker>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DemoContainer components={['DatePicker']}>
                <DatePicker/>
              </DemoContainer>
            </LocalizationProvider>
          </CustomDatePicker>
        </Grid>
        <Grid item sm={6} xs={12}>
          <CustomInputLabel>Email Address</CustomInputLabel>
          <CustomTextField disabled placeholder='Email Address' />
        </Grid>
        <Grid item sm={6} xs={12}>
          <CustomInputLabel>Mobile Number</CustomInputLabel>
          <CustomTextField placeholder='Mobile Number' />
        </Grid>
        <Grid item sm={6} xs={12}>
          <CustomInputLabel>Street Address</CustomInputLabel>
          <CustomTextField placeholder='Street Address' />
        </Grid>
        <Grid item sm={6} xs={12}>
          <CustomInputLabel>State</CustomInputLabel>
          <CustomSelectField value={selectTypeState} onChange={setTypeState}>
            <MenuItem value={selectTypeState}>State 1</MenuItem>
            <MenuItem value={selectTypeState}>State 2</MenuItem>
            <MenuItem value={selectTypeState}>State 3</MenuItem>
          </CustomSelectField>
        </Grid>
        <Grid item sm={6} xs={12}>
          <CustomInputLabel>Country</CustomInputLabel>
          <CustomSelectField value={selectTypeCountry} onChange={setTypeCountry}>
            <MenuItem value={selectTypeCountry}>Country 1</MenuItem>
            <MenuItem value={selectTypeCountry}>Country 2</MenuItem>
            <MenuItem value={selectTypeCountry}>Country 3</MenuItem>
          </CustomSelectField>
        </Grid>
        <Grid item sm={6} xs={12}>
          <CustomInputLabel>ZIP Code</CustomInputLabel>
          <CustomTextField placeholder='ZIP Code' />
        </Grid>
      </Grid>
      <Box className='form-btn-wrap'>
        <Grid container spacing={2}>
          <Grid item lg={3} md={4} sm={6} xs={6}>
            <CustomPrimaryButton fullWidth>Cancel</CustomPrimaryButton>
          </Grid>
          <Grid item lg={3} md={4} sm={6} xs={6}>
            <CustomMainButton fullWidth>Save</CustomMainButton>
          </Grid>
        </Grid>
      </Box>
    </PersonalDetailsWrapper>
  )
}
