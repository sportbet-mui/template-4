import { Box, capitalize, styled } from '@mui/material';

export const MyAccountWrapper = styled(Box)(({ theme }) => {
    return {
    };
});

export const PersonalDetailsWrapper = styled(Box)(({ theme }) => {
    return {
        '& .profile-wrap': {
            marginBottom: theme.spacing(4),
            '& .profile-box': {
                width: 'fit-content',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                '& .MuiAvatar-root': {
                    width: theme.spacing(8.25),
                    height: theme.spacing(8.25),
                    marginRight: theme.spacing(2.5),
                },
                '& .text-box': {
                    '& .MuiTypography-h6': {
                        color: theme.palette.primary.contrastText,
                        fontSize: theme.spacing('1rem'),
                        fontWeight: theme.typography.fontWeightMedium,
                    },
                    '& .MuiButton-text': {
                        height: theme.spacing('auto'),
                        color: theme.palette.primary.contrastTextActive,
                        fontSize: theme.spacing('0.75rem'),
                        fontWeight: theme.typography.fontWeightRegular,
                        padding: theme.spacing(0),
                        '&:hover': {
                            color: theme.palette.primary.contrastText,
                        },
                    },
                },
            },
        },
    };
});

export const ChangePasswordWrapper = styled(Box)(({ theme }) => {
    return {
    };
});

export const KYCWrapper = styled(Box)(({ theme }) => {
    return {
        '& .upload-doc-btn': {
            width: theme.spacing('100%'),
            height: theme.spacing(25),
            backgroundColor: theme.palette.primary.dark,
            borderRadius: theme.spacing(1.25),
            color: theme.palette.primary.contrastTextLight,
            fontSize: theme.spacing('0.875rem'),
            fontWeight: theme.typography.fontWeightRegular,
            textTransform: 'capitalize',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'column',
            '& .icon-box': {
                width: theme.spacing(5.5),
                height: theme.spacing(5.5),
                backgroundColor: theme.palette.primary.light,
                borderWidth: theme.spacing(0.1875),
                borderStyle: 'solid',
                borderColor: theme.palette.border.light,
                borderRadius: theme.spacing(1.5),
                boxShadow: theme.shadows[4],
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
            },
            '& .MuiTypography-body1': {
                fontSize: theme.spacing('0.875rem'),
                marginTop: theme.spacing(2),
            },
        },
    };
});