import React from 'react'
import { ChangePasswordWrapper } from './style';
import { useTheme } from '@emotion/react'
import { Box, Grid, IconButton, InputAdornment, Typography } from '@mui/material';
import { CustomMainButton, CustomPrimaryButton } from '../../Common/StyledButton/CustomButton';
import { CustomInputLabel, CustomPasswordField, CustomSelectField, CustomTextField, PasswordInput } from '../../Common/StyledForm/style';
import { Visibility, VisibilityOff } from '@mui/icons-material';

export default function ChangePassword() {
  const theme = useTheme();

  const [showCurrentPassword, setShowCurrentPassword] = React.useState(false);
  const handleClickShowCurrentPassword = () => setShowCurrentPassword((show) => !show);
  const handleMouseDownCurrentPassword = (event) => {
    event.preventDefault();
  };
  
  const [showPassword, setShowPassword] = React.useState(false);
  const handleClickShowPassword = () => setShowPassword((show) => !show);
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const [showConfirmPassword, setShowConfirmPassword] = React.useState(false);
  const handleClickShowConfirmPassword = () => setShowConfirmPassword((show) => !show);
  const handleMouseDownConfirmPassword = (event) => {
    event.preventDefault();
  };

  return (
    <ChangePasswordWrapper theme={theme}>
      <Typography variant='h2'>Change Password</Typography>
      <Grid container spacing={3}>
        <Grid item sm={6} xs={12}>
          <CustomInputLabel>Current Password</CustomInputLabel>
          <CustomPasswordField>
            <PasswordInput
              placeholder='Current Password'
              id='outlined-adornment-password'
              type={showCurrentPassword ? 'text' : 'password'}
              endAdornment={
                <InputAdornment position='end'>
                  <IconButton
                    aria-label='toggle password visibility'
                    onClick={handleClickShowCurrentPassword}
                    onMouseDown={handleMouseDownCurrentPassword}
                    edge='end'
                  >
                    {showCurrentPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              }
            />
          </CustomPasswordField>
        </Grid>
        <Grid item sm={6} xs={12}>&nbsp;</Grid>
        <Grid item sm={6} xs={12}>
          <CustomInputLabel>New Password</CustomInputLabel>
          <CustomPasswordField>
            <PasswordInput
              placeholder='Password'
              id='outlined-adornment-password'
              type={showPassword ? 'text' : 'password'}
              endAdornment={
                <InputAdornment position='end'>
                  <IconButton
                    aria-label='toggle password visibility'
                    onClick={handleClickShowPassword}
                    onMouseDown={handleMouseDownPassword}
                    edge='end'
                  >
                    {showPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              }
            />
          </CustomPasswordField>
        </Grid>
        <Grid item sm={6} xs={12}>
          <CustomInputLabel>Confirm Password</CustomInputLabel>
          <CustomPasswordField>
            <PasswordInput
              placeholder='Confirm Password'
              id='outlined-adornment-password'
              type={showConfirmPassword ? 'text' : 'password'}
              endAdornment={
                <InputAdornment position='end'>
                  <IconButton
                    aria-label='toggle password visibility'
                    onClick={handleClickShowConfirmPassword}
                    onMouseDown={handleMouseDownConfirmPassword}
                    edge='end'
                  >
                    {showConfirmPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              }
            />
          </CustomPasswordField>
        </Grid>
      </Grid>
      <Box className='form-btn-wrap'>
        <Grid container spacing={2}>
          <Grid item lg={3} md={4} sm={6} xs={6}>
            <CustomPrimaryButton fullWidth>Cancel</CustomPrimaryButton>
          </Grid>
          <Grid item lg={3} md={4} sm={6} xs={6}>
            <CustomMainButton fullWidth>Save</CustomMainButton>
          </Grid>
        </Grid>
      </Box>
    </ChangePasswordWrapper>
  )
}
