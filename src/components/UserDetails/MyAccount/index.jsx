import React from 'react'
import { MyAccountWrapper } from './style';
import { useTheme } from '@emotion/react'
import UserTabs from '../UserTabs';
import { ContentWrapper, UserSubTabsWrapper } from '../style';
import { Box, Container, Tab, Tabs } from '@mui/material';
import PersonalDetails from './PersonalDetails';
import ChangePassword from './ChangePassword';
import KYC from './KYC';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role='tabpanel'
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box className='tab-panel'>
          {children}
        </Box>
      )}
    </div>
  );
}

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

export default function MyAccount() {
  const theme = useTheme();

  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <MyAccountWrapper theme={theme}>
      <UserTabs/>

      <Container maxWidth='lg'>
        <UserSubTabsWrapper theme={theme}>
          <Tabs value={value} onChange={handleChange} variant='scrollable' scrollButtons allowScrollButtonsMobile aria-label='basic tabs example'>
            <Tab label='Personal Details' {...a11yProps(0)} />
            <Tab label='Change Password' {...a11yProps(1)} />
            <Tab label='KYC' {...a11yProps(2)} />
          </Tabs>
        </UserSubTabsWrapper>

        <TabPanel value={value} index={0}>
          <ContentWrapper theme={theme}>
            <PersonalDetails/>
          </ContentWrapper>
        </TabPanel>

        <TabPanel value={value} index={1}>
          <ContentWrapper theme={theme}>
            <ChangePassword/>
          </ContentWrapper>
        </TabPanel>

        <TabPanel value={value} index={2}>
          <ContentWrapper theme={theme}>
            <KYC/>
          </ContentWrapper>
        </TabPanel>
      </Container>
    </MyAccountWrapper>
  )
}
