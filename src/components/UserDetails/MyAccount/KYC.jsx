import React from 'react'
import { KYCWrapper } from './style';
import { useTheme } from '@emotion/react'
import { Box, Grid, IconButton, Typography } from '@mui/material';
import { CustomMainButton, CustomPrimaryButton } from '../../Common/StyledButton/CustomButton';
import { CustomInputLabel } from '../../Common/StyledForm/style';

export default function KYC() {
  const theme = useTheme();

  return (
    <KYCWrapper theme={theme}>
      <Typography variant='h2'>KYC</Typography>
      <Typography variant='subtitle1'>Please complete the identity verification process by Providing Photos of two bank cards in your name. SportsBet does not store players’ bank information details. All transactions and entry of account details will take place on your selected payment gateway.</Typography>
      <Grid container spacing={3}>
        <Grid item sm={6} xs={12}>
          <CustomInputLabel>Document 1*</CustomInputLabel>
          <IconButton component='label' className='upload-doc-btn' aria-label='upload picture'>
            <input hidden accept='image/*' multiple type='file' />
            <Box className='icon-box'>
              <img src={'assets/images/svg/icon-upload.svg'} alt='Icon' />
            </Box>
            <Typography variant='body1'>Upload Front Document</Typography>
          </IconButton>
        </Grid>
        <Grid item sm={6} xs={12}>
          <CustomInputLabel>Document 1*</CustomInputLabel>
          <IconButton component='label' className='upload-doc-btn' aria-label='upload picture'>
            <input hidden accept='image/*' multiple type='file' />
            <Box className='icon-box'>
              <img src={'assets/images/svg/icon-upload.svg'} alt='Icon' />
            </Box>
            <Typography variant='body1'>Upload Back Document</Typography>
          </IconButton>
        </Grid>
      </Grid>
      <Box className='form-btn-wrap'>
        <Grid container spacing={2}>
          <Grid item lg={3} md={4} sm={6} xs={6}>
            <CustomPrimaryButton fullWidth>Cancel</CustomPrimaryButton>
          </Grid>
          <Grid item lg={3} md={4} sm={6} xs={6}>
            <CustomMainButton fullWidth>Submit</CustomMainButton>
          </Grid>
        </Grid>
      </Box>
    </KYCWrapper>
  )
}
