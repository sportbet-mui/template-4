import React from 'react'
import { UserDetailsWrapper } from './style';
import { useTheme } from '@emotion/react';
import UserTabs from './UserTabs';

export default function UserDetails() {
  const theme = useTheme();

  return (
    <UserDetailsWrapper>
      <UserTabs/>
    </UserDetailsWrapper>
  )
}
