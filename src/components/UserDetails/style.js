import { Box, styled } from '@mui/material';

export const UserTabsWrapper = styled(Box)(({ theme }) => {
    return {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(6),
        '& .MuiTabs-root': {
            borderRadius: theme.spacing(1.25),
            '& .MuiTab-root': {
                background: theme.palette.gradient.tabBg,
                color: theme.palette.primary.contrastText,
                fontSize: theme.spacing('1.125rem'),
                fontWeight: theme.typography.fontWeightMedium,
                display: 'flex',
                justifyContent: 'flex-start',
                alignItems: 'center',
                padding: theme.spacing(3, 2, 2),
                '& .MuiTab-iconWrapper': {
                    width: theme.spacing(10),
                    height: theme.spacing(10),
                    background: theme.palette.gradient.tabIconBg,
                    borderWidth: theme.spacing(0.125),
                    borderStyle: 'solid',
                    borderColor: theme.palette.border.grey,
                    borderRadius: theme.spacing(5),
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginBottom: theme.spacing(2),
                },
                '& .MuiSvgIcon-root, & img': {
                    width: theme.spacing(5),
                    height: theme.spacing(5),
                },
                '&.Mui-selected': {
                    background: theme.palette.gradient.tabBgActive,
                    '& .MuiTab-iconWrapper': {
                        borderColor: theme.palette.border.active,
                        boxShadow: theme.shadows[3],
                    },
                },
            },
            [theme.breakpoints.down('xm')]: {
                '& .MuiTab-root': {
                    fontSize: theme.spacing('1rem'),
                    '& .MuiTab-iconWrapper': {
                        width: theme.spacing(7.5),
                        height: theme.spacing(7.5),
                        marginBottom: theme.spacing(2),
                    },
                    '& .MuiSvgIcon-root, & img': {
                        width: theme.spacing(3.75),
                        height: theme.spacing(3.75),
                    },
                },
            },
            [theme.breakpoints.down('sm')]: {
                '& .MuiTab-root': {
                    fontSize: theme.spacing('0.75rem'),
                    '& .MuiTab-iconWrapper': {
                        width: theme.spacing(5),
                        height: theme.spacing(5),
                        marginBottom: theme.spacing(2),
                    },
                    '& .MuiSvgIcon-root, & img': {
                        width: theme.spacing(2.5),
                        height: theme.spacing(2.5),
                    },
                },
            },
        },
        '& .MuiTabs-indicator': {
            display: 'none',
        },
    };
});

export const UserSubTabsWrapper = styled(Box)(({ theme }) => {
    return {
        zIndex: '0',
        marginBottom: theme.spacing(6),
        position: 'relative',
        '&::before': {
            content: '""',
            width: theme.spacing('100%'),
            height: theme.spacing(0.125),
            borderWidth: theme.spacing(0, 0, 0.125, 0),
            borderStyle: 'solid',
            borderColor: theme.palette.border.light,
            zIndex: '-1',
            position: 'absolute',
            top: theme.spacing('50%'),
            left: theme.spacing(0),
            transform: theme.spacing('translateY(-50%)'),
        },
        '& .MuiTabs-flexContainer': {
            justifyContent: 'space-around',
            [theme.breakpoints.down('md')]: {
                justifyContent: 'flex-start',
            },
        },
        '& .MuiTabs-root': {
            '& .MuiTab-root': {
                width: theme.spacing('100%'),
                maxWidth: theme.spacing(24.875),
                backgroundColor: theme.palette.primary.dark,
                borderRadius: theme.spacing(1.25),
                color: theme.palette.secondary.contrastTextActive,
                fontSize: theme.spacing('1.125rem'),
                fontWeight: theme.typography.fontWeightMedium,
                textTransform: 'capitalize',
                padding: theme.spacing(1, 2),
                margin: theme.spacing(0, 2),
                '&:first-child': {
                    marginLeft: theme.spacing(0),
                },
                '&:last-child': {
                    marginRight: theme.spacing(0),
                },
                '&.Mui-selected': {
                    color: theme.palette.primary.contrastText,
                },
                [theme.breakpoints.down('xm')]: {
                    fontSize: theme.spacing('1rem'),
                },
                [theme.breakpoints.down('sm')]: {
                    width: theme.spacing('auto'),
                },
            },
            '& .MuiTabScrollButton-root': {
                width: theme.spacing(2.5),
                backgroundColor: theme.palette.primary.main,
                '& .MuiSvgIcon-root': {
                    color: theme.palette.primary.contrastTextActive,
                },
            },
        },
    };
});

export const ContentWrapper = styled(Box)(({ theme }) => {
    return {
        backgroundColor: theme.palette.primary.light,
        borderRadius: theme.spacing(1.25),
        padding: theme.spacing(5),
        marginBottom: theme.spacing(5),
        '& .MuiTypography-h2': {
            color: theme.palette.primary.contrastText,
            fontSize: theme.spacing('1.5rem'),
            fontWeight: theme.typography.fontWeightMedium,
            marginBottom: theme.spacing(3),
        },
        '& .MuiTypography-subtitle1': {
            color: theme.palette.primary.contrastTextLight,
            fontSize: theme.spacing('0.875rem'),
            fontWeight: theme.typography.fontWeightRegular,
            marginBottom: theme.spacing(3),
        },
        '& .MuiList-root': {
            listStyleType: 'disc', 
            marginLeft: theme.spacing(3),
            '& .MuiListItem-root': {
                color: theme.palette.primary.contrastTextLight,
                display: 'list-item',
                padding: theme.spacing(0),
                '& .MuiListItemText-root': {
                    '& .MuiTypography-body1': {
                        color: theme.palette.primary.contrastTextLight,
                        fontSize: theme.spacing('0.875rem'),
                        fontWeight: theme.typography.fontWeightRegular,
                    },
                },
            },
        },  
        '& .form-btn-wrap': {
            marginTop: theme.spacing(4),
            '& .MuiGrid-container': {
                justifyContent: 'center',
            },
            '& .MuiButtonBase-root': {
                height: theme.spacing(6.5),
            },
        },
        '& .withdrawal-amount-box': {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: theme.spacing(2),
            '& .MuiFormLabel-root': {
                margin: theme.spacing(0, 1, 0, 0),
            },
            '& .MuiButton-text': {
                height: theme.spacing('auto'),
                color: theme.palette.primary.contrastTextActive,
                fontSize: theme.spacing('0.875rem'),
                fontWeight: theme.typography.fontWeightRegular,
                padding: theme.spacing(0),
                '&:hover, &.active': {
                    color: theme.palette.primary.contrastText,
                },
            },
        },
    };
});