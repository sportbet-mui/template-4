import React from 'react'
import { UserTabsWrapper } from './style';
import { useTheme } from '@emotion/react'
import { Box, Container, Tab, Tabs } from '@mui/material';
import PersonalDetails from './MyAccount/PersonalDetails';

function LinkTab(props) {
  return (
    <Tab
      component='a'
      onClick={(event) => {
        event.preventDefault();
      }}
      {...props}
    />
  );
}

export default function UserTabs() {
  const theme = useTheme();

  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <UserTabsWrapper theme={theme}>
      <Container maxWidth='lg'>
        <Tabs value={value} onChange={handleChange} variant='fullWidth' aria-label='nav tabs example'>
          <LinkTab href='/' icon={<Box><img src={'assets/images/svg/user.svg'} alt='Icon' /></Box>} label='My Account' />
          <LinkTab href='/' icon={<Box><img src={'assets/images/svg/wallet.svg'} alt='Icon' /></Box>} label='Wallet' />
          <LinkTab href='/' icon={<Box><img src={'assets/images/svg/responsible-gambling.svg'} alt='Icon' /></Box>} label='Responsible Gambling' />
        </Tabs>
      </Container>
    </UserTabsWrapper>
  )
}