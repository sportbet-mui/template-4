import styled from '@emotion/styled';
import { Button } from '@mui/material';


export const StyledMainButton = styled(Button)(({ theme }) => {
    return {
        height: theme.spacing(5.375),
        backgroundColor: theme.palette.button.main,
        borderRadius: theme.spacing(1.25),
        color: theme.palette.primary.contrastText,
        fontSize: theme.spacing('1rem'),
        fontWeight: theme.typography.fontWeightMedium,
        textTransform: 'capitalize',
        padding: theme.spacing(1.5, 3),
        '& svg': {
            fill: theme.palette.primary.contrastText,
        },
        '&:hover': {
            backgroundColor: theme.palette.button.white,
            color: theme.palette.primary.contrastTextActive,
            '& svg': {
                fill: theme.palette.primary.contrastTextActive,
            },
        },
    };
});

export const StyledPrimaryButton = styled(Button)(({ theme }) => {
    return {
        height: theme.spacing(5.375),
        backgroundColor: theme.palette.button.light,
        borderRadius: theme.spacing(1.25),
        color: theme.palette.primary.contrastText,
        fontSize: theme.spacing('1rem'),
        fontWeight: theme.typography.fontWeightMedium,
        textTransform: 'capitalize',
        padding: theme.spacing(1.5, 3),
        '& svg': {
            fill: theme.palette.primary.contrastText,
        },
        '&:hover': {
            backgroundColor: theme.palette.button.white,
            color: theme.palette.secondary.light,
            '& svg': {
                fill: theme.palette.secondary.light,
            },
        },
    };
});

export const StyledTextButton = styled(Button)(({ theme }) => {
    return {
        height: theme.spacing(5.375),
        backgroundColor: theme.palette.button.transparent,
        borderRadius: theme.spacing(1.25),
        color: theme.palette.primary.contrastText,
        fontSize: theme.spacing('1rem'),
        fontWeight: theme.typography.fontWeightMedium,
        textTransform: 'capitalize',
        padding: theme.spacing(1.5, 3),
        '& svg': {
            fill: theme.palette.primary.contrastText,
        },
        '&:hover': {
            backgroundColor: theme.palette.button.transparent,
            color: theme.palette.primary.contrastTextActive,
            '& svg': {
                fill: theme.palette.primary.contrastTextActive,
            },
        },
    };
});