import React from 'react'
import { StyledHeadingTypography } from './style';
import { useTheme } from '@emotion/react';

export const HeadingWrapper = () => {
  const theme = useTheme();
  
  return (
    <HeadingWrapper theme={theme}>
    </HeadingWrapper>
  );
};

export const CustomHeadingTypography = ({ variant, display, component, align, value, noWrap, ...otherProps }) => {
  return (
    <StyledHeadingTypography variant='h1' component={component} display={display} align={align} noWrap={noWrap} {...otherProps}>
      {value}
    </StyledHeadingTypography>
  );
};