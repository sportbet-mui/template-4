import { Box, Typography, styled } from '@mui/material';

export const HeadingWrapper = styled(Box)(({ theme }) => {
    return {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: '0',
        margin: theme.spacing(5, 0),
        position: 'relative',
        '&::before': {
            content: '""',
            width: theme.spacing('100%'),
            height: theme.spacing(0.125),
            borderWidth: theme.spacing(0, 0, 0.125, 0),
            borderStyle: 'solid',
            borderColor: theme.palette.border.light,
            zIndex: '-1',
            position: 'absolute',
            top: theme.spacing('50%'),
            left: theme.spacing(0),
            transform: theme.spacing('translateY(-50%)'),
        },
    };
});

export const StyledHeadingTypography = styled(Typography)(({ theme }) => {
    return {
        maxWidth: theme.spacing('90%'),
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.primary.contrastText,
        fontSize: theme.spacing('1.5rem'),
        fontWeight: theme.typography.fontWeightMedium,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        padding: theme.spacing(0, 3),
        '& .heading-img': {
            maxWidth: theme.spacing('100%'),
            height: theme.spacing('auto'),
            marginRight: theme.spacing(3),
        },
        [theme.breakpoints.down('sm')]: {
            padding: theme.spacing(0, 1.5),
            '& .heading-img': {
                marginRight: theme.spacing(1.5),
            },
        },
    };
});