import { Box, Menu, styled } from '@mui/material';

export const TableWrapper = styled(Box)(({ theme }) => {
    return {
        marginBottom: theme.spacing(5),
        '& .MuiPaper-root': {
            backgroundColor: theme.palette.primary.light,
            borderRadius: theme.spacing(1.25),
            boxShadow: theme.shadows[0],
            '& .MuiTable-root': {
                minWidth: theme.spacing(81.25),
                '& .MuiTableHead-root': {
                    '& .MuiTableRow-root': {
                        backgroundColor: theme.palette.primary.light,
                        '& .MuiTableCell-root': {
                            borderColor: theme.palette.border.main,
                            color: theme.palette.primary.contrastText,
                            fontSize: theme.spacing('1rem'),
                            fontWeight: theme.typography.fontWeightMedium,
                        },
                    },
                },
                '& .MuiTableBody-root': {
                    '& .MuiTableRow-root': {
                        backgroundColor: theme.palette.primary.light,
                        '&:nth-child(odd)': {
                            backgroundColor: theme.palette.secondary.dark,
                        },
                        '& .MuiTableCell-root': {
                            borderWidth: theme.spacing(0),
                            color: theme.palette.primary.contrastTextLight,
                            fontSize: theme.spacing('0.875rem'),
                            fontWeight: theme.typography.fontWeightMedium,
                        },
                    },
                },
                '& .MuiTableHead-root, & .MuiTableBody-root': {
                    '& .MuiTableRow-root': {
                        '& .MuiTableCell-root': {
                            '& .vs-text': {
                                display: 'inline-block',
                                margin: theme.spacing(0, 3),
                            },
                            '&.secondary-heading': {
                                color: theme.palette.primary.contrastTextLight,
                                fontSize: theme.spacing('0.875rem'),
                            },
                            '& .MuiButton-text': {
                                height: theme.spacing('auto'),
                                color: theme.palette.primary.contrastTextActive,
                                fontSize: theme.spacing('0.875rem'),
                                padding: theme.spacing(0),
                                '&:hover, &.active': {
                                    color: theme.palette.primary.contrastText,
                                },
                            },
                            '& .MuiChip-root': {
                                minWidth: theme.spacing(9),
                                borderRadius: theme.spacing(0.625),
                                color: theme.palette.primary.contrastText,
                                fontSize: theme.spacing('0.875rem'),
                                '&.status-win': {
                                    backgroundColor: theme.palette.success.main,
                                },
                                '&.status-lose': {
                                    backgroundColor: theme.palette.danger.main,
                                },
                            },
                        },
                    },
                },
            },
        },
    };
});

export const FilterWrapper = styled(Box)(({ theme }) => {
    return {
        margin: theme.spacing(3, 0),
        '& .filter-content-box': {
            minWidth: theme.spacing(22.5),
            '& .MuiFormControl-root': {
                width: theme.spacing('100%'),
                minWidth: theme.spacing(22.5),
                maxWidth: theme.spacing('min-content'),
                backgroundColor: theme.palette.primary.light,
                borderRadius: theme.spacing(1.25),
                '& .MuiInputBase-root': {
                    backgroundColor: theme.palette.primary.light,
                    '& .MuiInputBase-input': {
                        color: theme.palette.primary.contrastTextLight,
                        padding: theme.spacing(1.25, 2),
                    },
                    '& .MuiSvgIcon-root': {
                        fill: theme.palette.primary.contrastTextLight,
                    },
                },
                '& .MuiStack-root': {
                    '& .MuiTextField-root': {
                        minWidth: theme.spacing(22.5),
                    },
                    '& .MuiInputBase-root': {
                        '& .MuiInputBase-input': {
                            padding: theme.spacing(1.43, 2),
                        },
                    },
                },
            },
            '& .MuiInputBase-root': {
                width: theme.spacing('100%'),
                minWidth: theme.spacing(22.5),
                maxWidth: theme.spacing('min-content'),
                backgroundColor: theme.palette.primary.light,
                '& .MuiInputBase-input': {
                    color: theme.palette.primary.contrastTextLight,
                    padding: theme.spacing(1.25, 2),
                },
                '& .MuiSvgIcon-root': {
                    fill: theme.palette.primary.contrastTextLight,
                },
            },
            '&.btn-box': {
                display: 'flex',
                alignItems: 'flex-end',
            },
            [theme.breakpoints.down('md')]: {
                width: theme.spacing('50%'),
                '& .MuiFormControl-root, & .MuiInputBase-root': {
                    maxWidth: theme.spacing('inherit'),
                },
            },
            [theme.breakpoints.down('sm')]: {
                minWidth: theme.spacing('auto'),
                '& .MuiFormControl-root, & .MuiInputBase-root': {
                    minWidth: theme.spacing('auto'),
                    maxWidth: theme.spacing('auto'),
                    '& .MuiStack-root': {
                        '& .MuiTextField-root': {
                            minWidth: theme.spacing('auto'),
                        },
                    },
                },
            },
        },
    };
});