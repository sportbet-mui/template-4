import React, { forwardRef } from 'react'
import { CustomDialogBoxWrapper } from './style';
import { useTheme } from '@emotion/react'
import HighlightOffRoundedIcon from '@mui/icons-material/HighlightOffRounded';
import { DialogContent, IconButton, Slide } from '@mui/material';

const Transition = forwardRef(function Transition(props, ref) {
  return <Slide direction='down' ref={ref} {...props} />;
});

export default function CustomDialogBox(props) {
  const theme = useTheme();

  const { dialogOpen, handleDialogClose, title, children, fullWidth, maxWidth, fullScreen, PaperComponent, scroll } = props;

  return (
    <CustomDialogBoxWrapper
      theme={theme}
      open={dialogOpen}
      onClose={handleDialogClose}
      aria-labelledby='alert-dialog-title'
      aria-describedby='alert-dialog-description'
      fullWidth={fullWidth}
      maxWidth={maxWidth}
      fullScreen={fullScreen}
      TransitionComponent={Transition}
      PaperComponent={PaperComponent}
      scroll={scroll}
    >
      <DialogContent>
        {handleDialogClose ? (
          <IconButton
            className='dialog-close-btn'
            aria-label='close'
            onClick={handleDialogClose}
          >
            <HighlightOffRoundedIcon />
          </IconButton>
        ) : null}
        {children}
      </DialogContent>
    </CustomDialogBoxWrapper>
  )
}
