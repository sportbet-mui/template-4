import { Box, Dialog, styled } from '@mui/material';

export const CustomDialogBoxWrapper = styled(Dialog)(({ theme }) => {
    return {
        backdropFilter: 'blur(3px)',
        '& .MuiDialog-paper': {
            width: theme.spacing('100%'),
            backgroundColor: theme.palette.primary.light,
            borderRadius: theme.spacing(1.25),
            '& .MuiDialogContent-root': {
                padding: theme.spacing(0),
                '& .dialog-close-btn': {
                    position: 'absolute',
                    top: theme.spacing(1.25),
                    right: theme.spacing(1.25),
                    '& .MuiSvgIcon-root': {
                        fill: theme.palette.primary.contrastTextLight,
                    },
                },
            },
            [theme.breakpoints.down('sm')]: {
                borderRadius: theme.spacing(0),
            },
        },
    };
});

export const ContentWrapper = styled(Box)(({ theme }) => {
    return {
        display: 'flex',
        '& .img-box': {
            width: theme.spacing('33.333%'),
            overflow: 'hidden',
            '& .dialog-img': {
                maxWidth: theme.spacing('100%'),
                height: theme.spacing('100%'),
            },
            [theme.breakpoints.down('md')]: {
                display: 'none',
            }
        },
        '& .form-wrap': {
            width: theme.spacing('100%'),
            padding: theme.spacing(5),
            [theme.breakpoints.up('sm')]: {
                maxHeight: theme.spacing('calc(100vh - 64px)'),
                overflowY: 'auto',
            },
            '&.login-form-wrap, &.signup-form-wrap': {
                width: theme.spacing('calc(100% - 33.333%)'),
                [theme.breakpoints.down('md')]: {
                    width: theme.spacing('100%'),
                },
            },
            '& .MuiTypography-h1': {
                color: theme.palette.primary.contrastText,
                fontSize: theme.spacing('2.5rem'),
                fontWeight: theme.typography.fontWeightSemiBold,
                lineHeight: theme.spacing(7.5),
                textAlign: 'center',
                marginBottom: theme.spacing(4.375),
                [theme.breakpoints.down('sm')]: {
                    fontSize: theme.spacing('2rem'),
                    lineHeight: theme.spacing(5),
                    marginBottom: theme.spacing(2.5),
                },
            },
            '& .MuiGrid-container': {
                justifyContent: 'center',
            },
            '& .btn-wrap': {
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                margin: theme.spacing(0, 0, 1),
                '& .MuiButtonBase-root': {
                    margin: theme.spacing(0, 1),
                },
            },
            '& .text-btn-box': {
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                '& .MuiButton-text': {
                    height: theme.spacing('auto'),
                    color: theme.palette.primary.contrastTextActive,
                    padding: theme.spacing(0),
                    '&:hover, &.active': {
                        color: theme.palette.primary.contrastText,
                    },
                },
            },
            '& .MuiTypography-body1': {
                color: theme.palette.primary.contrastTextLight,
                fontSize: theme.spacing('0.75rem'),
                fontWeight: theme.typography.fontWeightRegular,
                textAlign: 'center',
                '& .MuiLink-root': {
                    color: theme.palette.primary.contrastTextActive,
                    textDecoration: 'none',
                },
            },
            '& .human-test-box': {
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
            },
        },
    };
});