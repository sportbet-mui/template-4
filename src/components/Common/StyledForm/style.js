import { Box, FormControl, InputLabel, OutlinedInput, Select, TextField, styled } from '@mui/material';

export const CustomFormWrapper = styled(Box)(({ theme }) => {
    return {
    };
});

export const CustomTextField = styled(TextField)(({ theme }) => {
    return {
        width: theme.spacing('100%'),
        '& .MuiInputBase-root': {
            maxHeight: theme.spacing(7),
            backgroundColor: theme.palette.primary.dark,
            borderRadius: theme.spacing(1.25),
            '& .MuiInputBase-input': {
                color: theme.palette.primary.contrastText,
                fontSize: theme.spacing('0.875rem'),
                fontWeight: theme.typography.fontWeightRegular,
                padding: theme.spacing(2.25, 2),
            },
            '& .MuiSvgIcon-root': {
                fill: theme.palette.primary.contrastText,
            },
            '& .MuiOutlinedInput-notchedOutline': {
                borderColor: theme.palette.border.transparent,
            },
            '&.Mui-focused': {
                '& .MuiOutlinedInput-notchedOutline': {
                    borderColor: theme.palette.border.green,
                },
            },
            '&:hover': {
                '& .MuiOutlinedInput-notchedOutline': {
                    borderColor: theme.palette.border.transparent,
                },
            },
            '&.Mui-focused, &.Mui-focused:hover': {
                '& .MuiOutlinedInput-notchedOutline': {
                    borderColor: theme.palette.border.green,
                },
            },
            '&.Mui-disabled': {
                backgroundColor: theme.palette.secondary.light,
                cursor: 'no-drop',
            },
        },
    };
});

export const CustomSelectField = styled(Select)(({ theme }) => {
    return {
        width: theme.spacing('100%'),
        maxHeight: theme.spacing(7),
        backgroundColor: theme.palette.primary.dark,
        borderRadius: theme.spacing(1.25),
        '& .MuiInputBase-input': {
            color: theme.palette.primary.contrastText,
            fontSize: theme.spacing('0.875rem'),
            fontWeight: theme.typography.fontWeightRegular,
            padding: theme.spacing(2.25, 2),
        },
        '& .MuiSvgIcon-root': {
            fill: theme.palette.primary.contrastText,
        },
        '& .MuiOutlinedInput-notchedOutline': {
            borderColor: theme.palette.border.transparent,
        },
        '&:hover': {
            '& .MuiOutlinedInput-notchedOutline': {
                borderColor: theme.palette.border.transparent,
            },
        },
        '&.Mui-focused, &.Mui-focused:hover': {
            '& .MuiOutlinedInput-notchedOutline': {
                borderColor: theme.palette.border.green,
            },
        },
        '&.Mui-disabled': {
            backgroundColor: theme.palette.secondary.light,
            cursor: 'no-drop',
        },
    };
});

export const CustomPasswordField = styled(FormControl)(({ theme }) => {
    return {
        width: theme.spacing('100%'),
        '& .MuiInputBase-root': {
            maxHeight: theme.spacing(7),
            backgroundColor: theme.palette.primary.dark,
            borderRadius: theme.spacing(1.25),
            '& .MuiInputBase-input': {
                color: theme.palette.primary.contrastText,
                fontSize: theme.spacing('0.875rem'),
                fontWeight: theme.typography.fontWeightRegular,
                padding: theme.spacing(2.25, 2),
            },
            '& .MuiSvgIcon-root': {
                fill: theme.palette.primary.contrastText,
            },
            '& .MuiOutlinedInput-notchedOutline': {
                borderColor: theme.palette.border.transparent,
            },
            '&:hover': {
                '& .MuiOutlinedInput-notchedOutline': {
                    borderColor: theme.palette.border.transparent,
                },
            },
            '&.Mui-focused, &.Mui-focused:hover': {
                '& .MuiOutlinedInput-notchedOutline': {
                    borderColor: theme.palette.border.green,
                },
            },
            '&.Mui-disabled': {
                backgroundColor: theme.palette.secondary.light,
                cursor: 'no-drop',
            },
        },
    };
});

export const PasswordInput = styled(OutlinedInput)(({ theme }) => {
    return {
        backgroundColor: theme.palette.primary.dark,
        '& .MuiInputBase-input': {
            borderRadius: theme.spacing(1.25, 0, 0, 1.25),
        },
        '&.Mui-disabled': {
            backgroundColor: theme.palette.secondary.light,
            cursor: 'no-drop',
        },
    };
});

export const CustomTextIconField = styled(FormControl)(({ theme }) => {
    return {
        width: theme.spacing('100%'),
        '& .MuiInputBase-root': {
            maxHeight: theme.spacing(7),
            backgroundColor: theme.palette.primary.dark,
            borderRadius: theme.spacing(1.25),
            '& .MuiInputBase-input': {
                color: theme.palette.primary.contrastText,
                fontSize: theme.spacing('0.875rem'),
                fontWeight: theme.typography.fontWeightRegular,
                padding: theme.spacing(2.25, 2),
            },
            '& .MuiSvgIcon-root': {
                fill: theme.palette.primary.contrastText,
            },
            '& .MuiOutlinedInput-notchedOutline': {
                borderColor: theme.palette.border.transparent,
            },
            '&:hover': {
                '& .MuiOutlinedInput-notchedOutline': {
                    borderColor: theme.palette.border.transparent,
                },
            },
            '&.Mui-focused, &.Mui-focused:hover': {
                '& .MuiOutlinedInput-notchedOutline': {
                    borderColor: theme.palette.border.green,
                },
            },
            '&.Mui-disabled': {
                backgroundColor: theme.palette.secondary.light,
                cursor: 'no-drop',
            },
        },
    };
});

export const IconInput = styled(OutlinedInput)(({ theme }) => {
    return {
        backgroundColor: theme.palette.primary.dark,
        '& .MuiInputBase-input': {
            borderRadius: theme.spacing(1.25, 0, 0, 1.25),
        },
        '&.Mui-disabled': {
            backgroundColor: theme.palette.secondary.light,
            cursor: 'no-drop',
        },
    };
});

export const CustomDatePicker = styled(FormControl)(({ theme }) => {
    return {
        width: theme.spacing('100%'),
        backgroundColor: theme.palette.primary.dark,
        borderRadius: theme.spacing(1.25),
        '& .MuiStack-root': {
            backgroundColor: theme.palette.primary.transparent,
            padding: theme.spacing(0),
            '& .MuiFormControl-root': {
                width: theme.spacing('100%'),
            },
        },
        '& .MuiInputBase-root': {
            maxHeight: theme.spacing(7),
            backgroundColor: theme.palette.primary.dark,
            borderRadius: theme.spacing(1.25),
            '& .MuiInputBase-input': {
                color: theme.palette.primary.contrastText,
                fontSize: theme.spacing('0.875rem'),
                fontWeight: theme.typography.fontWeightRegular,
                padding: theme.spacing(2.25, 2),
            },
            '& .MuiSvgIcon-root': {
                fill: theme.palette.primary.contrastText,
            },
            '& .MuiOutlinedInput-notchedOutline': {
                borderColor: theme.palette.border.transparent,
            },
            '&:hover': {
                '& .MuiOutlinedInput-notchedOutline': {
                    borderColor: theme.palette.border.transparent,
                },
            },
            '&.Mui-focused, &.Mui-focused:hover': {
                '& .MuiOutlinedInput-notchedOutline': {
                    borderColor: theme.palette.border.green,
                },
            },
            '&.Mui-disabled': {
                backgroundColor: theme.palette.secondary.light,
                cursor: 'no-drop',
            },
        },
        '&.Mui-disabled': {
            backgroundColor: theme.palette.secondary.light,
            cursor: 'no-drop',
        },
    };
});

export const CustomInputLabel = styled(InputLabel)(({ theme }) => {
    return {
        color: theme.palette.primary.contrastText,
        fontSize: theme.spacing('0.875rem'),
        fontWeight: theme.typography.fontWeightRegular,
        marginBottom: theme.spacing(1),
    };
});