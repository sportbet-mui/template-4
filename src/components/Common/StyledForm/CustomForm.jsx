import React from 'react'
import { CustomDatePicker, CustomFormWrapper, CustomInputLabel, CustomPasswordField, CustomSelectField, CustomTextField, PasswordInput } from './style';
import { useTheme } from '@emotion/react'
import { IconButton, InputAdornment, MenuItem } from '@mui/material';
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';

export default function CustomForm() {
  const theme = useTheme();

  return (
    <CustomFormWrapper theme={theme}>
      <CustomTextField/>

      <CustomSelectField>
        <MenuItem value='values'>Label</MenuItem>
      </CustomSelectField>

      <CustomPasswordField>
        <PasswordInput
          id='outlined-adornment-password'
          type={showPassword ? 'text' : 'password'}
          endAdornment={
            <InputAdornment position='end'>
              <IconButton
                aria-label='toggle password visibility'
                onClick={handleClickShowPassword}
                onMouseDown={handleMouseDownPassword}
                edge='end'
              >
                {showPassword ? <VisibilityOff /> : <Visibility />}
              </IconButton>
            </InputAdornment>
          }
          label='Password'
        />
      </CustomPasswordField>

      <CustomTextIconField>
        <IconInput
          id='outlined-adornment-weight'
          endAdornment={
            <InputAdornment position='end'>
              <IconButton
                aria-label='Icon Button'
                edge='end'
              >
                <InsertEmoticonRoundedIcon/>
              </IconButton>
            </InputAdornment>
          }
          inputProps={{
            'aria-label': 'icon',
          }}
        />
      </CustomTextIconField>

      <CustomDatePicker>
        <LocalizationProvider dateAdapter={AdapterDayjs}>
          <DemoContainer components={['DatePicker']}>
            <DatePicker/>
          </DemoContainer>
        </LocalizationProvider>
      </CustomDatePicker>

      <CustomInputLabel/>
    </CustomFormWrapper>
  )
}
