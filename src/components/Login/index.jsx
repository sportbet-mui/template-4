import React, { useState } from 'react'
import { LoginWrapper } from './style';
import { useTheme } from '@emotion/react'
import ForgotPassword from './ForgotPassword';
import CustomDialogBox from '../Common/StyledDialogBox/CustomDialogBox';
import { ContentWrapper } from '../Common/StyledDialogBox/style';
import { Box, Grid, Link, Typography, useMediaQuery } from '@mui/material';
import { CustomTextField } from '../Common/StyledForm/style';
import { CustomMainButton, CustomPrimaryButton, CustomTextButton } from '../Common/StyledButton/CustomButton';

export default function Login({ dialogOpen, handleClose, handleOpenSignupForm }) {
  const theme = useTheme();

  const [forgotPasswordForm, setForgotPassword] = useState(false);

  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const [maxWidth] = React.useState('md');

  return (
    <>
      <CustomDialogBox maxWidth={maxWidth} fullScreen={fullScreen} dialogOpen={dialogOpen} handleDialogClose={handleClose} aria-labelledby='customized-dialog-title'>
        <LoginWrapper theme={theme}>
          <ContentWrapper>
            <Box className='img-box'>
              <img src={'assets/images/stock-images/modal-img.png'} className='dialog-img' alt='Img' />
            </Box>
            <Box className='form-wrap login-form-wrap'>
              <Grid container spacing={2}>
                <Grid item sm={12} xs={12}>
                  <Box className='btn-wrap'>
                    <CustomMainButton>Log In</CustomMainButton>
                    <CustomPrimaryButton onClick={handleOpenSignupForm}>Sign Up</CustomPrimaryButton>
                  </Box>
                </Grid>
                <Grid item sm={8} xs={12}>
                  <CustomTextField placeholder='Enter Username' />
                </Grid>
                <Grid item sm={8} xs={12}>
                  <CustomTextField placeholder='Password' />
                </Grid>
                <Grid item sm={8} xs={12}>
                  <CustomTextField placeholder='Two factor authentication (If applicable)' />
                </Grid>
                <Grid item sm={8} xs={12}>
                  <CustomMainButton fullWidth>Log In</CustomMainButton>
                </Grid>
                <Grid item sm={8} xs={12}>
                  <Box className='text-btn-box'>
                    <CustomTextButton onClick={() => {handleClose(); setForgotPassword(true);}}>Forgot Password?</CustomTextButton>
                  </Box>
                </Grid>
                <Grid item sm={6} xs={12}>
                  <Typography variant='body1'>Having issues logging in?<br/> Contact <Link href='mailto:support@sportsbet.com'>support@sportsbet.com</Link> from<br/> your registered email for assistance.</Typography>
                </Grid>
              </Grid>
            </Box>
          </ContentWrapper>
        </LoginWrapper>
      </CustomDialogBox>
      <ForgotPassword dialogOpen={forgotPasswordForm} handleClose={() => setForgotPassword()} />
    </>
  )
}