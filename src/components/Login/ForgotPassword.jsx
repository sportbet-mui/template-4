import React from 'react'
import { ForgotPasswordWrapper } from './style';
import { useTheme } from '@emotion/react'
import CustomDialogBox from '../Common/StyledDialogBox/CustomDialogBox';
import { ContentWrapper } from '../Common/StyledDialogBox/style';
import { Box, Grid, Typography, useMediaQuery } from '@mui/material';
import { CustomTextField } from '../Common/StyledForm/style';
import { CustomMainButton } from '../Common/StyledButton/CustomButton';

export default function ForgotPassword({ dialogOpen, handleClose }) {
  const theme = useTheme();

  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const [maxWidth] = React.useState('xs');

  return (
    <CustomDialogBox maxWidth={maxWidth} fullScreen={fullScreen} dialogOpen={dialogOpen} handleDialogClose={handleClose} aria-labelledby='customized-dialog-title'>
      <ForgotPasswordWrapper theme={theme}>
        <ContentWrapper>
          <Box className='form-wrap'>
            <Typography variant='h1'>Forgot Password</Typography>
            <Grid container spacing={2}>
              <Grid item sm={12} xs={12}>
                <CustomTextField placeholder='Please enter the registered email' />
              </Grid>
              <Grid item sm={12} xs={12}>
                <CustomMainButton fullWidth>Submit</CustomMainButton>
              </Grid>
            </Grid>
          </Box>
        </ContentWrapper>
      </ForgotPasswordWrapper>
    </CustomDialogBox>
  )
}