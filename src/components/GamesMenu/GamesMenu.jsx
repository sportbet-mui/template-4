import React from 'react'
import { GamesMenuWrapper } from './style';
import { Accordion, AccordionDetails, AccordionSummary, Box, List, ListItem, ListItemAvatar, ListItemButton, ListItemText, Typography, useTheme } from '@mui/material';
import ArrowDropDownRoundedIcon from '@mui/icons-material/ArrowDropDownRounded';

export default function GamesMenu() {
  const theme = useTheme();

  const [expanded, setExpanded] = React.useState(false);

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  return (
    <GamesMenuWrapper theme={theme}>
      <Typography variant="h4">Menu</Typography>
      <Box className='accordion-wrap'>
        <Accordion expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
          <AccordionSummary
            expandIcon={<ArrowDropDownRoundedIcon />}
            aria-controls="panel1bh-content"
            id="panel1bh-header"
          >
            <Box className='img-box'>
              <img src={'assets/images/svg/icon-soccer.svg'} className='game-img' alt='Soccer' />
              <img src={'assets/images/svg/icon-soccer-active.svg'} className='game-img-active' alt='Soccer' />
            </Box>
            <Typography variant='h5'>Soccer</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <List>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-bangladesh.png'} className='flag-img' alt='Bangladesh' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Bangladesh' />
                    <Typography variant='h6'>20</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-brazil.png'} className='flag-img' alt='Brazil' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Brazil' />
                    <Typography variant='h6'>14</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-finland.png'} className='flag-img' alt='Finland' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Finland' />
                    <Typography variant='h6'>5</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-france.png'} className='flag-img' alt='France' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='France' />
                    <Typography variant='h6'>8</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-germany.png'} className='flag-img' alt='Germany' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Germany' />
                    <Typography variant='h6'>22</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-iran.png'} className='flag-img' alt='Iran' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Iran' />
                    <Typography variant='h6'>20</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-ireland.png'} className='flag-img' alt='Ireland' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Ireland' />
                    <Typography variant='h6'>14</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-mexico.png'} className='flag-img' alt='Mexico' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Mexico' />
                    <Typography variant='h6'>5</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-thailand.png'} className='flag-img' alt='Thailand' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Thailand' />
                    <Typography variant='h6'>8</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
            </List>
          </AccordionDetails>
        </Accordion>

        <Accordion expanded={expanded === 'panel2'} onChange={handleChange('panel2')}>
          <AccordionSummary
            expandIcon={<ArrowDropDownRoundedIcon />}
            aria-controls="panel2bh-content"
            id="panel2bh-header"
          >
            <Box className='img-box'>
              <img src={'assets/images/svg/icon-basketball.svg'} className='game-img' alt='Basketball' />
              <img src={'assets/images/svg/icon-basketball-active.svg'} className='game-img-active' alt='Basketball' />
            </Box>
            <Typography variant='h5'>Basketball</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <List>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-bangladesh.png'} className='flag-img' alt='Bangladesh' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Bangladesh' />
                    <Typography variant='h6'>20</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-brazil.png'} className='flag-img' alt='Brazil' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Brazil' />
                    <Typography variant='h6'>14</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-finland.png'} className='flag-img' alt='Finland' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Finland' />
                    <Typography variant='h6'>5</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-france.png'} className='flag-img' alt='France' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='France' />
                    <Typography variant='h6'>8</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-germany.png'} className='flag-img' alt='Germany' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Germany' />
                    <Typography variant='h6'>22</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-iran.png'} className='flag-img' alt='Iran' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Iran' />
                    <Typography variant='h6'>20</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-ireland.png'} className='flag-img' alt='Ireland' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Ireland' />
                    <Typography variant='h6'>14</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-mexico.png'} className='flag-img' alt='Mexico' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Mexico' />
                    <Typography variant='h6'>5</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-thailand.png'} className='flag-img' alt='Thailand' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Thailand' />
                    <Typography variant='h6'>8</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
            </List>
          </AccordionDetails>
        </Accordion>

        <Accordion expanded={expanded === 'panel3'} onChange={handleChange('panel3')}>
          <AccordionSummary
            expandIcon={<ArrowDropDownRoundedIcon />}
            aria-controls="panel3bh-content"
            id="panel3bh-header"
          >
            <Box className='img-box'>
              <img src={'assets/images/svg/icon-voleyball.svg'} className='game-img' alt='Voleyball' />
              <img src={'assets/images/svg/icon-voleyball-active.svg'} className='game-img-active' alt='Voleyball' />
            </Box>
            <Typography variant='h5'>Voleyball</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <List>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-bangladesh.png'} className='flag-img' alt='Bangladesh' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Bangladesh' />
                    <Typography variant='h6'>20</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-brazil.png'} className='flag-img' alt='Brazil' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Brazil' />
                    <Typography variant='h6'>14</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-finland.png'} className='flag-img' alt='Finland' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Finland' />
                    <Typography variant='h6'>5</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-france.png'} className='flag-img' alt='France' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='France' />
                    <Typography variant='h6'>8</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-germany.png'} className='flag-img' alt='Germany' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Germany' />
                    <Typography variant='h6'>22</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-iran.png'} className='flag-img' alt='Iran' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Iran' />
                    <Typography variant='h6'>20</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-ireland.png'} className='flag-img' alt='Ireland' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Ireland' />
                    <Typography variant='h6'>14</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-mexico.png'} className='flag-img' alt='Mexico' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Mexico' />
                    <Typography variant='h6'>5</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-thailand.png'} className='flag-img' alt='Thailand' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Thailand' />
                    <Typography variant='h6'>8</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
            </List>
          </AccordionDetails>
        </Accordion>

        <Accordion expanded={expanded === 'panel4'} onChange={handleChange('panel4')}>
          <AccordionSummary
            expandIcon={<ArrowDropDownRoundedIcon />}
            aria-controls="panel4bh-content"
            id="panel4bh-header"
          >
            <Box className='img-box'>
              <img src={'assets/images/svg/icon-tennis.svg'} className='game-img' alt='Tennis' />
              <img src={'assets/images/svg/icon-tennis-active.svg'} className='game-img-active' alt='Tennis' />
            </Box>
            <Typography variant='h5'>Tennis</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <List>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-bangladesh.png'} className='flag-img' alt='Bangladesh' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Bangladesh' />
                    <Typography variant='h6'>20</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-brazil.png'} className='flag-img' alt='Brazil' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Brazil' />
                    <Typography variant='h6'>14</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-finland.png'} className='flag-img' alt='Finland' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Finland' />
                    <Typography variant='h6'>5</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-france.png'} className='flag-img' alt='France' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='France' />
                    <Typography variant='h6'>8</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-germany.png'} className='flag-img' alt='Germany' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Germany' />
                    <Typography variant='h6'>22</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-iran.png'} className='flag-img' alt='Iran' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Iran' />
                    <Typography variant='h6'>20</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-ireland.png'} className='flag-img' alt='Ireland' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Ireland' />
                    <Typography variant='h6'>14</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-mexico.png'} className='flag-img' alt='Mexico' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Mexico' />
                    <Typography variant='h6'>5</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-thailand.png'} className='flag-img' alt='Thailand' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Thailand' />
                    <Typography variant='h6'>8</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
            </List>
          </AccordionDetails>
        </Accordion>

        <Accordion expanded={expanded === 'panel5'} onChange={handleChange('panel5')}>
          <AccordionSummary
            expandIcon={<ArrowDropDownRoundedIcon />}
            aria-controls="panel5bh-content"
            id="panel5bh-header"
          >
            <Box className='img-box'>
              <img src={'assets/images/svg/icon-rugby.svg'} className='game-img' alt='Rugby' />
              <img src={'assets/images/svg/icon-rugby-active.svg'} className='game-img-active' alt='Rugby' />
            </Box>
            <Typography variant='h5'>Rugby</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <List>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-bangladesh.png'} className='flag-img' alt='Bangladesh' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Bangladesh' />
                    <Typography variant='h6'>20</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-brazil.png'} className='flag-img' alt='Brazil' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Brazil' />
                    <Typography variant='h6'>14</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-finland.png'} className='flag-img' alt='Finland' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Finland' />
                    <Typography variant='h6'>5</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-france.png'} className='flag-img' alt='France' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='France' />
                    <Typography variant='h6'>8</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-germany.png'} className='flag-img' alt='Germany' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Germany' />
                    <Typography variant='h6'>22</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-iran.png'} className='flag-img' alt='Iran' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Iran' />
                    <Typography variant='h6'>20</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-ireland.png'} className='flag-img' alt='Ireland' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Ireland' />
                    <Typography variant='h6'>14</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-mexico.png'} className='flag-img' alt='Mexico' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Mexico' />
                    <Typography variant='h6'>5</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-thailand.png'} className='flag-img' alt='Thailand' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Thailand' />
                    <Typography variant='h6'>8</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
            </List>
          </AccordionDetails>
        </Accordion>

        <Accordion expanded={expanded === 'panel6'} onChange={handleChange('panel6')}>
          <AccordionSummary
            expandIcon={<ArrowDropDownRoundedIcon />}
            aria-controls="panel6bh-content"
            id="panel6bh-header"
          >
            <Box className='img-box'>
              <img src={'assets/images/svg/icon-badminton.svg'} className='game-img' alt='Badminton' />
              <img src={'assets/images/svg/icon-badminton-active.svg'} className='game-img-active' alt='Badminton' />
            </Box>
            <Typography variant='h5'>Badminton</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <List>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-bangladesh.png'} className='flag-img' alt='Bangladesh' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Bangladesh' />
                    <Typography variant='h6'>20</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-brazil.png'} className='flag-img' alt='Brazil' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Brazil' />
                    <Typography variant='h6'>14</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-finland.png'} className='flag-img' alt='Finland' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Finland' />
                    <Typography variant='h6'>5</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-france.png'} className='flag-img' alt='France' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='France' />
                    <Typography variant='h6'>8</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-germany.png'} className='flag-img' alt='Germany' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Germany' />
                    <Typography variant='h6'>22</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-iran.png'} className='flag-img' alt='Iran' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Iran' />
                    <Typography variant='h6'>20</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-ireland.png'} className='flag-img' alt='Ireland' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Ireland' />
                    <Typography variant='h6'>14</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-mexico.png'} className='flag-img' alt='Mexico' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Mexico' />
                    <Typography variant='h6'>5</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-thailand.png'} className='flag-img' alt='Thailand' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Thailand' />
                    <Typography variant='h6'>8</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
            </List>
          </AccordionDetails>
        </Accordion>

        <Accordion expanded={expanded === 'panel7'} onChange={handleChange('panel7')}>
          <AccordionSummary
            expandIcon={<ArrowDropDownRoundedIcon />}
            aria-controls="panel7bh-content"
            id="panel7bh-header"
          >
            <Box className='img-box'>
              <img src={'assets/images/svg/icon-boxing.svg'} className='game-img' alt='Boxing' />
              <img src={'assets/images/svg/icon-boxing-active.svg'} className='game-img-active' alt='Boxing' />
            </Box>
            <Typography variant='h5'>Boxing</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <List>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-bangladesh.png'} className='flag-img' alt='Bangladesh' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Bangladesh' />
                    <Typography variant='h6'>20</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-brazil.png'} className='flag-img' alt='Brazil' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Brazil' />
                    <Typography variant='h6'>14</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-finland.png'} className='flag-img' alt='Finland' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Finland' />
                    <Typography variant='h6'>5</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-france.png'} className='flag-img' alt='France' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='France' />
                    <Typography variant='h6'>8</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-germany.png'} className='flag-img' alt='Germany' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Germany' />
                    <Typography variant='h6'>22</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-iran.png'} className='flag-img' alt='Iran' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Iran' />
                    <Typography variant='h6'>20</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-ireland.png'} className='flag-img' alt='Ireland' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Ireland' />
                    <Typography variant='h6'>14</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-mexico.png'} className='flag-img' alt='Mexico' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Mexico' />
                    <Typography variant='h6'>5</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-thailand.png'} className='flag-img' alt='Thailand' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Thailand' />
                    <Typography variant='h6'>8</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
            </List>
          </AccordionDetails>
        </Accordion>

        <Accordion expanded={expanded === 'panel8'} onChange={handleChange('panel8')}>
          <AccordionSummary
            expandIcon={<ArrowDropDownRoundedIcon />}
            aria-controls="panel8bh-content"
            id="panel8bh-header"
          >
            <Box className='img-box'>
              <img src={'assets/images/svg/icon-table-tennis.svg'} className='game-img' alt='Table Tennis' />
              <img src={'assets/images/svg/icon-table-tennis-active.svg'} className='game-img-active' alt='Table Tennis' />
            </Box>
            <Typography variant='h5'>Table Tennis</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <List>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-bangladesh.png'} className='flag-img' alt='Bangladesh' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Bangladesh' />
                    <Typography variant='h6'>20</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-brazil.png'} className='flag-img' alt='Brazil' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Brazil' />
                    <Typography variant='h6'>14</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-finland.png'} className='flag-img' alt='Finland' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Finland' />
                    <Typography variant='h6'>5</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-france.png'} className='flag-img' alt='France' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='France' />
                    <Typography variant='h6'>8</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-germany.png'} className='flag-img' alt='Germany' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Germany' />
                    <Typography variant='h6'>22</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-iran.png'} className='flag-img' alt='Iran' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Iran' />
                    <Typography variant='h6'>20</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-ireland.png'} className='flag-img' alt='Ireland' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Ireland' />
                    <Typography variant='h6'>14</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-mexico.png'} className='flag-img' alt='Mexico' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Mexico' />
                    <Typography variant='h6'>5</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-thailand.png'} className='flag-img' alt='Thailand' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Thailand' />
                    <Typography variant='h6'>8</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
            </List>
          </AccordionDetails>
        </Accordion>

        <Accordion expanded={expanded === 'panel9'} onChange={handleChange('panel9')}>
          <AccordionSummary
            expandIcon={<ArrowDropDownRoundedIcon />}
            aria-controls="panel9bh-content"
            id="panel9bh-header"
          >
            <Box className='img-box'>
              <img src={'assets/images/svg/icon-cricket.svg'} className='game-img' alt='Cricket' />
              <img src={'assets/images/svg/icon-cricket-active.svg'} className='game-img-active' alt='Cricket' />
            </Box>
            <Typography variant='h5'>Cricket</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <List>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-bangladesh.png'} className='flag-img' alt='Bangladesh' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Bangladesh' />
                    <Typography variant='h6'>20</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-brazil.png'} className='flag-img' alt='Brazil' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Brazil' />
                    <Typography variant='h6'>14</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-finland.png'} className='flag-img' alt='Finland' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Finland' />
                    <Typography variant='h6'>5</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-france.png'} className='flag-img' alt='France' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='France' />
                    <Typography variant='h6'>8</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-germany.png'} className='flag-img' alt='Germany' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Germany' />
                    <Typography variant='h6'>22</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-iran.png'} className='flag-img' alt='Iran' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Iran' />
                    <Typography variant='h6'>20</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-ireland.png'} className='flag-img' alt='Ireland' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Ireland' />
                    <Typography variant='h6'>14</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-mexico.png'} className='flag-img' alt='Mexico' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Mexico' />
                    <Typography variant='h6'>5</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-thailand.png'} className='flag-img' alt='Thailand' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Thailand' />
                    <Typography variant='h6'>8</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
            </List>
          </AccordionDetails>
        </Accordion>

        <Accordion expanded={expanded === 'panel10'} onChange={handleChange('panel10')}>
          <AccordionSummary
            expandIcon={<ArrowDropDownRoundedIcon />}
            aria-controls="panel10bh-content"
            id="panel10bh-header"
          >
            <Box className='img-box'>
              <img src={'assets/images/svg/icon-golf.svg'} className='game-img' alt='Golf' />
              <img src={'assets/images/svg/icon-golf-active.svg'} className='game-img-active' alt='Golf' />
            </Box>
            <Typography variant='h5'>Golf</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <List>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-bangladesh.png'} className='flag-img' alt='Bangladesh' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Bangladesh' />
                    <Typography variant='h6'>20</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-brazil.png'} className='flag-img' alt='Brazil' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Brazil' />
                    <Typography variant='h6'>14</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-finland.png'} className='flag-img' alt='Finland' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Finland' />
                    <Typography variant='h6'>5</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-france.png'} className='flag-img' alt='France' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='France' />
                    <Typography variant='h6'>8</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-germany.png'} className='flag-img' alt='Germany' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Germany' />
                    <Typography variant='h6'>22</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-iran.png'} className='flag-img' alt='Iran' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Iran' />
                    <Typography variant='h6'>20</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-ireland.png'} className='flag-img' alt='Ireland' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Ireland' />
                    <Typography variant='h6'>14</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-mexico.png'} className='flag-img' alt='Mexico' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Mexico' />
                    <Typography variant='h6'>5</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-thailand.png'} className='flag-img' alt='Thailand' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Thailand' />
                    <Typography variant='h6'>8</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
            </List>
          </AccordionDetails>
        </Accordion>

        <Accordion expanded={expanded === 'panel11'} onChange={handleChange('panel11')}>
          <AccordionSummary
            expandIcon={<ArrowDropDownRoundedIcon />}
            aria-controls="panel11bh-content"
            id="panel11bh-header"
          >
            <Box className='img-box'>
              <img src={'assets/images/svg/icon-baseball.svg'} className='game-img' alt='Baseball' />
              <img src={'assets/images/svg/icon-baseball-active.svg'} className='game-img-active' alt='Baseball' />
            </Box>
            <Typography variant='h5'>Baseball</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <List>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-bangladesh.png'} className='flag-img' alt='Bangladesh' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Bangladesh' />
                    <Typography variant='h6'>20</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-brazil.png'} className='flag-img' alt='Brazil' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Brazil' />
                    <Typography variant='h6'>14</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-finland.png'} className='flag-img' alt='Finland' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Finland' />
                    <Typography variant='h6'>5</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-france.png'} className='flag-img' alt='France' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='France' />
                    <Typography variant='h6'>8</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-germany.png'} className='flag-img' alt='Germany' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Germany' />
                    <Typography variant='h6'>22</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-iran.png'} className='flag-img' alt='Iran' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Iran' />
                    <Typography variant='h6'>20</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-ireland.png'} className='flag-img' alt='Ireland' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Ireland' />
                    <Typography variant='h6'>14</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-mexico.png'} className='flag-img' alt='Mexico' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Mexico' />
                    <Typography variant='h6'>5</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-thailand.png'} className='flag-img' alt='Thailand' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Thailand' />
                    <Typography variant='h6'>8</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
            </List>
          </AccordionDetails>
        </Accordion>

        <Accordion expanded={expanded === 'panel12'} onChange={handleChange('panel12')}>
          <AccordionSummary
            expandIcon={<ArrowDropDownRoundedIcon />}
            aria-controls="panel12bh-content"
            id="panel12bh-header"
          >
            <Box className='img-box'>
              <img src={'assets/images/svg/icon-hockey.svg'} className='game-img' alt='Hockey' />
              <img src={'assets/images/svg/icon-hockey-active.svg'} className='game-img-active' alt='Hockey' />
            </Box>
            <Typography variant='h5'>Hockey</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <List>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-bangladesh.png'} className='flag-img' alt='Bangladesh' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Bangladesh' />
                    <Typography variant='h6'>20</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-brazil.png'} className='flag-img' alt='Brazil' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Brazil' />
                    <Typography variant='h6'>14</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-finland.png'} className='flag-img' alt='Finland' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Finland' />
                    <Typography variant='h6'>5</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-france.png'} className='flag-img' alt='France' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='France' />
                    <Typography variant='h6'>8</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-germany.png'} className='flag-img' alt='Germany' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Germany' />
                    <Typography variant='h6'>22</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-iran.png'} className='flag-img' alt='Iran' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Iran' />
                    <Typography variant='h6'>20</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-ireland.png'} className='flag-img' alt='Ireland' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Ireland' />
                    <Typography variant='h6'>14</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-mexico.png'} className='flag-img' alt='Mexico' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Mexico' />
                    <Typography variant='h6'>5</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-thailand.png'} className='flag-img' alt='Thailand' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Thailand' />
                    <Typography variant='h6'>8</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
            </List>
          </AccordionDetails>
        </Accordion>

        <Accordion expanded={expanded === 'panel13'} onChange={handleChange('panel13')}>
          <AccordionSummary
            expandIcon={<ArrowDropDownRoundedIcon />}
            aria-controls="panel13bh-content"
            id="panel13bh-header"
          >
            <Box className='img-box'>
              <img src={'assets/images/svg/icon-bowling.svg'} className='game-img' alt='Bowling' />
              <img src={'assets/images/svg/icon-bowling-active.svg'} className='game-img-active' alt='Bowling' />
            </Box>
            <Typography variant='h5'>Bowling</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <List>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-bangladesh.png'} className='flag-img' alt='Bangladesh' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Bangladesh' />
                    <Typography variant='h6'>20</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-brazil.png'} className='flag-img' alt='Brazil' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Brazil' />
                    <Typography variant='h6'>14</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-finland.png'} className='flag-img' alt='Finland' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Finland' />
                    <Typography variant='h6'>5</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-france.png'} className='flag-img' alt='France' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='France' />
                    <Typography variant='h6'>8</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-germany.png'} className='flag-img' alt='Germany' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Germany' />
                    <Typography variant='h6'>22</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-iran.png'} className='flag-img' alt='Iran' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Iran' />
                    <Typography variant='h6'>20</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-ireland.png'} className='flag-img' alt='Ireland' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Ireland' />
                    <Typography variant='h6'>14</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-mexico.png'} className='flag-img' alt='Mexico' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Mexico' />
                    <Typography variant='h6'>5</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-thailand.png'} className='flag-img' alt='Thailand' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Thailand' />
                    <Typography variant='h6'>8</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
            </List>
          </AccordionDetails>
        </Accordion>

        <Accordion expanded={expanded === 'panel14'} onChange={handleChange('panel14')}>
          <AccordionSummary
            expandIcon={<ArrowDropDownRoundedIcon />}
            aria-controls="panel14bh-content"
            id="panel14bh-header"
          >
            <Box className='img-box'>
              <img src={'assets/images/svg/icon-darts.svg'} className='game-img' alt='Darts' />
              <img src={'assets/images/svg/icon-darts-active.svg'} className='game-img-active' alt='Darts' />
            </Box>
            <Typography variant='h5'>Darts</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <List>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-bangladesh.png'} className='flag-img' alt='Bangladesh' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Bangladesh' />
                    <Typography variant='h6'>20</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-brazil.png'} className='flag-img' alt='Brazil' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Brazil' />
                    <Typography variant='h6'>14</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-finland.png'} className='flag-img' alt='Finland' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Finland' />
                    <Typography variant='h6'>5</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-france.png'} className='flag-img' alt='France' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='France' />
                    <Typography variant='h6'>8</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-germany.png'} className='flag-img' alt='Germany' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Germany' />
                    <Typography variant='h6'>22</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-iran.png'} className='flag-img' alt='Iran' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Iran' />
                    <Typography variant='h6'>20</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-ireland.png'} className='flag-img' alt='Ireland' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Ireland' />
                    <Typography variant='h6'>14</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-mexico.png'} className='flag-img' alt='Mexico' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Mexico' />
                    <Typography variant='h6'>5</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-thailand.png'} className='flag-img' alt='Thailand' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Thailand' />
                    <Typography variant='h6'>8</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
            </List>
          </AccordionDetails>
        </Accordion>

        <Accordion expanded={expanded === 'panel15'} onChange={handleChange('panel15')}>
          <AccordionSummary
            expandIcon={<ArrowDropDownRoundedIcon />}
            aria-controls="panel15bh-content"
            id="panel15bh-header"
          >
            <Box className='img-box'>
              <img src={'assets/images/svg/icon-motorsports.svg'} className='game-img' alt='Motorsports' />
              <img src={'assets/images/svg/icon-motorsports-active.svg'} className='game-img-active' alt='Motorsports' />
            </Box>
            <Typography variant='h5'>Motorsports</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <List>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-bangladesh.png'} className='flag-img' alt='Bangladesh' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Bangladesh' />
                    <Typography variant='h6'>20</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-brazil.png'} className='flag-img' alt='Brazil' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Brazil' />
                    <Typography variant='h6'>14</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-finland.png'} className='flag-img' alt='Finland' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Finland' />
                    <Typography variant='h6'>5</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-france.png'} className='flag-img' alt='France' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='France' />
                    <Typography variant='h6'>8</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-germany.png'} className='flag-img' alt='Germany' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Germany' />
                    <Typography variant='h6'>22</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-iran.png'} className='flag-img' alt='Iran' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Iran' />
                    <Typography variant='h6'>20</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-ireland.png'} className='flag-img' alt='Ireland' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Ireland' />
                    <Typography variant='h6'>14</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-mexico.png'} className='flag-img' alt='Mexico' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Mexico' />
                    <Typography variant='h6'>5</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemAvatar>
                    <img src={'assets/images/stock-images/fl-thailand.png'} className='flag-img' alt='Thailand' />
                  </ListItemAvatar>
                  <Box className='text-box'>
                    <ListItemText primary='Thailand' />
                    <Typography variant='h6'>8</Typography>
                  </Box>
                </ListItemButton>
              </ListItem>
            </List>
          </AccordionDetails>
        </Accordion>
      </Box>
    </GamesMenuWrapper>
  )
}
