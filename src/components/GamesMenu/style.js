import { Box, styled } from '@mui/material';

export const GamesMenuWrapper = styled(Box)(({ theme }) => {
    return {
        '& .MuiTypography-h4': {
            color: theme.palette.primary.contrastText,
            fontSize: theme.spacing('1rem'),
            fontWeight: theme.typography.fontWeightMedium,
            marginBottom: theme.spacing(1.25),
        },
        '& .accordion-wrap': {
            '& .MuiAccordion-root': {
                backgroundColor: theme.palette.primary.transparent,
                borderRadius: theme.spacing(0),
                boxShadow: theme.shadows[0],
                '&::before': {
                    display: 'none',
                },
                '&.Mui-expanded': {
                    margin: theme.spacing(0),
                },
                '& .MuiAccordionSummary-root': {
                    padding: theme.spacing(0),
                    '&.Mui-expanded': {
                        minHeight: theme.spacing('unset'),
                    },
                    '& .MuiAccordionSummary-content': {
                        display: 'flex',
                        alignItems: 'center',
                        '& .img-box': {
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginRight: theme.spacing(2),
                            '& .game-img-active': {
                                display: 'none',
                            },
                        },
                        '& .MuiTypography-h5': {
                            color: theme.palette.primary.contrastTextLight,
                            fontSize: theme.spacing('0.875rem'),
                            fontWeight: theme.typography.fontWeightMedium,
                        },
                        '&.Mui-expanded': {
                            margin: theme.spacing(1.5, 0),
                            '& .img-box': {
                                '& .game-img': {
                                    display: 'none',
                                },
                                '& .game-img-active': {
                                    display: 'block',
                                },
                            },
                            '& .MuiTypography-h5': {
                                color: theme.palette.primary.contrastText,
                            },
                        },
                    },
                },
                '& .MuiAccordionSummary-expandIconWrapper': {
                    '& .MuiSvgIcon-root': {
                        fill: theme.palette.primary.contrastTextLight,
                    },
                    '&.Mui-expanded': {
                        '& .MuiSvgIcon-root': {
                            fill: theme.palette.primary.contrastText,
                        },
                    },
                },
                '& .MuiCollapse-root': {
                    borderWidth: theme.spacing(0.125, 0, 0.125, 0),
                    borderStyle: 'solid',
                    borderColor: theme.palette.border.dark,
                    '& .MuiAccordionDetails-root': {
                        padding: theme.spacing(2, 0.5, 2, 1),
                        '& .MuiList-root': {
                            maxHeight: theme.spacing(28.75),
                            overflowY: 'auto',
                            padding: theme.spacing(0),
                            '&::-webkit-scrollbar-track': {
                                backgroundColor: theme.palette.scrollbar.track,
                                borderRadius: theme.spacing(0.5),
                            },
                            '&::-webkit-scrollbar': {
                                width: theme.spacing(0.625),
                                height: theme.spacing(0.625),
                            },
                            '&::-webkit-scrollbar-thumb': {
                                backgroundColor: theme.palette.scrollbar.thumb,
                                borderRadius: theme.spacing(0.5),
                            },
                            '& .MuiListItem-root': {
                                '& .MuiListItemButton-root': {
                                    padding: theme.spacing(1, 0),
                                    '& .MuiListItemAvatar-root': {
                                        minWidth: theme.spacing('unset'),
                                        display: 'flex',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        marginRight: theme.spacing(1.25),
                                        '& .flag-img': {
                                            width: theme.spacing(2.5),
                                            height: theme.spacing(2.5),
                                        },
                                    },
                                    '& .text-box': {
                                        width: theme.spacing('100%'),
                                        display: 'flex',
                                        justifyContent: 'space-between',
                                        alignItems: 'center',
                                        '& .MuiListItemText-root': {
                                            '& .MuiTypography-body1': {
                                                color: theme.palette.primary.contrastTextLight,
                                                fontSize: theme.spacing('0.875rem'),
                                                fontWeight: theme.typography.fontWeightRegular,
                                                margin: theme.spacing(0),
                                                transition: '0.3s all',
                                            },
                                        },
                                        '& .MuiTypography-h6': {
                                            color: theme.palette.primary.contrastTextLight,
                                            fontSize: theme.spacing('0.875rem'),
                                            fontWeight: theme.typography.fontWeightRegular,
                                            margin: theme.spacing(0, 2),
                                            transition: '0.3s all',
                                        },
                                    },
                                    '&:hover': {
                                        backgroundColor: theme.palette.primary.transparent,
                                        '& .text-box': {
                                            '& .MuiListItemText-root, & .MuiTypography-h6': {
                                                color: theme.palette.primary.contrastTextActive,
                                                transition: '0.3s all',
                                            },
                                        },
                                    },
                                },
                            },
                        },
                    },
                },
            },
        },
    };
});