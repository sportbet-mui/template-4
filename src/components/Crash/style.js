import { Box, styled } from '@mui/material';

export const CrashWrapper = styled(Box)(({ theme }) => {
    return {
        '& .game-wrap': {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            flexWrap: 'wrap',
            margin: theme.spacing(-1, -1, 5),
            '& .card-holder': {
                width: theme.spacing('calc(100% / 7)'),
                padding: theme.spacing(1),
                [theme.breakpoints.down('xm')]: {
                    width: theme.spacing('calc(100% / 5)'),
                },
                [theme.breakpoints.down('md')]: {
                    width: theme.spacing('calc(100% / 4)'),
                },
                [theme.breakpoints.down('sm')]: {
                    width: theme.spacing('calc(100% / 3)'),
                },
                '& .MuiCard-root': {
                    backgroundColor: theme.palette.primary.light,
                    borderRadius: theme.spacing(1.25),
                    boxShadow: theme.shadows[2],
                    '& .MuiCardContent-root': {
                        padding: theme.spacing(1.5),
                        '& .MuiTypography-h6': {
                            color: theme.palette.primary.contrastTextLight,
                            fontSize: theme.spacing('1rem'),
                            fontWeight: theme.typography.fontWeightRegular,
                            whiteSpace: 'nowrap',
                            textOverflow: 'ellipsis',
                            overflow: 'hidden',
                            [theme.breakpoints.down('xl')]: {
                                fontSize: theme.spacing('0.875rem'),
                            },
                        },
                    },
                },
            },
        },
        '& .table-wrap': {
            height: theme.spacing('100%'),
            marginBottom: theme.spacing(5),
        },
    };
});

export const CrashGameWrapper = styled(Box)(({ theme }) => {
    return {
        minHeight: theme.spacing('calc(100vh - 99px)'),
        backgroundColor: theme.palette.primary.light,
        borderRadius: theme.spacing(1.25),
        padding: theme.spacing(3),
        position: 'relative',
        '& .crash-game-wrap': {
            '& img': {
                width: theme.spacing('100%'),
                maxWidth: theme.spacing('100%'),
                height: theme.spacing('calc(100vh - 154px);'),
            },
        },
        '& .content-wrap': {
            width: theme.spacing('calc(100% - 48px)'),
            height: theme.spacing('calc(100% - 48px)'),
            padding: theme.spacing(0, 3),
            position: 'absolute',
            top: theme.spacing(3),
            left: theme.spacing(3),
            right: theme.spacing(3),
            bottom: theme.spacing(3),
            '& > .MuiGrid-container': {
                height: theme.spacing('100%'),
            },
            '& .bet-place-column': {
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center',
                flexDirection: 'column',
            },
            '& .bets-column': {
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'flex-end',
            },
            [theme.breakpoints.down('lg')]: {
                width: theme.spacing('100%'),
                padding: theme.spacing(0, 0),
                position: 'static',
            },
        },
        '& .bet-btn-wrap': {
            width: theme.spacing('100%'),
            display: 'flex',
            flexWrap: 'wrap',
            margin: theme.spacing(-0.5, -0.5, 3 ),
            [theme.breakpoints.down('lg')]: {
                width: theme.spacing('calc(100% - 48px)'),
                padding: theme.spacing(3, 3, 0),
                position: 'absolute',
                top: theme.spacing(0),
            },
            '& .bet-btn': {
                minWidth: theme.spacing('unset'),
                height: theme.spacing(3.75),
                borderRadius: theme.spacing(0.625),
                color: theme.palette.primary.contrastText,
                fontSize: theme.spacing('0.75rem'),
                fontWeight: theme.typography.fontWeightBold,
                boxShadow: theme.shadows[0],
                padding: theme.spacing(0.75, 1),
                margin: theme.spacing(0.5),
                '&.green-btn': {
                    backgroundColor: theme.palette.button.main,
                },
                '&.red-btn': {
                    backgroundColor: theme.palette.danger.main,
                },
                '&.default-btn': {
                    backgroundColor: theme.palette.secondary.contrastTextActive, 
                },
            },
        },
        '& .switch-box': {
            height: theme.spacing('100%'),
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'flex-start',
            flexDirection: 'column',
            '& .MuiSwitch-root': {
                background: theme.palette.gradient.tabBgActiveFlip,
                borderWidth: theme.spacing(0.125),
                borderStyle: 'solid',
                borderColor: theme.palette.border.active,
                borderRadius: theme.spacing(2.5),
                padding: theme.spacing(0),
                '& .MuiButtonBase-root': {
                    color: theme.palette.primary.contrastTextLight,
                    padding: theme.spacing(1),
                    '&.Mui-checked': {
                        color: theme.palette.primary.contrastTextActive,
                    },
                },
                '& .MuiSwitch-track': {
                    opacity: '0',
                },
            },
        },
        '& .bet-content-box': {
            width: theme.spacing('100%'),
            height: theme.spacing('100%'),
            background: theme.palette.gradient.tabBgActiveFlip,
            borderWidth: theme.spacing(0.125),
            borderStyle: 'solid',
            borderColor: theme.palette.border.active,
            borderRadius: theme.spacing(1.25),
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            padding: theme.spacing(1, 2),
            position: 'relative',
            '& .ice-img': {
                position: 'absolute',
                top: theme.spacing(-1.5),
                left: theme.spacing(1.25),
            },
            '& .text-box': {
                width: theme.spacing('100%'),
                height: theme.spacing('100%'),
                flexGrow: '1',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'column',
                '& .MuiTypography-h5': {
                    color: theme.palette.primary.contrastText,
                    fontSize: theme.spacing('1.75rem'),
                    fontWeight: theme.typography.fontWeightMedium,
                    textAlign: 'center',
                    marginBottom: theme.spacing(1.25),
                },
                '& .MuiTypography-h6': {
                    color: theme.palette.primary.contrastTextLight,
                    fontSize: theme.spacing('0.875rem'),
                    fontWeight: theme.typography.fontWeightRegular,
                    textAlign: 'center',
                    '& span': {
                        color: theme.palette.primary.contrastText,
                    },
                },
            },
            '& .btn-box': {
                maxWidth: theme.spacing(6.5),
                margin: theme.spacing(-0.5, 0),
                '& .MuiButtonBase-root': {
                    minWidth: theme.spacing(5),
                    height: theme.spacing(4.5),
                    fontSize: theme.spacing('0.875rem'),
                    fontWeight: theme.typography.fontWeightRegular,
                    padding: theme.spacing(0.75, 1),
                    margin: theme.spacing(0.5, 0),
                },
            },
        },
        '& .bet-btn': {
            background: theme.palette.button.main,
            borderRadius: theme.spacing(1.25),
            boxShadow: theme.shadows[5],
            color: theme.palette.primary.contrastText,
            fontSize: theme.spacing('2.5rem'),
            fontWeight: theme.typography.fontWeightMedium,
            textTransform: 'uppercase',
            padding: theme.spacing(2, 2),
            '&:hover, &.active': {
                background: theme.palette.button.main,
            },
        },
        '& .bets-wrap': {
            width: theme.spacing('100%'),
            height: theme.spacing('40vh'),
            position: 'relative',
            '&.expand': {
                height: theme.spacing('100%'),
            },
            [theme.breakpoints.down('lg')]: {
                marginTop: theme.spacing(3),
            },
            '& .ice-img': {
                position: 'absolute',
                top: theme.spacing(-2.5),
                right: theme.spacing(1.25),
            },
            '& .MuiCard-root': {
                width: theme.spacing('100%'),
                height: theme.spacing('100%'),
                background: theme.palette.gradient.tabBgActiveFlip,
                borderWidth: theme.spacing(0.125),
                borderStyle: 'solid',
                borderColor: theme.palette.border.active,
                borderRadius: theme.spacing(1.25),
                padding: theme.spacing(1, 2, 2),
                '& .MuiCardHeader-root': {
                    justifyContent: 'center',
                    padding: theme.spacing(0, 0, 1),
                    '& .MuiCardHeader-content': {
                        display: 'none',
                    },
                    '& .MuiIconButton-root': {
                        '& .MuiSvgIcon-root': {
                            fill: theme.palette.primary.contrastTextLight,
                            transform: 'scale(1.5)',
                        },
                    },
                },
                '& .MuiCardContent-root': {
                    height: theme.spacing('100%'),
                    padding: theme.spacing(0),
                    '& .balance-box': {
                        display: 'flex',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        margin: theme.spacing(0, 0, 1),
                        '& .MuiTypography-caption': {
                            color: theme.palette.primary.contrastText,
                            fontSize: theme.spacing('0.875rem'),
                            fontWeight: theme.typography.fontWeightMedium,
                        },
                        '& .MuiButton-text': {
                            height: theme.spacing('auto'),
                            backgroundColor: theme.palette.secondary.dark, 
                            color: theme.palette.primary.contrastText,
                            fontSize: theme.spacing('0.75rem'),
                            fontWeight: theme.typography.fontWeightRegular,
                            padding: theme.spacing(1, 2),
                            '& .MuiSvgIcon-root': {
                                fill: theme.palette.primary.contrastTextLight,
                            },
                            '&:hover': {
                                backgroundColor: theme.palette.primary.contrastText, 
                                color: theme.palette.primary.contrastTextActive,
                                '& .MuiSvgIcon-root': {
                                    fill: theme.palette.primary.contrastTextActive,
                                },
                            },
                        },
                    },
                    '& .table-wrap': {
                        marginBottom: theme.spacing(0),
                        '& .MuiPaper-root': {
                            height: theme.spacing('100%'),
                            backgroundColor: theme.palette.primary.transparent, 
                            '& .MuiTable-root': {
                                minWidth: theme.spacing(0),
                                '& .MuiTableHead-root': {
                                    '& .MuiTableRow-root': {
                                        backgroundColor: theme.palette.primary.transparent, 
                                        '& .MuiTableCell-root': {
                                            color: theme.palette.primary.contrastTextLight,
                                            fontSize: theme.spacing('0.875rem'),
                                            fontWeight: theme.typography.fontWeightMedium,
                                            padding: theme.spacing(0.75),
                                        },
                                    },
                                },
                                '& .MuiTableBody-root': {
                                    '& .MuiTableRow-root': {
                                        backgroundColor: theme.palette.primary.transparent, 
                                        '& .MuiTableCell-root': {
                                            borderWidth: theme.spacing(0, 0, 0.125, 0),
                                            borderStyle: 'solid',
                                            borderColor: theme.palette.border.grey,
                                            color: theme.palette.primary.contrastText,
                                            fontSize: theme.spacing('0.875rem'),
                                            fontWeight: theme.typography.fontWeightRegular,
                                            padding: theme.spacing(0.875),
                                        },
                                    },
                                },
                            },
                        },
                    },
                    '& .MuiTabs-root': {
                        minHeight: theme.spacing('auto'),
                        margin: theme.spacing(0, -0.5, 1),
                        '& .MuiButtonBase-root': {
                            minWidth: theme.spacing(7.5),
                            minHeight: theme.spacing(3.75),
                            backgroundColor: theme.palette.primary.transparent,
                            borderWidth: theme.spacing(0, 0, 0.375, 0),
                            borderStyle: 'solid',
                            borderColor: theme.palette.border.main,
                            color: theme.palette.primary.contrastTextLight,
                            fontSize: theme.spacing('0.875rem'),
                            fontWeight: theme.typography.fontWeightMedium,
                            textTransform: 'capitalize',
                            padding: theme.spacing(0.75, 1.75),
                            margin: theme.spacing(0, 0.5),
                            '&.Mui-selected': {
                                backgroundColor: theme.palette.primary.transparent,
                                borderColor: theme.palette.border.active,
                                color: theme.palette.primary.contrastTextActive,
                            },
                            '&.MuiTabScrollButton-root': {
                                width: theme.spacing(2.5),
                                minWidth: theme.spacing('unset'),
                                backgroundColor: theme.palette.button.transparent,
                                color: theme.palette.primary.contrastTextActive,
                                opacity: '1',
                                padding: theme.spacing(0),
                            },
                        },
                        '& .MuiTabs-indicator': {
                            display: 'none',
                        },
                    },
                    '& .tab-panel-wrap': {
                        height: theme.spacing('calc(100% - 115px)'),
                        '&:nth-last-of-type(2)': {
                            height: theme.spacing('calc(100% - 70px)'),
                        },
                        '& .tab-panel': {
                            height: theme.spacing('100%'),
                        },
                    },
                },
            },
        },
    };
});