import React from 'react'
import { CrashWrapper } from './style';
import CrashGame from './CrashGame';
import { useTheme } from '@emotion/react';
import { Box, Card, CardContent, CardMedia, Container, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from '@mui/material';
import { HeadingWrapper, StyledHeadingTypography } from '../Common/StyledTypography/style';
import { TableWrapper } from '../Common/style';

export default function Crash() {
  const theme = useTheme();

  return (
    <CrashWrapper theme={theme}>
      <CrashGame/>

      <Box className='games-category-wrap'>
        <HeadingWrapper>
          <StyledHeadingTypography>
            <img src={'assets/images/svg/sb-originals.svg'} className='heading-img' alt='Img'/>
            More From SB
          </StyledHeadingTypography>
        </HeadingWrapper>
        <Box className='game-wrap'>
          <Box className='card-holder'>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/sb-originals-img-01.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Dino Odyssey</Typography>
              </CardContent>
            </Card>
          </Box>
          <Box className='card-holder'>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/sb-originals-img-02.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Dino Odyssey</Typography>
              </CardContent>
            </Card>
          </Box>
          <Box className='card-holder'>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/sb-originals-img-03.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Dino Odyssey</Typography>
              </CardContent>
            </Card>
          </Box>
          <Box className='card-holder'>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/sb-originals-img-04.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Dino Odyssey</Typography>
              </CardContent>
            </Card>
          </Box>
          <Box className='card-holder'>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/sb-originals-img-05.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Dino Odyssey</Typography>
              </CardContent>
            </Card>
          </Box>
          <Box className='card-holder'>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/sb-originals-img-06.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Dino Odyssey</Typography>
              </CardContent>
            </Card>
          </Box>
          <Box className='card-holder'>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/sb-originals-img-07.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Dino Odyssey</Typography>
              </CardContent>
            </Card>
          </Box>
          <Box className='card-holder'>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/sb-originals-img-08.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Dino Odyssey</Typography>
              </CardContent>
            </Card>
          </Box>
          <Box className='card-holder'>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/sb-originals-img-09.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Dino Odyssey</Typography>
              </CardContent>
            </Card>
          </Box>
          <Box className='card-holder'>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/sb-originals-img-10.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Dino Odyssey</Typography>
              </CardContent>
            </Card>
          </Box>
          <Box className='card-holder'>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/sb-originals-img-11.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Griffin’s Quest</Typography>
              </CardContent>
            </Card>
          </Box>
          <Box className='card-holder'>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/sb-originals-img-12.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Griffin’s Quest</Typography>
              </CardContent>
            </Card>
          </Box>
          <Box className='card-holder'>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/sb-originals-img-13.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Joker Times</Typography>
              </CardContent>
            </Card>
          </Box>
          <Box className='card-holder'>
            <Card>
              <CardMedia component='img' image='assets/images/stock-images/sb-originals-img-14.png' alt='Img'/>
              <CardContent>
                <Typography variant='h6'>Joker Times</Typography>
              </CardContent>
            </Card>
          </Box>
        </Box>
      </Box>
      
      <Box className='bets-wrap'>
        <HeadingWrapper theme={theme}>
          <StyledHeadingTypography>
            <img src={'assets/images/svg/bets.svg'} className='heading-img' alt='Img'/>
            Bets
          </StyledHeadingTypography>
        </HeadingWrapper>
        <Container maxWidth='lg'>
          <TableWrapper theme={theme} className='table-wrap'>
            <TableContainer component={Paper}>
              <Table aria-label='simple table'>
                <TableHead>
                  <TableRow>
                    <TableCell>Game</TableCell>
                    <TableCell>Player Name</TableCell>
                    <TableCell>Wager</TableCell>
                    <TableCell>Multiplier</TableCell>
                    <TableCell>Payout</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {Array(10).fill('').map((item)=>{
                    return (    
                      <TableRow>
                        <TableCell>Joker Supreme</TableCell>
                        <TableCell>Josh Ludin</TableCell>
                        <TableCell>$100</TableCell>
                        <TableCell>0.30x</TableCell>
                        <TableCell>$100</TableCell>
                      </TableRow>
                    )
                  })}
                </TableBody>
              </Table>
            </TableContainer>
          </TableWrapper>
        </Container>
      </Box>
    </CrashWrapper>
  )
}
