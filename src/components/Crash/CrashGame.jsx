import React from 'react'
import { CrashGameWrapper } from './style';
import { useTheme } from '@emotion/react';
import { Box, Button, Card, CardContent, CardHeader, Grid, IconButton, Paper, Switch, Tab, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Tabs, Typography } from '@mui/material';
import { CustomMainButton, CustomTextButton } from '../Common/StyledButton/CustomButton';
import ExpandMoreRoundedIcon from '@mui/icons-material/ExpandMoreRounded';
import ExpandLessRoundedIcon from '@mui/icons-material/ExpandLessRounded';
import HistoryRoundedIcon from '@mui/icons-material/HistoryRounded';
import AddRoundedIcon from '@mui/icons-material/AddRounded';
import RemoveRoundedIcon from '@mui/icons-material/RemoveRounded';
import { TableWrapper } from '../Common/style';
import { CustomInputLabel } from '../Common/StyledForm/style';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      className='tab-panel-wrap'
      role='tabpanel'
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box className='tab-panel'>
          {children}
        </Box>
      )}
    </div>
  );
}

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

export default function CrashGame() {
  const theme = useTheme();

  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const label = { inputProps: { 'aria-label': 'Switch demo' } };

  return (
    <CrashGameWrapper theme={theme}>
      <Box className='crash-game-wrap'>
        <img src={'assets/images/stock-images/crash-game-bg.png'} alt='Img' />
      </Box>
      <Box className='content-wrap'>
        <Grid container spacing={2}>
          <Grid item lg={9} xs={12} className='bet-place-column'>
            <Box className='bet-btn-wrap'>
              <Button className='bet-btn green-btn'>2.32x</Button>
              <Button className='bet-btn green-btn'>2.32x</Button>
              <Button className='bet-btn default-btn'>2.32x</Button>
              <Button className='bet-btn red-btn'>2.32x</Button>
              <Button className='bet-btn green-btn'>2.32x</Button>
              <Button className='bet-btn default-btn'>2.32x</Button>
              <Button className='bet-btn red-btn'>2.32x</Button>
              <Button className='bet-btn green-btn'>2.32x</Button>
              <Button className='bet-btn default-btn'><HistoryRoundedIcon /></Button>
            </Box>
            <Grid container spacing={2}>
              <Grid item lg={1} xs={2}>
                <Box className='switch-box'>
                  <CustomInputLabel>Auto Bet</CustomInputLabel>
                  <Switch {...label} />
                </Box>
              </Grid>
              <Grid item lg={4} xs={10}>
                <Box className='bet-content-box'>
                  <Box className='btn-box'>
                    <CustomMainButton fullWidth>Min</CustomMainButton>
                    <CustomMainButton fullWidth>Max</CustomMainButton>
                  </Box>
                  <Box className='text-box'>
                    <Typography variant='h5'>$10</Typography>
                    <Typography variant='h6'>Available Bal. <span>$10000</span></Typography>
                  </Box>
                  <Box className='btn-box'>
                    <CustomMainButton fullWidth>1/2</CustomMainButton>
                    <CustomMainButton fullWidth>2x</CustomMainButton>
                  </Box>
                </Box>
              </Grid>
              <Grid item lg={4} xs={8}>
                <Box className='bet-content-box'>
                  <img src={'assets/images/stock-images/ice-2.png'} className='ice-img' alt='Img' />
                  <Box className='btn-box'>
                    <CustomMainButton><RemoveRoundedIcon/></CustomMainButton>
                  </Box>
                  <Box className='text-box'>
                    <Typography variant='h5'>2.10x</Typography>
                    <Typography variant='h6'>Cash Out At</Typography>
                  </Box>
                  <Box className='btn-box'>
                    <CustomMainButton><AddRoundedIcon/></CustomMainButton>
                  </Box>
                </Box>
              </Grid>
              <Grid item lg={3} xs={4}>
                <Button fullWidth className='bet-btn'>Bet</Button>
              </Grid>
            </Grid>
          </Grid>
          <Grid item lg={3} xs={12} className='bets-column'>
            <Box className='bets-wrap'>
              <img src={'assets/images/stock-images/ice-1.png'} className='ice-img' alt='Img' />
              <Card>
                <CardHeader action={<IconButton aria-label="settings"><ExpandLessRoundedIcon /></IconButton>} />

                <CardContent>
                  <Tabs variant='fullWidth' centered value={value} onChange={handleChange} aria-label='basic tabs example'>
                    <Tab label='All Bet' {...a11yProps(0)} />
                    <Tab label='My Bets' {...a11yProps(1)} />
                    <Tab label='Top' {...a11yProps(2)} />
                  </Tabs>

                  <TabPanel value={value} index={0}>
                    <Box className='balance-box'>
                      <Typography variant='caption'>Total Bets: 112</Typography>
                      <CustomTextButton startIcon={<HistoryRoundedIcon />}>Previous Hand</CustomTextButton>
                    </Box>
                    <TableWrapper theme={theme} className='table-wrap'>
                      <TableContainer component={Paper}>
                        <Table aria-label='simple table'>
                          <TableHead>
                            <TableRow>
                              <TableCell align='center'>User</TableCell>
                              <TableCell align='center'>Bet</TableCell>
                              <TableCell align='center'>Mult.</TableCell>
                              <TableCell align='center'>Cashout</TableCell>
                            </TableRow>
                          </TableHead>
                          <TableBody>
                            {Array(10).fill('').map((item)=>{
                              return (
                                <TableRow>
                                  <TableCell align='center'>User Name</TableCell>
                                  <TableCell align='center'>$100.00</TableCell>
                                  <TableCell align='center'>2.0x</TableCell>
                                  <TableCell align='center'>$200.00</TableCell>
                                </TableRow>
                              )
                            })}
                          </TableBody>
                        </Table>
                      </TableContainer>
                    </TableWrapper>
                  </TabPanel>

                  <TabPanel value={value} index={1}>
                    <TableWrapper theme={theme} className='table-wrap'>
                      <TableContainer component={Paper}>
                        <Table aria-label='simple table'>
                          <TableHead>
                            <TableRow>
                              <TableCell align='center'>User</TableCell>
                              <TableCell align='center'>Bet</TableCell>
                              <TableCell align='center'>Mult.</TableCell>
                              <TableCell align='center'>Cashout</TableCell>
                            </TableRow>
                          </TableHead>
                          <TableBody>
                            {Array(10).fill('').map((item)=>{
                              return (
                                <TableRow>
                                  <TableCell align='center'>User Name</TableCell>
                                  <TableCell align='center'>$100.00</TableCell>
                                  <TableCell align='center'>2.0x</TableCell>
                                  <TableCell align='center'>$200.00</TableCell>
                                </TableRow>
                              )
                            })}
                          </TableBody>
                        </Table>
                      </TableContainer>
                    </TableWrapper>
                  </TabPanel>

                  <TabPanel value={value} index={2}>
                    <Box className='balance-box'>
                      <CustomTextButton>Huge Wins</CustomTextButton>
                      <CustomTextButton>Biggest Wins</CustomTextButton>
                      <CustomTextButton>Multipliers</CustomTextButton>
                    </Box>
                    <TableWrapper theme={theme} className='table-wrap'>
                      <TableContainer component={Paper}>
                        <Table aria-label='simple table'>
                          <TableHead>
                            <TableRow>
                              <TableCell align='center'>User</TableCell>
                              <TableCell align='center'>Cashout</TableCell>
                              <TableCell align='center'>Win</TableCell>
                            </TableRow>
                          </TableHead>
                          <TableBody>
                            {Array(10).fill('').map((item)=>{
                              return (
                                <TableRow>
                                  <TableCell align='center'>User Name</TableCell>
                                  <TableCell align='center'>$200.00</TableCell>
                                  <TableCell align='center'>$100.00</TableCell>
                                </TableRow>
                              )
                            })}
                          </TableBody>
                        </Table>
                      </TableContainer>
                    </TableWrapper>
                  </TabPanel>
                </CardContent>
              </Card>
            </Box>
          </Grid>
        </Grid>
      </Box>
    </CrashGameWrapper>
  )
}
