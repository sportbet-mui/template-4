import React, { useState } from 'react'
import { SignupWrapper } from './style';
import { useTheme } from '@emotion/react'
import CustomDialogBox from '../Common/StyledDialogBox/CustomDialogBox';
import { ContentWrapper } from '../Common/StyledDialogBox/style';
import { Box, Grid, IconButton, InputAdornment, Link, MenuItem, Typography, useMediaQuery } from '@mui/material';
import { CustomPasswordField, CustomSelectField, CustomTextField, PasswordInput } from '../Common/StyledForm/style';
import { CustomMainButton, CustomPrimaryButton } from '../Common/StyledButton/CustomButton';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';

export default function Signup({ dialogOpen, handleClose, handleOpenLoginForm }) {
  const theme = useTheme();

  const [showPassword, setShowPassword] = React.useState(false);
  const handleClickShowPassword = () => setShowPassword((show) => !show);
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const [showConfirmPassword, setShowConfirmPassword] = React.useState(false);
  const handleClickShowConfirmPassword = () => setShowConfirmPassword((show) => !show);
  const handleMouseDownConfirmPassword = (event) => {
    event.preventDefault();
  };

  const [selectTypeState,setTypeState]=useState(1);

  const [selectTypeCountry,setTypeCountry]=useState(1);

  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const [maxWidth] = React.useState('xm');

  return (
    <>
      <CustomDialogBox maxWidth={maxWidth} fullScreen={fullScreen} dialogOpen={dialogOpen} handleDialogClose={handleClose} aria-labelledby='customized-dialog-title'>
        <SignupWrapper theme={theme}>
          <ContentWrapper>
            <Box className='img-box'>
              <img src={'assets/images/stock-images/modal-img.png'} className='dialog-img' alt='Img' />
            </Box>
            <Box className='form-wrap signup-form-wrap'>
              <Grid container spacing={2}>
                <Grid item sm={12} xs={12}>
                  <Box className='btn-wrap'>
                    <CustomPrimaryButton onClick={handleOpenLoginForm}>Log In</CustomPrimaryButton>
                    <CustomMainButton>Sign Up</CustomMainButton>
                  </Box>
                </Grid>
                <Grid item sm={6} xs={12}>
                  <CustomTextField placeholder='Enter Username' />
                </Grid>
                <Grid item sm={6} xs={12}>
                  <CustomTextField placeholder='Full Name' />
                </Grid>
                <Grid item sm={6} xs={12}>
                  <CustomPasswordField>
                    <PasswordInput
                      placeholder='Password'
                      id='outlined-adornment-password'
                      type={showPassword ? 'text' : 'password'}
                      endAdornment={
                        <InputAdornment position='end'>
                          <IconButton
                            aria-label='toggle password visibility'
                            onClick={handleClickShowPassword}
                            onMouseDown={handleMouseDownPassword}
                            edge='end'
                          >
                            {showPassword ? <VisibilityOff /> : <Visibility />}
                          </IconButton>
                        </InputAdornment>
                      }
                    />
                  </CustomPasswordField>
                </Grid>
                <Grid item sm={6} xs={12}>
                  <CustomPasswordField>
                    <PasswordInput
                      placeholder='Confirm Password'
                      id='outlined-adornment-password'
                      type={showConfirmPassword ? 'text' : 'password'}
                      endAdornment={
                        <InputAdornment position='end'>
                          <IconButton
                            aria-label='toggle password visibility'
                            onClick={handleClickShowConfirmPassword}
                            onMouseDown={handleMouseDownConfirmPassword}
                            edge='end'
                          >
                            {showConfirmPassword ? <VisibilityOff /> : <Visibility />}
                          </IconButton>
                        </InputAdornment>
                      }
                    />
                  </CustomPasswordField>
                </Grid>
                <Grid item sm={6} xs={12}>
                  <CustomTextField placeholder='Enter Email Address' />
                </Grid>
                <Grid item sm={6} xs={12}>
                  <CustomTextField placeholder='Enter Mobile Number' />
                </Grid>
                <Grid item sm={6} xs={12}>
                  <CustomTextField placeholder='Address' />
                </Grid>
                <Grid item sm={6} xs={12}>
                  <CustomSelectField value={selectTypeState} onChange={setTypeState}>
                    <MenuItem value={selectTypeState}>State 1</MenuItem>
                    <MenuItem value={selectTypeState}>State 2</MenuItem>
                    <MenuItem value={selectTypeState}>State 3</MenuItem>
                  </CustomSelectField>
                </Grid>
                <Grid item sm={6} xs={12}>
                  <CustomSelectField value={selectTypeCountry} onChange={setTypeCountry}>
                    <MenuItem value={selectTypeCountry}>Country 1</MenuItem>
                    <MenuItem value={selectTypeCountry}>Country 2</MenuItem>
                    <MenuItem value={selectTypeCountry}>Country 3</MenuItem>
                  </CustomSelectField>
                </Grid>
                <Grid item sm={6} xs={12}>
                  <CustomTextField placeholder='ZIP Code' />
                </Grid>
                <Grid item sm={12} xs={12}>
                  <Box className='human-test-box'>
                    <img src={'assets/images/stock-images/human-test.png'} alt='Img' />
                  </Box>
                </Grid>
                <Grid item sm={6} xs={12}>
                  <CustomMainButton fullWidth>Create New Account</CustomMainButton>
                </Grid>
                <Grid item sm={12} xs={12}>
                  <Typography variant='body1'>Having issues Signing Up?<br/> Contact <Link href='mailto:support@sportsbet.com'>support@sportsbet.com</Link> from your registered email for assistance.</Typography>
                </Grid>
              </Grid>
            </Box>
          </ContentWrapper>
        </SignupWrapper>
      </CustomDialogBox>
    </>
  )
}
