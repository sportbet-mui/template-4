import { Box, styled } from '@mui/material';

export const Styledlayout = styled(Box)(({ theme }) => {
  return {
    width: theme.spacing('100%'),
    backgroundColor: theme.palette.primary.main,
    '& .main-wrapper': {
      minHeight: theme.spacing('100vh'),
      padding: theme.spacing(2),
      marginLeft: theme.spacing(9.5),
      [theme.breakpoints.down('sm')]: {
          marginLeft: theme.spacing(0),
      },
    },
    '& .MuiContainer-root': {
      [theme.breakpoints.down('md')]: {
        paddingLeft: theme.spacing(0),
        paddingRight: theme.spacing(0),
      },
    },
  };
});
