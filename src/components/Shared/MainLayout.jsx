import React, {useState} from 'react'
import { Styledlayout } from './style'
import Header from './Header'
import SideBar from './SideBar'
import Footer from './Footer'
import Home from '../Home'
import { Box } from '@mui/material'
import { Outlet } from 'react-router-dom'

export default function MainLayout() {
  const [ openSidebar , setOpenSidebar] = useState(false)
  return (
    <Styledlayout component="main">
      <Header setOpenSidebar={setOpenSidebar} openSidebar={openSidebar}/>
      <SideBar openSidebar={openSidebar}/>
      <Box className="main-wrapper">
        <Outlet/>
      </Box>
      <Footer/>
    </Styledlayout>
  )
}
