import { Box, styled } from '@mui/material';

export const SidebarWrapper = styled(Box)(({ theme }) => {
    return {
        width: theme.spacing(9.5),
        backgroundColor: theme.palette.primary.dark,
        overflow: 'hidden',
        position: 'fixed',
        zIndex: '2',
        paddingTop: theme.spacing(8.375),
        position: 'fixed',
        top: theme.spacing(0),
        left: theme.spacing(0),
        transition: '0.3s all',
        '& .MuiPaper-root': {
            backgroundColor: theme.palette.primary.transparent,
            borderRadius: theme.spacing(0),
            boxShadow: theme.shadows[0],
            '& .MuiList-root': {
                height: theme.spacing('100vh'),
                overflowY: 'auto',
                paddingBottom: theme.spacing(8.375),
                '&::-webkit-scrollbar-track': {
                    backgroundColor: theme.palette.scrollbar.transparent,
                    borderRadius: theme.spacing(0.5),
                },
                '&::-webkit-scrollbar': {
                    width: theme.spacing(0.625),
                    height: theme.spacing(0.625),
                },
                '&::-webkit-scrollbar-thumb': {
                    backgroundColor: theme.palette.scrollbar.thumb,
                    borderRadius: theme.spacing(0.5),
                },
                '& .MuiMenuItem-root': {
                    padding: theme.spacing(0),
                    '& .nav-link': {
                        width: theme.spacing('100%'),
                        textDecoration: 'none',
                        display: 'flex',
                        padding: theme.spacing(2, 0),
                        '& .MuiListItemIcon-root': {
                            width: theme.spacing(9.5),
                            minWidth: theme.spacing(9.5),
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                            '& svg': {
                                fill: theme.palette.secondary.light,
                                transition: '0.3s all',
                            },
                        },
                        '& .MuiListItemText-root': {
                            color: theme.palette.secondary.light,
                            fontSize: theme.spacing('1rem'),
                            fontWeight: theme.typography.fontWeightMedium,
                            transition: '0.3s all',
                        },
                        '&:hover': {
                            '& .MuiListItemIcon-root svg': {
                                fill: theme.palette.primary.contrastTextLight,
                                transition: '0.3s all',
                            },
                            '& .MuiListItemText-root': {
                                color: theme.palette.primary.contrastTextLight,
                                transition: '0.3s all',
                            },
                        },
                        '&.active': {
                            '& .MuiListItemIcon-root svg': {
                                fill: theme.palette.primary.contrastTextActive,
                                transition: '0.3s all',
                            },
                            '& .MuiListItemText-root': {
                                color: theme.palette.primary.contrastTextActive,
                                transition: '0.3s all',
                            },
                        },
                    },
                },
            },
        },
        '& .MuiDivider-root': {
            borderColor: theme.palette.secondary.light,
            marginTop: theme.spacing(2),
            marginBottom: theme.spacing(2),
        },
        '&:hover': {
            width: theme.spacing(25),
            transition: '0.3s all',
        },
        [theme.breakpoints.down('sm')]: {
            '&.sidebar-expand': {
                width: theme.spacing(25),
                left: theme.spacing(0),
                transition: '0.3s all',
            },
            '&.sidebar-shrink': {
                width: theme.spacing(25),
                left: theme.spacing('-100%'),
                transition: '0.6s all',                
            },
        },
    };
});