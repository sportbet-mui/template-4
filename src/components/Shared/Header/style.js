import { Box, styled } from '@mui/material';

export const HeaderWrapper = styled(Box)(({ theme }) => {
    return {
        backgroundColor: theme.palette.primary.light,
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexWrap: 'nowrap',
        zIndex: '3',
        position: 'sticky',
        top: theme.spacing(0),
        padding: theme.spacing(1.5, 11.5),
        '& .brand-logo-wrap': {
            display: 'flex',
            alignItems: 'center',
            '& .brand-logo': {
                width: theme.spacing(30),
                maxWidth: theme.spacing('100%'),
                height: theme.spacing('auto'),
                display: 'flex',
                alignItems: 'center',
            },
        },
        '& .nav-wrap': {
            display: 'flex',
            marginLeft: theme.spacing(2),
            '& .MuiButtonBase-root': {
                marginLeft: theme.spacing(2),
                '&:first-child': {
                    marginLeft: theme.spacing(0),
                },
                '& svg': {
                    transform: 'scale(1.25)',
                },
                '& .MuiTypography-root': {
                    [theme.breakpoints.down('sm')]: {
                        display: 'none',
                    },
                },
                '&.signup-btn, &.login-btn': {
                    '& svg': {
                        marginRight: theme.spacing(1),
                        [theme.breakpoints.down('sm')]: {
                            marginRight: theme.spacing(0),
                        },
                    },
                },
                '&.signup-btn, &.login-btn, &.mobile-menu-btn': {
                    [theme.breakpoints.down('sm')]: {
                        minWidth: theme.spacing(5.375),
                        padding: theme.spacing(1),
                    },
                },
                '&.mobile-menu-btn': {
                    [theme.breakpoints.up('sm')]: {
                        display: 'none',
                    },
                },
            },
        },
        [theme.breakpoints.down('lg')]: {
            padding: theme.spacing(1.5, 3),
        },
        [theme.breakpoints.down('md')]: {
            '& .brand-logo-wrap': {
                '& .brand-logo': {
                    width: theme.spacing(22.5),
                },
            },
        },
        [theme.breakpoints.down('sm')]: {
            padding: theme.spacing(1.5, 2),
            '& .brand-logo-wrap': {
                '& .brand-logo': {
                    width: theme.spacing(10),
                },
            },
            '& .nav-wrap': {
                '& .MuiButtonBase-root': {
                    marginLeft: theme.spacing(1),
                },
            },
        },
    };
});

export const UserNavWrapper = styled(Box)(({ theme }) => {
    return {
        display: 'flex',
        '& .wallet-btn': {
            height: theme.spacing(5.375),
            backgroundColor: theme.palette.primary.dark,
            borderWidth: theme.spacing(0.175),
            borderStyle: 'solid',
            borderColor: theme.palette.primary.light,
            borderRadius: theme.spacing(1.25, 2.75, 2.75, 1.25),
            color: theme.palette.primary.contrastText,
            fontSize: theme.spacing('1rem'),
            fontWeight: theme.typography.fontWeightMedium,
            textTransform: 'capitalize',
            padding: theme.spacing(1.5, 8.75, 1.5, 1.5),
            position: 'relative',
            '& .btn-icon': {
                marginRight: theme.spacing(1),
            },
            '& .MuiTypography-root': {
                fontSize: theme.spacing('1rem'),
                fontWeight: theme.typography.fontWeightMedium,
                display: 'flex',
                alignItems: 'center',
            },
            '& svg': {
                fill: theme.palette.primary.contrastTextLight,
                marginLeft: theme.spacing(1),
            },
            '& .wallet-icon-box': {
                width: theme.spacing(5.375),
                height: theme.spacing(5.375),
                backgroundColor: theme.palette.button.main,
                borderWidth: theme.spacing(0.175),
                borderStyle: 'solid',
                borderColor: theme.palette.border.green,
                borderRadius: theme.spacing(2.75),
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                PointerEvent: 'none',
                position: 'absolute',
                right: theme.spacing(0),
            },
            [theme.breakpoints.down('sm')]: {                        
                borderRadius: theme.spacing(1.25),
                padding: theme.spacing(1.5),
                '& .MuiTypography-root': {
                    fontSize: theme.spacing('0.75rem'),
                    display: 'inline-block !important',
                    '& svg': {
                        display: 'none',
                    },
                },
                '& .wallet-icon-box': {
                    display: 'none',
                },
            },
        },
        '& .profile-btn': {
            height: theme.spacing(5.375),
            backgroundColor: theme.palette.primary.transparent,
            borderWidth: theme.spacing(0.175),
            borderStyle: 'solid',
            borderColor: theme.palette.primary.light,
            borderRadius: theme.spacing(1.25),
            color: theme.palette.primary.contrastText,
            fontSize: theme.spacing('1rem'),
            fontWeight: theme.typography.fontWeightMedium,
            textTransform: 'capitalize',
            padding: theme.spacing(0.1875),
            '& .user-img': {
                width: theme.spacing(5),
                height: theme.spacing(5),
                marginRight: theme.spacing(1),
            },
            '& .MuiTypography-root': {
                fontSize: theme.spacing('1rem'),
                fontWeight: theme.typography.fontWeightMedium,
                display: 'flex',
                alignItems: 'center',
            },
            '& svg': {
                fill: theme.palette.primary.contrastText,
                transform: 'scale(1.5)',
                marginLeft: theme.spacing(1.5),
            },
            [theme.breakpoints.down('sm')]: {
                minWidth: theme.spacing('auto'),
                padding: theme.spacing(0.1875),
                '& .user-img': {
                    marginRight: theme.spacing(0),
                },
                '& .MuiTypography-root': {
                    fontSize: theme.spacing('0.75rem'),
                    display: 'none',
                },
            },
        },
    };
});

export const LoginSignupWrapper = styled(Box)(({ theme }) => {
    return {
    };
});