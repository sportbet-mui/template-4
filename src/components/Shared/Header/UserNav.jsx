import React from 'react'
import { UserNavWrapper } from './style';
import { useTheme } from '@emotion/react'
import { Box, Button, MenuItem, Typography } from '@mui/material';
import { CustomMainMenu } from '../../Common/CustomMenu/CustomMenu';
import { useNavigate } from 'react-router-dom';

const settings = [
  {
    title: 'My Account',
    img: 'assets/images/svg/user.svg',
  },
  {
    title: 'Wallet',
    img: 'assets/images/svg/wallet.svg'
  },
  {
    title: 'Responsible Gambling',
    img: 'assets/images/svg/responsible-gambling.svg'
  },
  {
    title: 'Logout',
    img: 'assets/images/svg/logout.svg'
  }
];

export default function UserNav() {
  const theme = useTheme();
  
  const [anchorElUser, setAnchorElUser] = React.useState(null);
  
  const navigate = useNavigate();

  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  const handleMenuClick = (type) => {
    if (type === 'My Account') {
      navigate('/my-account');
    } else if (type === 'Wallet') {
      navigate('/wallet');
    } else if (type === 'Responsible Gambling') {
      navigate('/responsible-gambling');
    } else if (type === 'Logout') {
      handleCloseUserMenu();
      // signOut();
      navigate('/login', { replace: true });
    }
  };

  return (
    <UserNavWrapper theme={theme}>
      <Button className='dropdown-btn wallet-btn'>
        <img src={'assets/images/svg/bitcoin.svg'} className='btn-icon' alt='Bitcoin' />
        <Typography>0.000000
          <svg x='0px' y='0px' width='8px' height='6px' viewBox='0 0 8 6' enable-background='new 0 0 8 6'>
            <path d='M4,5.25c-0.192,0-0.384-0.073-0.53-0.22l-3-3c-0.293-0.293-0.293-0.768,0-1.061s0.768-0.293,1.061,0L4,3.439
            l2.47-2.47c0.293-0.293,0.768-0.293,1.061,0s0.293,0.768,0,1.061l-3,3C4.384,5.177,4.192,5.25,4,5.25z'/>
          </svg>
        </Typography>
        <Box className='wallet-icon-box'>
          <img src={'assets/images/svg/icon-wallet.svg'} alt='Wallet' />
        </Box>
      </Button>

      <Button className='dropdown-btn profile-btn' onClick={handleOpenUserMenu}>
        <img src={'assets/images/stock-images/user-img.png'} className='user-img' alt='Bitcoin' />
        <Typography>Rietanolieya
          <svg x='0px' y='0px' width='8px' height='6px' viewBox='0 0 8 6' enable-background='new 0 0 8 6'>
            <path d='M4,5.25c-0.192,0-0.384-0.073-0.53-0.22l-3-3c-0.293-0.293-0.293-0.768,0-1.061s0.768-0.293,1.061,0L4,3.439
            l2.47-2.47c0.293-0.293,0.768-0.293,1.061,0s0.293,0.768,0,1.061l-3,3C4.384,5.177,4.192,5.25,4,5.25z'/>
          </svg>
        </Typography>
      </Button>

      <CustomMainMenu
        id="menu-appbar"
        anchorEl={anchorElUser}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        keepMounted
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={Boolean(anchorElUser)}
        onClose={handleCloseUserMenu}
      >
        {settings.map((setting) => (
          <MenuItem key={setting} onClick={() => handleMenuClick(setting?.title)}>
            <img src={setting?.img} className="icon-img" alt="Icon" />
            <Typography textAlign="center">{setting?.title}</Typography>
          </MenuItem>
        ))}
      </CustomMainMenu>
    </UserNavWrapper>
  )
}
