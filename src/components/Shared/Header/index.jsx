import React, { useState } from 'react'
import { HeaderWrapper } from './style';
import UserNav from './UserNav';
import LoginSignup from './LoginSignup';
import { Box, Link, useTheme } from '@mui/material';
import { CustomPrimaryButton } from '../../Common/StyledButton/CustomButton';
import MenuRoundedIcon from '@mui/icons-material/MenuRounded';
import CloseRoundedIcon from '@mui/icons-material/CloseRounded';

export default function Header({setOpenSidebar, openSidebar}) {
  const theme = useTheme();

  return (
    <HeaderWrapper theme={theme}>
      <Box className='brand-logo-wrap'>
        <Link href='#' underline='none'>
          <img src={'assets/images/logo/brand-logo.svg'} className='brand-logo' alt='Brand Logo' />
        </Link>
      </Box>
      <Box className='nav-wrap'>
        {/* <LoginSignup/> */}

        <UserNav/>

        <CustomPrimaryButton className='mobile-menu-btn' onClick={()=>{setOpenSidebar((prev)=> prev? false: true)}}>
          {!openSidebar? (
            <MenuRoundedIcon/>
            ) : (
            <CloseRoundedIcon/>
          )}          
        </CustomPrimaryButton>
      </Box>
    </HeaderWrapper>
  )
}
