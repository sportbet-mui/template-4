import React, { useState } from 'react'
import { LoginSignupWrapper } from './style';
import { useTheme } from '@emotion/react';
import { CustomMainButton, CustomPrimaryButton } from '../../Common/StyledButton/CustomButton';
import { Typography } from '@mui/material';
import Login from '../../Login';
import Signup from '../../Signup';

export default function LoginSignup({setOpenSidebar, openSidebar}) {
  const theme = useTheme();

  const [loginForm,setLoginForm]=useState(false);
  const [signupForm,setSignupForm]=useState(false);

  return (
    <LoginSignupWrapper theme={theme}>
      <CustomMainButton className='signup-btn' onClick={()=>{setSignupForm(true)}}>
        <svg x='0px' y='0px' width='18px' height='13.695px' viewBox='3 5.152 18 13.695' enable-background='new 3 5.152 18 13.695'>
          <g>
            <path d='M11.205,13.313h-4.26C4.765,13.313,3,15.078,3,17.258c0,0.879,0.71,1.59,1.589,1.59h8.971c0.879,0,1.59-0.711,1.59-1.59C15.15,15.078,13.386,13.313,11.205,13.313z'/>
            <path d='M9.075,12.189c1.945,0,3.519-1.576,3.519-3.518c0-1.944-1.575-3.519-3.519-3.519c-1.943,0-3.518,1.576-3.518,3.519C5.556,10.613,7.132,12.189,9.075,12.189z'/>
            <path d='M19.89,9.155h-1.441V7.714c0-0.615-0.497-1.111-1.11-1.111c-0.615,0-1.109,0.496-1.109,1.111v1.441h-1.442c-0.61,0-1.11,0.496-1.11,1.11c0,0.611,0.496,1.111,1.11,1.111h1.442v1.441c0,0.61,0.494,1.109,1.109,1.109c0.611,0,1.11-0.493,1.11-1.109v-1.44h1.441c0.614,0,1.11-0.496,1.11-1.111C21,9.655,20.504,9.155,19.89,9.155z'/>
          </g>
        </svg>
        <Typography>Register</Typography>
      </CustomMainButton>
      <CustomPrimaryButton className='login-btn' onClick={()=>{setLoginForm(true)}}>
        <svg x='0px' y='0px' width='18px' height='13.697px' viewBox='2.996 5.151 18 13.697' enable-background='new 2.996 5.151 18 13.697'>
          <g>
            <path d='M11.202,13.313H6.941c-2.18,0-3.945,1.767-3.945,3.945c0,0.88,0.711,1.59,1.59,1.59h8.973c0.879,0,1.59-0.71,1.59-1.59C15.148,15.078,13.384,13.313,11.202,13.313z'/>
            <path d='M9.072,12.189c1.945,0,3.52-1.577,3.52-3.519c0-1.945-1.575-3.519-3.52-3.519c-1.943,0-3.518,1.576-3.518,3.519C5.553,10.612,7.129,12.189,9.072,12.189z'/>
            <path d='M18.068,11.671c0.771,0.115,1.587-0.125,2.182-0.72c0.994-0.993,0.994-2.607,0-3.603c-0.994-0.995-2.609-0.995-3.605,0c-0.593,0.594-0.832,1.41-0.718,2.183l-1.479,1.478c-0.091,0.089-0.141,0.212-0.141,0.34v1.464c0,0.263,0.213,0.479,0.479,0.479h1.464c0.127,0,0.249-0.052,0.338-0.142L18.068,11.671z M17.959,9.64c-0.269-0.269-0.269-0.705,0-0.977c0.27-0.269,0.708-0.269,0.978,0c0.27,0.271,0.27,0.707,0,0.977S18.23,9.909,17.959,9.64z'/>
          </g>
        </svg>
        <Typography>Log In</Typography>
      </CustomPrimaryButton>

      <Login dialogOpen={loginForm} handleClose={()=>setLoginForm()} handleOpenSignupForm={()=>{setLoginForm(false);setSignupForm(true)}} />
      <Signup dialogOpen={signupForm} handleClose={()=>setSignupForm()} handleOpenLoginForm={()=>{setSignupForm(false);setLoginForm(true)}} />
    </LoginSignupWrapper>
  )
}
