import React from 'react'
import { Box, ButtonBase, Grid, List, ListItem, ListItemButton, ListItemIcon, ListItemText, MenuItem, MenuList, Typography, useTheme } from '@mui/material';
import { FooterWrapper } from './style';

export default function Footer() {
  const theme = useTheme();
  return (
    <FooterWrapper theme={theme}>
      <Box className='footer-container'>
        <Grid container columnSpacing={1}>
          <Grid item lg={4} md={6} sm={12} xs={12}>
            <Box className='logo-wrap'>
              <ButtonBase className='logo-box'>
                <img src={'assets/images/logo/brand-logo.svg'} alt='Logo' />
              </ButtonBase>
              <Typography variant="body1">Sports Bet is built by gamblers for gamblers. With our unique community and a huge selection of games like HashDice, Plinko, Slots, and many more. Your greatest casino adventure is waiting for you!</Typography>
            </Box>
          </Grid>
          <Grid item lg={3} md={6} sm={12} xs={12}>
            <Box className='footer-link-box'>
              <Typography variant='h6'>Help & Advice</Typography>
              <Grid container columnSpacing={1}>
                <Grid item xs={6}>
                  <List>
                    <ListItem disablePadding>
                      <ListItemButton>
                        <ListItemText primary='Play Responsibility' />
                      </ListItemButton>
                    </ListItem>
                    <ListItem disablePadding>
                      <ListItemButton>
                        <ListItemText primary='About Us' />
                      </ListItemButton>
                    </ListItem>
                    <ListItem disablePadding>
                      <ListItemButton>
                        <ListItemText primary='Terms & Conditions' />
                      </ListItemButton>
                    </ListItem>
                    <ListItem disablePadding>
                      <ListItemButton>
                        <ListItemText primary='Privacy Policy' />
                      </ListItemButton>
                    </ListItem>
                  </List>
                </Grid>
                <Grid item xs={6}>
                  <List>
                    <ListItem disablePadding>
                      <ListItemButton>
                        <ListItemText primary='FAQ' />
                      </ListItemButton>
                    </ListItem>
                    <ListItem disablePadding>
                      <ListItemButton>
                        <ListItemText primary='Support' />
                      </ListItemButton>
                    </ListItem>
                    <ListItem disablePadding>
                      <ListItemButton>
                        <ListItemText primary='Cookie Policy' />
                      </ListItemButton>
                    </ListItem>
                    <ListItem disablePadding>
                      <ListItemButton>
                        <ListItemText primary='Affiliate Program' />
                      </ListItemButton>
                    </ListItem>
                  </List>
                </Grid>
              </Grid>
            </Box>
          </Grid>
          <Grid item lg={3} md={6} sm={12} xs={12}>
            <Box className='footer-link-box'>
              <Grid container columnSpacing={1}>
                <Grid item xs={6}>
                  <Typography variant='h6'>Offers</Typography>
                  <List>
                    <ListItem disablePadding>
                      <ListItemButton>
                        <ListItemText primary='Rewards & Bonuses' />
                      </ListItemButton>
                    </ListItem>
                    <ListItem disablePadding>
                      <ListItemButton>
                        <ListItemText primary='Promotions' />
                      </ListItemButton>
                    </ListItem>
                  </List>
                </Grid>
                <Grid item xs={6}>
                  <Typography variant='h6'>Games</Typography>
                  <List>
                    <ListItem disablePadding>
                      <ListItemButton>
                        <ListItemText primary='Soccer' />
                      </ListItemButton>
                    </ListItem>
                    <ListItem disablePadding>
                      <ListItemButton>
                        <ListItemText primary='Basketball' />
                      </ListItemButton>
                    </ListItem>
                    <ListItem disablePadding>
                      <ListItemButton>
                        <ListItemText primary='Golf' />
                      </ListItemButton>
                    </ListItem>
                    <ListItem disablePadding>
                      <ListItemButton>
                        <ListItemText primary='Baseball' />
                      </ListItemButton>
                    </ListItem>
                    <ListItem disablePadding>
                      <ListItemButton>
                        <ListItemText primary='Iceball' />
                      </ListItemButton>
                    </ListItem>
                    <ListItem disablePadding>
                      <ListItemButton>
                        <ListItemText primary='Cricket' />
                      </ListItemButton>
                    </ListItem>
                  </List>
                </Grid>
              </Grid>
            </Box>
          </Grid>
          <Grid item lg={2} md={6} sm={12} xs={12}>
            <Box className='footer-link-box social-link-box'>
              <Typography variant='h6'>Social Media</Typography>
              <MenuList>
                <MenuItem>
                  <ListItemIcon>
                    <img src={'assets/images/svg/telegram.svg'} alt='Telegram' />
                  </ListItemIcon>
                </MenuItem>
                <MenuItem>
                  <ListItemIcon>
                    <img src={'assets/images/svg/twitter.svg'} alt='Twitter' />
                  </ListItemIcon>
                </MenuItem>
                <MenuItem>
                  <ListItemIcon>
                    <img src={'assets/images/svg/whatsapp.svg'} alt='Whatsapp' />
                  </ListItemIcon>
                </MenuItem>
                <MenuItem>
                  <ListItemIcon>
                    <img src={'assets/images/svg/pinterest.svg'} alt='Pinterest' />
                  </ListItemIcon>
                </MenuItem>
                <MenuItem>
                  <ListItemIcon>
                    <img src={'assets/images/svg/instagram.svg'} alt='Instagram' />
                  </ListItemIcon>
                </MenuItem>
                <MenuItem>
                  <ListItemIcon>
                    <img src={'assets/images/svg/youtube.svg'} alt='Youtube' />
                  </ListItemIcon>
                </MenuItem>
                <MenuItem>
                  <ListItemIcon>
                    <img src={'assets/images/svg/discord.svg'} alt='Discord' />
                  </ListItemIcon>
                </MenuItem>
              </MenuList>
            </Box>
          </Grid>
        </Grid>
      </Box>
      <Box className='copyright-container'>
        <Typography variant='body2'>
          Copyright © 2019-2022 SPORTSBET. All rights reserved.
        </Typography>
      </Box>
    </FooterWrapper>
  )
}
