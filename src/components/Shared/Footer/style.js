import { Box, styled } from '@mui/material';

export const FooterWrapper = styled(Box)(({ theme }) => {
    return {
        backgroundColor: theme.palette.primary.main,
        marginLeft: theme.spacing(9.5),
        '& .footer-container': {
            backgroundColor: theme.palette.primary.light,
            padding: theme.spacing(2.5, 5),
            '& .logo-wrap': {
                padding: theme.spacing(2.5, 12, 2.5, 0),
                '& .logo-box': {
                    maxWidth: theme.spacing('100%'),
                },
                '& .MuiTypography-body1': {
                    color: theme.palette.primary.contrastTextLight,
                    fontSize: theme.spacing('0.875rem'),
                    fontWeight: theme.typography.fontWeightRegular,
                    marginTop: theme.spacing(1.25),
                },
                [theme.breakpoints.down('md')]: {
                    padding: theme.spacing(2.5, 0, 2.5, 0),
                },
            },
            '& .footer-link-box': {
                padding: theme.spacing(2.5, 0),
                '& .MuiTypography-h6': {
                    color: theme.palette.primary.contrastText,
                    fontSize: theme.spacing('1.5rem'),
                    fontWeight: theme.typography.fontWeightSemiBold,
                    marginBottom: theme.spacing(2),
                },
                '& .MuiList-root': {
                    padding: theme.spacing(0),
                    '& .MuiListItem-root': {
                        marginBottom: theme.spacing(1),
                        '& .MuiButtonBase-root': {
                            padding: theme.spacing(0),
                            '& .MuiTypography-root': {
                                color: theme.palette.primary.contrastTextLight,
                                fontSize: theme.spacing('0.875rem'),
                                fontWeight: theme.typography.fontWeightRegular,
                                transition: '0.3s all',
                            },
                            '&:hover': {
                                backgroundColor: theme.palette.primary.transparent,
                                '& .MuiTypography-root': {
                                    color: theme.palette.primary.contrastTextActive,
                                    transition: '0.3s all',
                                },
                            },
                        },
                    },
                },
                '&.social-link-box': {
                    '& .MuiList-root': {
                        padding: theme.spacing(0),
                        marginBottom: theme.spacing(0),
                        '& .MuiButtonBase-root': {
                            display: 'inline-block',
                            padding: theme.spacing(0),
                            marginRight: theme.spacing(1),
                            '& .MuiListItemIcon-root': {
                                minWidth: 'unset',
                                '& img': {
                                    width: theme.spacing(2.5),
                                    height: theme.spacing(2.5),
                                }
                            },
                            '&:hover': {
                                backgroundColor: theme.palette.primary.transparent,
                            },
                        },
                    },
                },
            },
            [theme.breakpoints.up('xl')]: {
                padding: theme.spacing(4.25, 15.5),
            },
            [theme.breakpoints.down('sm')]: {
                padding: theme.spacing(2, 2, 0),
            },
        },
        '& .copyright-container': {
            backgroundColor: theme.palette.primary.main,
            padding: theme.spacing(2),
            '& .MuiTypography-root': {
                color: theme.palette.primary.contrastText,
                fontSize: theme.spacing('0.875rem'),
                fontWeight: theme.typography.fontWeightRegular,
                textAlign: 'center',
                margin: theme.spacing(0),
                [theme.breakpoints.down('sm')]: {
                    fontSize: theme.spacing('0.75rem'),
                },
            },
        },
        [theme.breakpoints.down('sm')]: {
            marginLeft: theme.spacing(0),
        },
    };
});