import React from 'react'
import { TableWrapper } from '../Common/style';
import { MatchResultWrapper } from './style';
import { useTheme } from '@emotion/react';
import { Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@mui/material';

export default function MatchResult() {
  const theme = useTheme();

  return (
    <MatchResultWrapper theme={theme}>
      <TableWrapper theme={theme} className='table-wrap'>
        <TableContainer component={Paper}>
          <Table aria-label='simple table'>
            <TableHead>
              <TableRow>
                <TableCell>Chelsea <span className='vs-text'>vs</span> Fulham</TableCell>
                <TableCell className='secondary-heading'>Full Time</TableCell>
                <TableCell className='secondary-heading'>Half Time</TableCell>
                <TableCell className='secondary-heading'>2nd Half</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {Array(10).fill('').map((item)=>{
                return (    
                  <TableRow>
                    <TableCell>1 x 2</TableCell>
                    <TableCell>0 - 1</TableCell>
                    <TableCell>0 - 1</TableCell>
                    <TableCell>0 - 1</TableCell>
                  </TableRow>
                )
              })}
            </TableBody>
          </Table>
        </TableContainer>
      </TableWrapper>
    </MatchResultWrapper>
  )
}
