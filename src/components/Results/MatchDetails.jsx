import { useTheme } from '@emotion/react';
import { MatchDetailsWrapper } from './style';
import React from 'react'
import { Accordion, AccordionDetails, AccordionSummary, Box, Link, Typography } from '@mui/material';
import ArrowDropDownRoundedIcon from '@mui/icons-material/ArrowDropDownRounded';

export default function MatchDetails() {
  const theme = useTheme();

  const [expanded, setExpanded] = React.useState('panel1');

  const handleChange = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : true);
  };

  return (
    <MatchDetailsWrapper theme={theme}>
      <Accordion expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
        <AccordionSummary
          expandIcon={<ArrowDropDownRoundedIcon />}
          aria-controls='panel1bh-content'
          id='panel1bh-header'
        >
          <Typography variant='h5'>England / Premier League</Typography>
        </AccordionSummary>
        <AccordionDetails>
          {Array(5).fill('').map((item)=>{
            return (
              <Link href='JavaScript:void(0);' className='match-details-wrap'>
                <Typography variant='subtitle1'>Feb 4, 1:30 AM – in 8 hours</Typography>
                <Box className='score-box'>
                  <Typography variant='body1'>
                    <span>Chelsea</span>
                    <span>0</span>
                  </Typography>
                  <Typography variant='body1'>
                    <span>Fulham</span>
                    <span>0</span>
                  </Typography>
                </Box>
              </Link>
            )
          })}
        </AccordionDetails>
      </Accordion>
    </MatchDetailsWrapper>
  )
}
