import { Box, styled } from '@mui/material';

export const ResultsWrapper = styled(Box)(({ theme }) => {
    return {
        '& .GamesMenu-column': {
            width: theme.spacing('100%'),
            [theme.breakpoints.up('md')]: {
                maxWidth: theme.spacing(37.25),
                minWidth: theme.spacing(37.25),
            },
        },
        '& .Results-column': {
            width: theme.spacing('100%'),
            [theme.breakpoints.up('md')]: {
                width: theme.spacing('calc(100% - 298px)'),
            },
        },
        '& .Match-Details-column': {
            width: theme.spacing('100%'),
            [theme.breakpoints.up('xm')]: {
                maxWidth: theme.spacing(45.75),
                minWidth: theme.spacing(45.75),
            },
        },
        '& .Match-Result-column': {
            width: theme.spacing('100%'),
            [theme.breakpoints.up('xm')]: {
                width: theme.spacing('calc(100% - 366px)'),
            },
        },
    };
});

export const MatchResultWrapper = styled(Box)(({ theme }) => {
    return {
        marginTop: theme.spacing(2),
    };
});

export const MatchDetailsWrapper = styled(Box)(({ theme }) => {
    return {
        marginTop: theme.spacing(2),
        '& .MuiAccordion-root': {
            backgroundColor: theme.palette.primary.transparent,
            borderRadius: theme.spacing(0),
            boxShadow: theme.shadows[0],
            marginBottom: theme.spacing(2),
            '&::before': {
                display: 'none',
            },
            '&.Mui-expanded': {
                marginBottom: theme.spacing(2),
            },
            '& .MuiAccordionSummary-root': {
                backgroundColor: theme.palette.primary.light,
                borderRadius: theme.spacing(1.25, 1.25, 0, 0),
                padding: theme.spacing(0.1675, 2),
                '&.Mui-expanded': {
                    minHeight: theme.spacing(6),
                },
                '& .MuiAccordionSummary-content': {
                    '& .MuiTypography-h5': {
                        color: theme.palette.primary.contrastText,
                        fontSize: theme.spacing('1rem'),
                        fontWeight: theme.typography.fontWeightMedium,
                    },
                    '&.Mui-expanded': {
                        margin: theme.spacing(1.5, 0),
                    },
                },
            },
            '& .MuiAccordionSummary-expandIconWrapper': {
                '& .MuiSvgIcon-root': {
                    fill: theme.palette.primary.contrastText,
                    transform: 'scale(1.5)',
                },
            },
            '& .MuiCollapse-root': {
                '& .MuiAccordionDetails-root': {
                    maxHeight: theme.spacing('80vh'),
                    overflowY: 'auto',
                    padding: theme.spacing(0),
                    '& .match-details-wrap': {
                        minWidth: theme.spacing('100%'),
                        backgroundColor: theme.palette.secondary.dark,
                        borderWidth: theme.spacing(0, 0, 0.125, 0),
                        borderStyle: 'solid',
                        borderColor: theme.palette.border.dark,
                        display: 'block',
                        padding: theme.spacing(1.5, 2),
                        transition: '0.3s all',
                        '& .MuiTypography-subtitle1': {
                            color: theme.palette.primary.contrastTextLight,
                            fontSize: theme.spacing('0.75rem'),
                            fontWeight: theme.typography.fontWeightRegular,
                        },
                        '& .score-box': {
                            marginTop: theme.spacing(1.5),
                            '& .MuiTypography-body1': {
                                color: theme.palette.primary.contrastText,
                                fontSize: theme.spacing('0.875rem'),
                                fontWeight: theme.typography.fontWeightMedium,
                                display: 'flex',
                                justifyContent: 'space-between',
                                marginBottom: theme.spacing(1),
                                '&:last-child': {
                                    marginBottom: theme.spacing(0),
                                },
                            },
                        },
                        '&:hover, &.active': {
                            backgroundColor: theme.palette.secondary.main,
                            transition: '0.3s all',
                        },
                    },
                },
            },
        },
    };
});