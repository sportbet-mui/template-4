import React from 'react'
import { ResultsWrapper } from './style';
import { useTheme } from '@emotion/react'
import { Grid } from '@mui/material';
import BannerSlider from '../BannerSlider/BannerSlider';
import GameNavbar from '../GameNavbar';
import GamesMenu from '../GamesMenu/GamesMenu';
import MatchResult from './MatchResult';
import MatchDetails from './MatchDetails';

export default function Results() {
  const theme = useTheme();

  return (
    <ResultsWrapper theme={theme}>
      <BannerSlider/>
      <Grid container spacing={2}>
        <Grid item className='GamesMenu-column'>
          <GamesMenu/>
        </Grid>
        <Grid item className='Results-column'>
          <GameNavbar/>
          <Grid container spacing={2}>
            <Grid item className='Match-Details-column'>
              <MatchDetails/>
            </Grid>
            <Grid item className='Match-Result-column'>
              <MatchResult/>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </ResultsWrapper>
  )
}
