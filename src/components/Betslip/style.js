import { Box, styled } from '@mui/material';

export const BetslipWrapper = styled(Box)(({ theme }) => {
    return {
        '& .MuiCard-root': {
            backgroundColor: theme.palette.primary.dark,
            borderRadius: theme.spacing(1.25),
            boxShadow: theme.shadows[0],
            '& .MuiCardHeader-root': {
                backgroundColor: theme.palette.primary.light,
                padding: theme.spacing(2),
                '& .MuiTypography-h5': {
                    color: theme.palette.primary.contrastText,
                    fontSize: theme.spacing('1rem'),
                    fontWeight: theme.typography.fontWeightMedium,
                    textAlign: 'center',
                },
            },
            '& .MuiCardContent-root': {
                padding: theme.spacing(2, 0),
                '& .MuiTabs-root': {
                    minHeight: theme.spacing('unset'),
                    '& .MuiButtonBase-root': {
                        minWidth: theme.spacing(7.5),
                        minHeight: theme.spacing(3.75),
                        backgroundColor: theme.palette.secondary.dark,
                        color: theme.palette.primary.contrastTextLight,
                        fontSize: theme.spacing('0.75rem'),
                        fontWeight: theme.typography.fontWeightRegular,
                        padding: theme.spacing(1.5, 1.75),
                        '&.Mui-selected': {
                            backgroundColor: theme.palette.secondary.contrastTextActive,
                            color: theme.palette.primary.contrastText,
                        },
                        '&.MuiTabScrollButton-root': {
                            width: theme.spacing(2.5),
                            minWidth: theme.spacing('unset'),
                            backgroundColor: theme.palette.button.transparent,
                            color: theme.palette.primary.contrastTextActive,
                            opacity: '1',
                            padding: theme.spacing(0),
                        },
                    },
                    '& .MuiTabs-indicator': {
                        display: 'none',
                    },
                },
                '& .tab-panel': {
                    padding: theme.spacing(2),
                    '& .MuiButton-text': {
                        height: theme.spacing('unset'),
                        color: theme.palette.primary.contrastTextLight,
                        fontSize: theme.spacing('0.75rem'),
                        fontWeight: theme.typography.fontWeightRegular,
                        padding: theme.spacing(0),
                    },
                    '& .betslip-card': {
                        borderRadius: theme.spacing(0),
                        marginTop: theme.spacing(1),
                        '& .MuiCardHeader-root': {
                            backgroundColor: theme.palette.secondary.dark,
                            borderWidth: theme.spacing(0),
                            borderBottomWidth: theme.spacing(0.125),
                            borderStyle: 'solid',
                            borderColor: theme.palette.border.dark,
                            padding: theme.spacing(1),
                            '& .MuiTypography-h6': {
                                color: theme.palette.primary.contrastText,
                                fontSize: theme.spacing('0.75rem'),
                                fontWeight: theme.typography.fontWeightRegular,
                                '& span': {
                                    color: theme.palette.warning.main,
                                },
                            },
                            '& .MuiIconButton-root': {
                                color: theme.palette.primary.contrastTextLight,
                                padding: theme.spacing(0.5),
                                '& .MuiSvgIcon-root': {
                                    width: theme.spacing('0.75em'),
                                    height: theme.spacing('0.75em'),
                                },
                            },
                        },
                        '& .MuiCardContent-root': {
                            backgroundColor: theme.palette.secondary.dark,
                            padding: theme.spacing(1),
                            '& .MuiTypography-subtitle1': {
                                color: theme.palette.primary.contrastTextLight,
                                fontSize: theme.spacing('0.75rem'),
                                fontWeight: theme.typography.fontWeightRegular,
                            },
                            '& .text-box': {
                                width: theme.spacing('100%'),
                                display: 'flex',
                                justifyContent: 'space-between',
                                margin: theme.spacing(0.5, 0, 0, 0),
                                '& .MuiTypography-body1': {
                                    color: theme.palette.primary.contrastText,
                                    fontSize: theme.spacing('0.875rem'),
                                    fontWeight: theme.typography.fontWeightMedium,
                                },
                                '& .MuiTypography-body2': {
                                    color: theme.palette.warning.main,
                                    fontSize: theme.spacing('0.875rem'),
                                    fontWeight: theme.typography.fontWeightMedium,
                                    display: 'flex',
                                    alignItems: 'center',
                                    marginLeft: theme.spacing(2),
                                    '& svg': {
                                        fill: theme.palette.warning.main,
                                        marginRight: theme.spacing(1),
                                    },
                                },
                            },
                        },
                    },
                },
            },
            '& .MuiCardActions-root': {
                backgroundColor: theme.palette.primary.light,
                flexDirection: 'column',
                padding: theme.spacing(2),
                '& .input-holder': {
                    marginBottom: theme.spacing(2),
                    position: 'relative',
                    '& .MuiFormControl-root': {
                        '& .MuiInputBase-root': {
                            borderRadius: theme.spacing(1.25),
                            '& .MuiInputBase-input': {
                                color: theme.palette.primary.contrastText,
                                fontSize: theme.spacing('0.875rem'),
                                fontWeight: theme.typography.fontWeightMedium,
                                textAlign: 'right',
                                padding: theme.spacing(1.0625, 1.75, 1.0625, 4),
                            },
                            '& .MuiOutlinedInput-notchedOutline': {
                                borderColor: theme.palette.border.main,
                            },
                        },
                    },
                    '& .icon-box': {
                        width: theme.spacing(4),
                        height: theme.spacing('100%'),
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        position: 'absolute',
                        top: theme.spacing('50%'),
                        transform: theme.spacing('translateY(-50%)'),
                        '& svg': {
                            fill: theme.palette.warning.main,
                        },
                    },
                },
                '& .total-text-box': {
                    width: theme.spacing('100%'),
                    display: 'flex',
                    justifyContent: 'space-between',
                    margin: theme.spacing(0, 0, 1, 0),
                    '& .MuiTypography-body1': {
                        color: theme.palette.primary.contrastText,
                        fontSize: theme.spacing('0.875rem'),
                        fontWeight: theme.typography.fontWeightMedium,
                    },
                    '& .MuiTypography-body2': {
                        color: theme.palette.warning.main,
                        fontSize: theme.spacing('0.875rem'),
                        fontWeight: theme.typography.fontWeightMedium,
                        display: 'flex',
                        alignItems: 'center',
                        marginLeft: theme.spacing(2),
                        '& svg': {
                            fill: theme.palette.warning.main,
                            marginRight: theme.spacing(1),
                        },
                    },
                },
                '& .MuiButtonBase-root': {
                    marginTop: theme.spacing(2),
                },
            },
        },
    };
});