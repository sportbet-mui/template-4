import React from 'react'
import { BetslipWrapper } from './style'
import { CustomMainButton, CustomTextButton } from '../Common/StyledButton/CustomButton';
import { Box, Card, CardActions, CardContent, CardHeader, IconButton, Tab, Tabs, TextField, Typography, useTheme } from '@mui/material';
import CloseRoundedIcon from '@mui/icons-material/CloseRounded';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role='tabpanel'
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box className='tab-panel'>
          {children}
        </Box>
      )}
    </div>
  );
}

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

export default function Betslip() {
  const theme = useTheme();

  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  
  return (
    <BetslipWrapper theme={theme}>
      <Card>
        <CardHeader title='Betslip' />

        <CardContent>
          <Tabs variant='fullWidth' centered value={value} onChange={handleChange} aria-label='basic tabs example'>
            <Tab label='Single' {...a11yProps(0)} />
            <Tab label='Multiple' {...a11yProps(1)} />
            <Tab label='System' {...a11yProps(2)} />
          </Tabs>

          <TabPanel value={value} index={0}>
            <CustomTextButton>Remove All</CustomTextButton>
            {Array(3).fill('').map((item)=>{
              return (
                <Card className='betslip-card'>
                  <CardHeader
                    title={
                      <Typography variant='h6'>
                        Chelsea <span>vs</span> Fulham
                      </Typography>
                    }
                    action={
                      <IconButton size='small'>
                        <CloseRoundedIcon />
                      </IconButton>
                    }
                  />
                  <CardContent>
                    <Typography variant='subtitle1'>1x2</Typography>
                    <Box className='text-box'>
                      <Typography variant='body1'>Fulham</Typography>
                      <Typography variant='body2'>1.67</Typography>
                    </Box>
                  </CardContent>
                </Card>
              )
            })}
          </TabPanel>

          <TabPanel value={value} index={1}>
            <CustomTextButton>Remove All</CustomTextButton>
            {Array(3).fill('').map((item)=>{
              return (
                <Card className='betslip-card'>
                  <CardHeader
                    title={
                      <Typography variant='h6'>
                        Chelsea <span>vs</span> Fulham
                      </Typography>
                    }
                    action={
                      <IconButton size='small'>
                        <CloseRoundedIcon />
                      </IconButton>
                    }
                  />
                  <CardContent>
                    <Typography variant='subtitle1'>1x2</Typography>
                    <Box className='text-box'>
                      <Typography variant='body1'>Fulham</Typography>
                      <Typography variant='body2'>1.67</Typography>
                    </Box>
                  </CardContent>
                </Card>
              )
            })}
          </TabPanel>

          <TabPanel value={value} index={2}>
            <CustomTextButton>Remove All</CustomTextButton>
            {Array(3).fill('').map((item)=>{
              return (
                <Card className='betslip-card'>
                  <CardHeader
                    title={
                      <Typography variant='h6'>
                        Chelsea <span>vs</span> Fulham
                      </Typography>
                    }
                    action={
                      <IconButton size='small'>
                        <CloseRoundedIcon />
                      </IconButton>
                    }
                  />
                  <CardContent>
                    <Typography variant='subtitle1'>1x2</Typography>
                    <Box className='text-box'>
                      <Typography variant='body1'>Fulham</Typography>
                      <Typography variant='body2'>1.67</Typography>
                    </Box>
                  </CardContent>
                </Card>
              )
            })}
          </TabPanel>
        </CardContent>

        <CardActions>
          <Box className='input-holder'>
            <TextField defaultValue='0.00' />
            <Box className='icon-box'>
              <svg x='0px' y='0px' width='11.622px' height='14.828px' viewBox='0.189 0.586 11.622 14.828' enable-background='new 0.189 0.586 11.622 14.828'>
                <g>
                  <path d='M5.999,0.586c-3.209,0-5.811,1.18-5.811,2.636v0.571c0,1.456,2.602,2.636,5.811,2.636c3.21,0,5.812-1.18,5.812-2.636V3.222C11.811,1.766,9.209,0.586,5.999,0.586z'/>
                  <path d='M5.999,7.72c-2.801,0-5.139-0.899-5.688-2.096c-0.08,0.174-0.122,0.354-0.122,0.54v0.571c0,1.456,2.602,2.636,5.811,2.636c3.21,0,5.812-1.18,5.812-2.636V6.164c0-0.186-0.043-0.366-0.123-0.54C11.139,6.821,8.801,7.72,5.999,7.72z'/>
                  <path d='M5.999,10.662c-2.801,0-5.139-0.898-5.688-2.096C0.23,8.74,0.188,8.922,0.188,9.106v0.571c0,1.455,2.602,2.636,5.811,2.636c3.21,0,5.812-1.181,5.812-2.636V9.106c0-0.185-0.043-0.366-0.123-0.54C11.139,9.764,8.801,10.662,5.999,10.662z'/>
                  <path d='M5.999,13.764c-2.801,0-5.139-0.899-5.688-2.096c-0.08,0.174-0.122,0.354-0.122,0.539v0.572c0,1.455,2.602,2.635,5.811,2.635c3.21,0,5.812-1.18,5.812-2.635v-0.572c0-0.185-0.043-0.365-0.123-0.539C11.139,12.864,8.801,13.764,5.999,13.764z'/>
                </g>
              </svg>
            </Box>
          </Box>

          <Box className='total-text-box'>
            <Typography variant='body1'>Total Odds</Typography>
            <Typography variant='body2'>12.83</Typography>
          </Box>

          <Box className='total-text-box'>
            <Typography variant='body1'>Total Payout</Typography>
            <Typography variant='body2'>
              <svg x='0px' y='0px' width='11.622px' height='14.828px' viewBox='0.189 0.586 11.622 14.828' enable-background='new 0.189 0.586 11.622 14.828'>
                <g>
                  <path d='M5.999,0.586c-3.209,0-5.811,1.18-5.811,2.636v0.571c0,1.456,2.602,2.636,5.811,2.636c3.21,0,5.812-1.18,5.812-2.636V3.222C11.811,1.766,9.209,0.586,5.999,0.586z'/>
                  <path d='M5.999,7.72c-2.801,0-5.139-0.899-5.688-2.096c-0.08,0.174-0.122,0.354-0.122,0.54v0.571c0,1.456,2.602,2.636,5.811,2.636c3.21,0,5.812-1.18,5.812-2.636V6.164c0-0.186-0.043-0.366-0.123-0.54C11.139,6.821,8.801,7.72,5.999,7.72z'/>
                  <path d='M5.999,10.662c-2.801,0-5.139-0.898-5.688-2.096C0.23,8.74,0.188,8.922,0.188,9.106v0.571c0,1.455,2.602,2.636,5.811,2.636c3.21,0,5.812-1.181,5.812-2.636V9.106c0-0.185-0.043-0.366-0.123-0.54C11.139,9.764,8.801,10.662,5.999,10.662z'/>
                  <path d='M5.999,13.764c-2.801,0-5.139-0.899-5.688-2.096c-0.08,0.174-0.122,0.354-0.122,0.539v0.572c0,1.455,2.602,2.635,5.811,2.635c3.21,0,5.812-1.18,5.812-2.635v-0.572c0-0.185-0.043-0.365-0.123-0.539C11.139,12.864,8.801,13.764,5.999,13.764z'/>
                </g>
              </svg>
              0.00
            </Typography>
          </Box>

          <CustomMainButton fullWidth>Place Bet</CustomMainButton>
        </CardActions>
      </Card>
    </BetslipWrapper>
  )
}
