import React, { useState } from 'react'
import { MyBetsWrapper } from './style';
import { useTheme } from '@emotion/react'
import BetDetails from './BetDetails';
import { CustomMainButton, CustomPrimaryButton, CustomTextButton } from '../Common/StyledButton/CustomButton';
import { Chip, Container, Grid, MenuItem, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from '@mui/material';
import { HeadingWrapper, StyledHeadingTypography } from '../Common/StyledTypography/style';
import { FilterWrapper, TableWrapper } from '../Common/style';
import { CustomDatePicker, CustomInputLabel, CustomSelectField } from '../Common/StyledForm/style';
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';

export default function MyBets() {
  const theme = useTheme();

  const [selectTypeStatus,setTypeStatus]=useState(1);

  const [selectTypeType,setTypeType]=useState(1);

  const [betDetailsDialog,setBetDetailsDialog]=useState(false);

  return (
    <>
      <MyBetsWrapper theme={theme}>
        <HeadingWrapper>
          <StyledHeadingTypography>My Bets</StyledHeadingTypography>
        </HeadingWrapper>
        <Typography  variant='body2'>Here you can find your detailed history of bets across the various sections of our website!</Typography>
        <Container maxWidth='lg'>
          <FilterWrapper theme={theme}>
            <Grid container spacing={2}>
              <Grid item className='filter-content-box'>
                <CustomInputLabel>Status</CustomInputLabel>
                <CustomSelectField value={selectTypeStatus} onChange={setTypeStatus}>
                  <MenuItem value={selectTypeStatus}>Status 1</MenuItem>
                  <MenuItem value={selectTypeStatus}>Status 2</MenuItem>
                  <MenuItem value={selectTypeStatus}>Status 3</MenuItem>
                </CustomSelectField>
              </Grid>
              <Grid item className='filter-content-box'>
                <CustomInputLabel>Type</CustomInputLabel>
                <CustomSelectField value={selectTypeType} onChange={setTypeType}>
                  <MenuItem value={selectTypeType}>Type 1</MenuItem>
                  <MenuItem value={selectTypeType}>Type 2</MenuItem>
                  <MenuItem value={selectTypeType}>Type 3</MenuItem>
                </CustomSelectField>
              </Grid>
              <Grid item className='filter-content-box'>
                <CustomInputLabel>Date</CustomInputLabel>
                <CustomDatePicker>
                  <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DemoContainer components={['DatePicker']}>
                      <DatePicker/>
                    </DemoContainer>
                  </LocalizationProvider>
                </CustomDatePicker>
              </Grid>
              <Grid item className='filter-content-box btn-box'>
                <Grid container spacing={2}>
                  <Grid item xs={6}>
                    <CustomPrimaryButton fullWidth>Reset</CustomPrimaryButton>
                  </Grid>
                  <Grid item xs={6}>
                    <CustomMainButton fullWidth>Show</CustomMainButton>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </FilterWrapper>
          <TableWrapper theme={theme} className='table-wrap'>
            <TableContainer component={Paper}>
              <Table aria-label='simple table'>
                <TableHead>
                  <TableRow>
                    <TableCell>Date</TableCell>
                    <TableCell>Bet Id</TableCell>
                    <TableCell>Type</TableCell>
                    <TableCell>Placed Odds</TableCell>
                    <TableCell>Amount</TableCell>
                    <TableCell>Winning</TableCell>
                    <TableCell>Status</TableCell>
                    <TableCell>&nbsp;</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {Array(5).fill('').map((item)=>{
                    return (
                      <>
                        <TableRow>
                          <TableCell>16/02/2023 <br/> 16:28</TableCell>
                          <TableCell>125</TableCell>
                          <TableCell>Single</TableCell>
                          <TableCell>2</TableCell>
                          <TableCell>$100</TableCell>
                          <TableCell>$200</TableCell>
                          <TableCell>
                            <Chip label='Win' className='status-win' />
                          </TableCell>
                          <TableCell>
                            <CustomTextButton onClick={()=>{setBetDetailsDialog(true)}}>View Details</CustomTextButton>
                          </TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>16/02/2023 <br/> 16:28</TableCell>
                          <TableCell>125</TableCell>
                          <TableCell>Single</TableCell>
                          <TableCell>2</TableCell>
                          <TableCell>$100</TableCell>
                          <TableCell>$200</TableCell>
                          <TableCell>
                            <Chip label='Lose' className='status-lose' />
                          </TableCell>
                          <TableCell>
                            <CustomTextButton onClick={()=>{setBetDetailsDialog(true)}}>View Details</CustomTextButton>
                          </TableCell>
                        </TableRow>
                      </>
                    )
                  })}
                </TableBody>
              </Table>
            </TableContainer>
          </TableWrapper>
        </Container>
      </MyBetsWrapper>
      <BetDetails dialogOpen={betDetailsDialog} handleClose={()=>setBetDetailsDialog()} />
    </>
  )
}
