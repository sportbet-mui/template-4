import React from 'react'
import { BetDetailsWrapper } from './style';
import { useTheme } from '@emotion/react'
import CustomDialogBox from '../Common/StyledDialogBox/CustomDialogBox';
import { Box, Chip, Grid, Typography, useMediaQuery } from '@mui/material';
import { CustomInputLabel, CustomTextField } from '../Common/StyledForm/style';

export default function BetDetails({ dialogOpen, handleClose }) {
  const theme = useTheme();

  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const [maxWidth] = React.useState('md');

  return (
    <>
      <CustomDialogBox maxWidth={maxWidth} fullScreen={fullScreen} dialogOpen={dialogOpen} handleDialogClose={handleClose} aria-labelledby='customized-dialog-title'>
        <BetDetailsWrapper theme={theme}>
          <Box className='title-wrap'>
            <Typography variant='h1'>Bet Details</Typography>
            <Chip label='Win' className='status-win' />
          </Box>
          <Box className='bet-details-wrap'>
            <Grid container spacing={2}>
              <Grid item sm={6} xs={12}>
                <Grid container spacing={2}>
                  <Grid item xs={4}>
                    <Typography variant='subtitle1'>Bet Details</Typography>
                  </Grid>
                  <Grid item xs={2}>
                    <Typography variant='subtitle1'>:</Typography>
                  </Grid>
                  <Grid item xs={6}>
                    <Typography variant='subtitle2'>125</Typography>
                  </Grid>
                  <Grid item xs={4}>
                    <Typography variant='subtitle1'>Date</Typography>
                  </Grid>
                  <Grid item xs={2}>
                    <Typography variant='subtitle1'>:</Typography>
                  </Grid>
                  <Grid item xs={6}>
                    <Typography variant='subtitle2'>16/02/2023 (16:30)</Typography>
                  </Grid>
                  <Grid item xs={4}>
                    <Typography variant='subtitle1'>Bet Type</Typography>
                  </Grid>
                  <Grid item xs={2}>
                    <Typography variant='subtitle1'>:</Typography>
                  </Grid>
                  <Grid item xs={6}>
                    <Typography variant='subtitle2'>Bet Details</Typography>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item sm={6} xs={12}>
                <Grid container spacing={2}>
                  <Grid item xs={4}>
                    <Typography variant='subtitle1'>Stack</Typography>
                  </Grid>
                  <Grid item xs={2}>
                    <Typography variant='subtitle1'>:</Typography>
                  </Grid>
                  <Grid item xs={6}>
                    <Typography variant='subtitle2'>$100</Typography>
                  </Grid>
                  <Grid item xs={4}>
                    <Typography variant='subtitle1'>Return</Typography>
                  </Grid>
                  <Grid item xs={2}>
                    <Typography variant='subtitle1'>:</Typography>
                  </Grid>
                  <Grid item xs={6}>
                    <Typography variant='subtitle2'>$200</Typography>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Box>
          <Typography variant='h2'>Odd Details</Typography>
          <Box className='odd-details-wrap'>
            <Grid container spacing={2}>
              <Grid item sm={6} xs={12}>
                <Typography variant='h6'>England Premiere League</Typography>
                <Typography variant='h5'>Chelsea vs Fulham</Typography>
                <Typography variant='h4'>1x2</Typography>
              </Grid>
              <Grid item sm={6} xs={12}>
                <Grid container spacing={2}>
                  <Grid item xs={6}>
                    <CustomInputLabel>Selected Outcome</CustomInputLabel>
                    <CustomTextField value={1} placeholder='Outcome' />
                  </Grid>
                  <Grid item xs={6}>
                    <CustomInputLabel>Odds</CustomInputLabel>
                    <CustomTextField value={2} placeholder='Odds' />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Box>
        </BetDetailsWrapper>
      </CustomDialogBox>
    </>
  )
}
