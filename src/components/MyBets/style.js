import { Box, styled } from '@mui/material';

export const MyBetsWrapper = styled(Box)(({ theme }) => {
    return {
        '& .MuiTypography-body2': {
            color: theme.palette.primary.contrastTextLight,
            fontSize: theme.spacing('0.875rem'),
            fontWeight: theme.typography.fontWeightRegular,
            textAlign: 'center',
            marginBottom: theme.spacing(5),
        },
    };
});

export const BetDetailsWrapper = styled(Box)(({ theme }) => {
    return {
        padding: theme.spacing(5),
        '& .title-wrap': {
            display: 'flex',
            marginBottom: theme.spacing(3),
            '& .MuiTypography-h1': {
                color: theme.palette.primary.contrastText,
                fontSize: theme.spacing('1.5rem'),
                fontWeight: theme.typography.fontWeightMedium,
            },
            '& .MuiChip-root': {
                minWidth: theme.spacing(9),
                borderRadius: theme.spacing(0.625),
                color: theme.palette.primary.contrastText,
                fontSize: theme.spacing('0.875rem'),
                marginLeft: theme.spacing(5),
                '&.status-win': {
                    backgroundColor: theme.palette.success.main,
                },
                '&.status-lose': {
                    backgroundColor: theme.palette.danger.main,
                },
            },
        },
        '& .MuiTypography-h2': {
            color: theme.palette.primary.contrastText,
            fontSize: theme.spacing('1rem'),
            fontWeight: theme.typography.fontWeightMedium,
            margin: theme.spacing(5, 0, 2),
        },
        '& .bet-details-wrap, & .odd-details-wrap': {
            backgroundColor: theme.palette.primary.dark,
            borderRadius: theme.spacing(1.25),
            padding: theme.spacing(3),
            '& .MuiTypography-subtitle1': {
                color: theme.palette.primary.contrastTextLight,
                fontSize: theme.spacing('0.875rem'),
                fontWeight: theme.typography.fontWeightRegular,
            },
            '& .MuiTypography-subtitle2': {
                color: theme.palette.primary.contrastText,
                fontSize: theme.spacing('0.875rem'),
                fontWeight: theme.typography.fontWeightRegular,
            },
            '& .MuiTypography-h6': {
                color: theme.palette.primary.contrastTextLight,
                fontSize: theme.spacing('0.75rem'),
                fontWeight: theme.typography.fontWeightRegular,
                marginBottom: theme.spacing(1.5),
            },
            '& .MuiTypography-h5': {
                color: theme.palette.primary.contrastTextActive,
                fontSize: theme.spacing('0.875rem'),
                fontWeight: theme.typography.fontWeightRegular,
                marginBottom: theme.spacing(1.5),
            },
            '& .MuiTypography-h4': {
                color: theme.palette.primary.contrastText,
                fontSize: theme.spacing('0.875rem'),
                fontWeight: theme.typography.fontWeightRegular,
            },
            '& .MuiFormLabel-root': {
                color: theme.palette.primary.contrastTextLight,
            },
            '& .MuiFormControl-root': {
                '& .MuiInputBase-root': {
                    backgroundColor: theme.palette.primary.light,
                    maxHeight: theme.spacing(5.5),
                },
            },
        },
    };
});