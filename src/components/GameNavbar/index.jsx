import React from 'react'
import { GameNavbarWrapper } from './style';
import { AppBar, Box, Tab, Tabs, TextField, Typography, useTheme } from '@mui/material';
import SearchRoundedIcon from '@mui/icons-material/SearchRounded';
import CalendarMonthRoundedIcon from '@mui/icons-material/CalendarMonthRounded';
import VideocamRoundedIcon from '@mui/icons-material/VideocamRounded';
import AssignmentTurnedInRoundedIcon from '@mui/icons-material/AssignmentTurnedInRounded';

function LinkTab(props) {
  return (
    <Tab
      component='a'
      onClick={(event) => {
        event.preventDefault();
      }}
      {...props}
    />
  );
}

export default function GameNavbar() {
  const theme = useTheme();

  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <GameNavbarWrapper theme={theme}>
      <AppBar position='static'>
        <Box className='title-box'>
          <img src={'assets/images/stock-images/soccer-bg.png'} alt='Title Img' />
          <Typography variant='h1'>Soccer</Typography>
        </Box>
        <Box className='navbar-wrap'>
          <Tabs value={value} onChange={handleChange} variant='scrollable' scrollButtons allowScrollButtonsMobile aria-label='nav tabs example'>
            <LinkTab href='/' icon={<CalendarMonthRoundedIcon />} iconPosition='start' label='Pre Matches' />
            <LinkTab href='/' icon={<VideocamRoundedIcon />} iconPosition='start' label='Live Matches' />
            <LinkTab href='/' icon={<AssignmentTurnedInRoundedIcon />} iconPosition='start' label='Results' />
          </Tabs>
        </Box>
        <Box className='search-wrap'>
          <Box className='input-holder'>
            <TextField placeholder='Search.....' />
            <Box className='icon-box'>
              <SearchRoundedIcon/>
            </Box>
          </Box>
        </Box>
      </AppBar>
    </GameNavbarWrapper>
  )
}
