import { Box, styled } from '@mui/material';

export const GameNavbarWrapper = styled(Box)(({ theme }) => {
    return {
        '& .MuiAppBar-root': {
            backgroundColor: theme.palette.primary.light,
            borderRadius: theme.spacing(1.25),
            justifyContent: 'space-between',
            flexDirection: 'row',
            '& .title-box': {
                width: theme.spacing('fit-content'),
                minWidth: theme.spacing(26.875),
                height: theme.spacing(10),
                borderRadius: theme.spacing(1.25, 0, 0, 1.25),
                overflow: 'hidden',
                position: 'relative',
                '&::before': {
                    content: '""',
                    width: theme.spacing('100%'),
                    height: theme.spacing('100%'),
                    background: theme.palette.gradient.appBarImg,
                    borderRadius: theme.spacing(1.25, 0, 0, 1.25),
                    display: 'block',
                    position: 'absolute',
                    top: theme.spacing(0),
                    right: theme.spacing(0),
                },
                '& img': {
                    height: theme.spacing(10),
                },
                '& .MuiTypography-h1': {
                    color: theme.palette.primary.contrastText,
                    fontSize: theme.spacing('2rem'),
                    fontWeight: theme.typography.fontWeightMedium,
                    position: 'absolute',
                    top: theme.spacing('50%'),
                    right: theme.spacing(0),
                    transform: theme.spacing('translateY(-50%)'),
                },
            },
            '& .navbar-wrap': {
                width: theme.spacing('100%'),
                display: 'flex',
                alignItems: 'center',
                flexGrow: '1',
                overflow: 'hidden',
                '& .MuiTabs-root': {
                    width: theme.spacing('100%'),
                    display: 'flex',
                    justifyContent: 'center',
                    '& .MuiTab-root': {
                        color: theme.palette.primary.contrastTextLight,
                        fontSize: theme.spacing('0.875rem'),
                        whiteSpace: 'nowrap',
                        display: 'flex !important',
                        '& .MuiButton-startIcon': {
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                        },
                        '& .MuiSvgIcon-root': {
                            fill: theme.palette.primary.contrastTextLight,
                            fontSize: theme.spacing('1.25rem'),
                        },
                        '&:hover, &.active': {
                            color: theme.palette.primary.contrastTextActive,
                            '& svg': {
                                fill: theme.palette.primary.contrastTextActive,
                            },
                        },
                    },
                    '& .MuiTabs-indicator': {
                        display: 'none',
                    },
                },
            },
            '& .search-wrap': {
                minWidth: theme.spacing(30),
                display: 'flex',
                alignItems: 'center',
                padding: theme.spacing(0, 2, 0, 0),
                '& .input-holder': {
                    width: theme.spacing('100%'),
                    marginLeft: theme.spacing('auto'),
                    position: 'relative',
                    '& .MuiFormControl-root': {
                        '& .MuiInputBase-root': {
                            borderRadius: theme.spacing(1.25),
                            '& .MuiInputBase-input': {
                                color: theme.palette.primary.contrastText,
                                fontSize: theme.spacing('0.875rem'),
                                fontWeight: theme.typography.fontWeightRegular,
                                padding: theme.spacing(1.75, 1.75, 1.75, 6),
                            },
                            '& .MuiOutlinedInput-notchedOutline': {
                                borderColor: theme.palette.border.main,
                            },
                        },
                    },
                    '& .icon-box': {
                        width: theme.spacing(6),
                        height: theme.spacing('100%'),
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        position: 'absolute',
                        top: theme.spacing('50%'),
                        transform: theme.spacing('translateY(-50%)'),
                        '& svg': {
                            fill: theme.palette.primary.contrastTextLight,
                        },
                    },
                },
            },
            [theme.breakpoints.down('xm')]: {
                flexWrap: 'wrap',
                '& .title-box': {
                    order: '1',
                },
                '& .navbar-wrap': {
                    order: '3',
                },
                '& .search-wrap': {
                    order: '2',
                },
            },
            [theme.breakpoints.down('md')]: {
                '& .search-wrap': {
                    width: theme.spacing('100%'),
                    padding: theme.spacing(2, 2, 0),
                    '& .MuiFormControl-root': {
                        width: theme.spacing('100%'),
                    },
                },
            },
        },
    };
});