import { Box, styled } from '@mui/material';

export const BonusesWrapper = styled(Box)(({ theme }) => {
    return {
        '& .bonuses-wrap': {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            flexWrap: 'wrap',
            margin: theme.spacing(-1, -1, 5),
            '& .card-holder': {
                width: theme.spacing('calc(100% / 5)'),
                padding: theme.spacing(1),
                [theme.breakpoints.down('lg')]: {
                    width: theme.spacing('calc(100% / 3)'),
                },
                [theme.breakpoints.down('md')]: {
                    width: theme.spacing('calc(100% / 2)'),
                },
                [theme.breakpoints.down('sm')]: {
                    width: theme.spacing('calc(100% / 1)'),
                },
                '& .MuiCard-root': {
                    width: theme.spacing('100%'),
                    backgroundColor: theme.palette.primary.light,
                    borderRadius: theme.spacing(1.25),
                    boxShadow: theme.shadows[0],
                    overflow: 'hidden',
                    '& .MuiCardContent-root': {
                        padding: theme.spacing(2),
                        '& .MuiTypography-body1': {
                            color: theme.palette.primary.contrastTextLight,
                            fontSize: theme.spacing('0.875rem'),
                            fontWeight: theme.typography.fontWeightRegular,
                        },
                    },
                },
            },
        },
    };
});