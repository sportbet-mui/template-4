import React from 'react'
import { BonusesWrapper } from './style';
import { useTheme } from '@emotion/react';
import BannerSlider from '../BannerSlider/BannerSlider';
import { HeadingWrapper, StyledHeadingTypography } from '../Common/StyledTypography/style';
import { Box, Card, CardContent, CardMedia, Typography } from '@mui/material';

export default function Bonuses() {
  const theme = useTheme();

  return (
    <BonusesWrapper theme={theme}>
      <BannerSlider/>
      <HeadingWrapper>
        <StyledHeadingTypography>Promotions / Bonuses</StyledHeadingTypography>
      </HeadingWrapper>
      <Box className='bonuses-wrap'>
        <Box className='card-holder'>
          <Card>
            <CardMedia component='img' image='assets/images/stock-images/bonus-joining.png' alt='Paella dish'/>
            <CardContent>
              <Typography variant='body1'>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five.</Typography>
            </CardContent>
          </Card>
        </Box>
        <Box className='card-holder'>
          <Card>
            <CardMedia component='img' image='assets/images/stock-images/bonus-loyalty.png' alt='Paella dish'/>
            <CardContent>
              <Typography variant='body1'>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five.</Typography>
            </CardContent>
          </Card>
        </Box>
        <Box className='card-holder'>
          <Card>
            <CardMedia component='img' image='assets/images/stock-images/bonus-happy-hours.png' alt='Paella dish'/>
            <CardContent>
              <Typography variant='body1'>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five.</Typography>
            </CardContent>
          </Card>
        </Box>
        <Box className='card-holder'>
          <Card>
            <CardMedia component='img' image='assets/images/stock-images/bonus-deposit.png' alt='Paella dish'/>
            <CardContent>
              <Typography variant='body1'>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five.</Typography>
            </CardContent>
          </Card>
        </Box>
        <Box className='card-holder'>
          <Card>
            <CardMedia component='img' image='assets/images/stock-images/bonus-bet.png' alt='Paella dish'/>
            <CardContent>
              <Typography variant='body1'>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five.</Typography>
            </CardContent>
          </Card>
        </Box>
      </Box>
    </BonusesWrapper>
  )
}
